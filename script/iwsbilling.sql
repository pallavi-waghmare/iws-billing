USE [iwsbilling]
GO
/****** Object:  User [iwsbilling]    Script Date: 10/23/2020 12:32:01 PM ******/
CREATE USER [iwsbilling] FOR LOGIN [iwsbilling] WITH DEFAULT_SCHEMA=[iwsbilling]
GO
/****** Object:  DatabaseRole [gd_execprocs]    Script Date: 10/23/2020 12:32:01 PM ******/
CREATE ROLE [gd_execprocs]
GO
ALTER ROLE [db_securityadmin] ADD MEMBER [iwsbilling]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [iwsbilling]
GO
ALTER ROLE [db_backupoperator] ADD MEMBER [iwsbilling]
GO
ALTER ROLE [db_datareader] ADD MEMBER [iwsbilling]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [iwsbilling]
GO
/****** Object:  Schema [iwsbilling]    Script Date: 10/23/2020 12:32:01 PM ******/
CREATE SCHEMA [iwsbilling]
GO
/****** Object:  Table [dbo].[BillingDetails]    Script Date: 10/23/2020 12:32:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BillingDetails](
	[BDID] [bigint] IDENTITY(1,1) NOT NULL,
	[Customerid] [bigint] NOT NULL CONSTRAINT [DF_BillingDetails_Customerid]  DEFAULT ((0)),
	[CompanyName] [varchar](50) NOT NULL CONSTRAINT [DF_BillingDetails_CompanyName]  DEFAULT (''),
	[BillingAddress] [varchar](max) NOT NULL CONSTRAINT [DF_BillingDetails_BillingAddress]  DEFAULT (''),
	[ContactPerson] [varchar](50) NOT NULL CONSTRAINT [DF_BillingDetails_ContactPerson]  DEFAULT (''),
	[GSTINNumber] [varchar](20) NOT NULL CONSTRAINT [DF_BillingDetails_GSTINNumber]  DEFAULT (''),
	[CreateDate] [datetime] NOT NULL CONSTRAINT [DF_BillingDetails_CreateDate]  DEFAULT (getdate()),
	[LastUpdated] [datetime] NOT NULL CONSTRAINT [DF_BillingDetails_LastUpdated]  DEFAULT (getdate()),
 CONSTRAINT [PK_BillingDetails] PRIMARY KEY CLUSTERED 
(
	[BDID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CustomerMaster]    Script Date: 10/23/2020 12:32:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CustomerMaster](
	[CustomerId] [bigint] IDENTITY(1,1) NOT NULL,
	[CompanyName] [varchar](100) NOT NULL CONSTRAINT [DF_CustomerMaster_CompanyName]  DEFAULT (''),
	[CustomerName] [varchar](100) NOT NULL CONSTRAINT [DF_CustomerMaster_CustomerName]  DEFAULT (''),
	[UserEmail] [varchar](150) NOT NULL CONSTRAINT [DF_CustomerMaster_UserEmail]  DEFAULT (''),
	[LoginPassword] [varchar](10) NOT NULL CONSTRAINT [DF_CustomerMaster_LoginPassword]  DEFAULT (''),
	[AlertEmail] [varchar](150) NOT NULL CONSTRAINT [DF_CustomerMaster_AlertEmail]  DEFAULT (''),
	[AlertMobile] [varchar](31) NOT NULL CONSTRAINT [DF_CustomerMaster_AlertMobile]  DEFAULT (''),
	[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_CustomerMaster_CreatedDate]  DEFAULT (getdate()),
	[ActiveStatus] [bit] NOT NULL CONSTRAINT [DF_CustomerMaster_ActiveStatus]  DEFAULT ((1)),
	[LeadSystem] [bit] NOT NULL CONSTRAINT [DF_CustomerMaster_LeadSystem]  DEFAULT ((0)),
	[Endate] [datetime] NULL,
	[FollupSms] [bit] NOT NULL CONSTRAINT [DF_CustomerMaster_FollupSms]  DEFAULT ((0)),
	[FollupEmail] [bit] NOT NULL CONSTRAINT [DF_CustomerMaster_FollupEmail]  DEFAULT ((0)),
	[MassSms] [bit] NOT NULL CONSTRAINT [DF_CustomerMaster_MassSms]  DEFAULT ((0)),
	[MassEmail] [bit] NOT NULL CONSTRAINT [DF_CustomerMaster_MassEmail]  DEFAULT ((0)),
	[AdminUserCount] [int] NOT NULL CONSTRAINT [DF_CustomerMaster_AdminUserCount]  DEFAULT ((0)),
	[StaffUserCount] [int] NOT NULL CONSTRAINT [DF_CustomerMaster_StaffUserCount]  DEFAULT ((0)),
	[EmailCount] [int] NOT NULL CONSTRAINT [DF_CustomerMaster_EmailCount]  DEFAULT ((0)),
	[SmsCount] [int] NOT NULL CONSTRAINT [DF_CustomerMaster_SmsCount]  DEFAULT ((0)),
	[ContactPerMob] [varchar](50) NOT NULL DEFAULT ((0)),
	[ServiceType] [varchar](10) NOT NULL DEFAULT ('Free'),
	[DailySalesReport] [bit] NULL DEFAULT ((0))
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CustomerMedium]    Script Date: 10/23/2020 12:32:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerMedium](
	[CustMediumId] [bigint] IDENTITY(1,1) NOT NULL,
	[MediumId] [bigint] NOT NULL CONSTRAINT [DF_CustomerMedium_MediumId]  DEFAULT ((0)),
	[CustomerId] [bigint] NOT NULL CONSTRAINT [DF_CustomerMedium_CustomerId]  DEFAULT ((0)),
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_CustomerMedium_IsActive]  DEFAULT ((1))
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[InvoiceDetails]    Script Date: 10/23/2020 12:32:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[InvoiceDetails](
	[InvDtlID] [bigint] IDENTITY(1,1) NOT NULL,
	[InvID] [bigint] NOT NULL,
	[Product] [varchar](50) NOT NULL CONSTRAINT [DF_InvoiceDetails_Product]  DEFAULT (''),
	[Qty] [int] NOT NULL CONSTRAINT [DF_InvoiceDetails_Qty]  DEFAULT ((0)),
	[UnitPrice] [numeric](18, 2) NOT NULL CONSTRAINT [DF_InvoiceDetails_UnitPrice]  DEFAULT ((0)),
	[TotalAmount] [numeric](18, 0) NOT NULL CONSTRAINT [DF_InvoiceDetails_TotaAmount]  DEFAULT ((0)),
 CONSTRAINT [PK_InvoiceDetails] PRIMARY KEY CLUSTERED 
(
	[InvDtlID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[InvoiceFooter]    Script Date: 10/23/2020 12:32:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[InvoiceFooter](
	[InvFootID] [bigint] IDENTITY(1,1) NOT NULL,
	[Body] [varchar](max) NOT NULL CONSTRAINT [DF_Table_1_Address]  DEFAULT (''),
	[CompanyID] [bigint] NOT NULL CONSTRAINT [DF_InvoiceFooter_CompanyID]  DEFAULT ((0)),
 CONSTRAINT [PK_InvoiceFooter] PRIMARY KEY CLUSTERED 
(
	[InvFootID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[InvoiceHeader]    Script Date: 10/23/2020 12:32:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[InvoiceHeader](
	[InvHeadID] [bigint] IDENTITY(1,1) NOT NULL,
	[Address] [varchar](500) NOT NULL CONSTRAINT [DF_InvoiceHeader_Address]  DEFAULT (''),
	[ContactNumber] [numeric](18, 0) NOT NULL CONSTRAINT [DF_InvoiceHeader_ContactNumber]  DEFAULT ((0)),
	[CompanyID] [bigint] NOT NULL CONSTRAINT [DF_InvoiceHeader_CompanyID]  DEFAULT ((0)),
 CONSTRAINT [PK_InvoiceHeader] PRIMARY KEY CLUSTERED 
(
	[InvHeadID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[InvoiceMaster]    Script Date: 10/23/2020 12:32:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[InvoiceMaster](
	[InvID] [bigint] IDENTITY(1,1) NOT NULL,
	[CustomerID] [bigint] NOT NULL CONSTRAINT [DF_InvoiceMaster_CustomerID]  DEFAULT ((0)),
	[CompanyID] [bigint] NOT NULL CONSTRAINT [DF_InvoiceMaster_CompanyID]  DEFAULT ((0)),
	[InvoiceType] [char](1) NOT NULL CONSTRAINT [DF_InvoiceMaster_InvoiceType]  DEFAULT (''),
	[InvoiceNo] [varchar](50) NOT NULL CONSTRAINT [DF_InvoiceMaster_InvoiceNo]  DEFAULT (''),
	[InvoiceNodigit] [numeric](18, 0) NOT NULL CONSTRAINT [DF_InvoiceMaster_InvoiceNodigit]  DEFAULT ((0)),
	[InvAmount] [numeric](18, 0) NOT NULL CONSTRAINT [DF_InvoiceMaster_InvAmount]  DEFAULT ((0)),
	[InvNetAmount] [numeric](18, 2) NOT NULL CONSTRAINT [DF_InvoiceMaster_InvNetAmount]  DEFAULT ((0)),
	[ValidDate] [datetime] NOT NULL CONSTRAINT [DF_InvoiceMaster_ValidDate]  DEFAULT (getdate()),
	[InvDate] [datetime] NOT NULL CONSTRAINT [DF_InvoiceMaster_InvDate]  DEFAULT (getdate()),
	[ModifyDate] [datetime] NOT NULL CONSTRAINT [DF_InvoiceMaster_ModifyDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_InvoiceMaster] PRIMARY KEY CLUSTERED 
(
	[InvID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[InvoiceNoMaster]    Script Date: 10/23/2020 12:32:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[InvoiceNoMaster](
	[InvnoID] [bigint] IDENTITY(1,1) NOT NULL,
	[StartDate] [date] NOT NULL CONSTRAINT [DF_InvoiceNoMaster_StartDate]  DEFAULT (getdate()),
	[EndDate] [date] NOT NULL CONSTRAINT [DF_InvoiceNoMaster_EndDate]  DEFAULT (getdate()),
	[InvoicePrefix] [varchar](50) NOT NULL CONSTRAINT [DF_InvoiceNoMaster_InvoicePrefix]  DEFAULT (''),
	[InvoiceDigit] [numeric](18, 0) NOT NULL CONSTRAINT [DF_InvoiceNoMaster_InvoiceDigit]  DEFAULT ((0)),
	[CompanyID] [bigint] NOT NULL CONSTRAINT [DF_InvoiceNoMaster_CompanyID]  DEFAULT ((0)),
 CONSTRAINT [PK_InvoiceNoMaster] PRIMARY KEY CLUSTERED 
(
	[InvnoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[InvoiceTaxDetails]    Script Date: 10/23/2020 12:32:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InvoiceTaxDetails](
	[InvTaxID] [bigint] IDENTITY(1,1) NOT NULL,
	[InvID] [bigint] NOT NULL CONSTRAINT [DF_InvoiceTaxDetails_InvID]  DEFAULT ((0)),
	[TaxID] [bigint] NOT NULL CONSTRAINT [DF_InvoiceTaxDetails_TaxID]  DEFAULT ((0)),
	[TaxValue] [numeric](18, 0) NOT NULL CONSTRAINT [DF_InvoiceTaxDetails_TaxValue]  DEFAULT ((0)),
	[TaxAmount] [numeric](18, 2) NOT NULL CONSTRAINT [DF_InvoiceTaxDetails_TaxAmount]  DEFAULT ((0)),
	[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_InvoiceTaxDetails_CreatedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_InvoiceTaxDetails] PRIMARY KEY CLUSTERED 
(
	[InvTaxID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MediumMaster]    Script Date: 10/23/2020 12:32:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MediumMaster](
	[MediumId] [bigint] IDENTITY(1,1) NOT NULL,
	[MediumName] [varchar](100) NOT NULL CONSTRAINT [DF_MediumMaster_MediumName]  DEFAULT (''),
	[CompanyId] [bigint] NOT NULL CONSTRAINT [DF_MediumMaster_CompanyId]  DEFAULT ((0))
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PaymentReceived]    Script Date: 10/23/2020 12:32:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PaymentReceived](
	[PaymentID] [bigint] IDENTITY(1,1) NOT NULL,
	[InvID] [bigint] NOT NULL CONSTRAINT [DF_PaymentReceived_InvID]  DEFAULT ((0)),
	[Type] [char](2) NOT NULL CONSTRAINT [DF_PaymentReceived_Type]  DEFAULT (''),
	[PaidAmount] [numeric](18, 2) NOT NULL CONSTRAINT [DF_PaymentReceived_PaidAmount]  DEFAULT ((0)),
	[PaymentMode] [char](2) NOT NULL CONSTRAINT [DF_PaymentReceived_PaymentMode]  DEFAULT (''),
	[ChequeNo] [numeric](18, 0) NOT NULL CONSTRAINT [DF_PaymentReceived_ChequeNo]  DEFAULT ((0)),
	[TransactionID] [varchar](50) NOT NULL CONSTRAINT [DF_PaymentReceived_TransactionID]  DEFAULT (''),
	[PaymentDate] [datetime] NOT NULL CONSTRAINT [DF_PaymentReceived_PaymentDate]  DEFAULT (getdate()),
	[TaxValue] [numeric](18, 0) NOT NULL CONSTRAINT [DF_PaymentReceived_TaxValue]  DEFAULT ((0)),
	[TaxAmount] [numeric](18, 0) NOT NULL CONSTRAINT [DF_PaymentReceived_TaxAmount]  DEFAULT ((0)),
	[Remark] [varchar](500) NOT NULL CONSTRAINT [DF_PaymentReceived_Remark]  DEFAULT (''),
 CONSTRAINT [PK_PaymentReceived] PRIMARY KEY CLUSTERED 
(
	[PaymentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductMaster]    Script Date: 10/23/2020 12:32:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductMaster](
	[ProductId] [bigint] IDENTITY(1,1) NOT NULL,
	[ProductName] [varchar](100) NOT NULL CONSTRAINT [DF_ProductMaster_ProductName]  DEFAULT (''),
	[ProductDescription] [varchar](500) NOT NULL CONSTRAINT [DF_ProductMaster_ProductDescription]  DEFAULT (''),
	[CustomerId] [bigint] NOT NULL CONSTRAINT [DF_ProductMaster_CustomerId]  DEFAULT ((0)),
	[IsArchieved] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TaxMaster]    Script Date: 10/23/2020 12:32:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TaxMaster](
	[TaxID] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL CONSTRAINT [DF_TaxMaster_Name]  DEFAULT (''),
	[Type] [char](1) NOT NULL CONSTRAINT [DF_TaxMaster_Mode]  DEFAULT (''),
	[Amount] [numeric](18, 0) NOT NULL CONSTRAINT [DF_TaxMaster_Percentage]  DEFAULT ((0)),
	[CompanyID] [bigint] NOT NULL CONSTRAINT [DF_TaxMaster_CustomerId]  DEFAULT ((0)),
 CONSTRAINT [PK_TaxMaster] PRIMARY KEY CLUSTERED 
(
	[TaxID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TeamMaster]    Script Date: 10/23/2020 12:32:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TeamMaster](
	[TeamId] [bigint] IDENTITY(1,1) NOT NULL,
	[TeamName] [varchar](250) NOT NULL CONSTRAINT [DF_TeamMaster_TeamName]  DEFAULT (''),
	[CompanyId] [bigint] NOT NULL CONSTRAINT [DF_TeamMaster_CompanyId]  DEFAULT ((0)),
	[UserName] [varchar](250) NOT NULL CONSTRAINT [DF_TeamMaster_UserName]  DEFAULT (''),
	[Password] [varchar](250) NOT NULL CONSTRAINT [DF_TeamMaster_Password]  DEFAULT (''),
	[MobileNo] [varchar](15) NOT NULL CONSTRAINT [DF__TeamMaste__Mobil__2BFE89A6]  DEFAULT ((0)),
	[FromEmail] [varchar](60) NULL,
	[RoleId] [int] NOT NULL DEFAULT ((0))
) ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[TeamMaster] ADD [BranchID] [varchar](100) NOT NULL DEFAULT ((0))
 CONSTRAINT [PK_TeamMaster] PRIMARY KEY CLUSTERED 
(
	[TeamId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserCustomerMaster]    Script Date: 10/23/2020 12:32:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserCustomerMaster](
	[CustomerId] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL CONSTRAINT [DF_UserCustomerMaster_Name]  DEFAULT (''),
	[EmailAddress] [varchar](150) NOT NULL CONSTRAINT [DF_UserCustomerMaster_EmailAddress]  DEFAULT (''),
	[Mobile] [varchar](15) NOT NULL CONSTRAINT [DF_UserCustomerMaster_Mobile]  DEFAULT (''),
	[Query] [varchar](500) NOT NULL CONSTRAINT [DF_UserCustomerMaster_Query]  DEFAULT (''),
	[CreateDate] [datetime] NOT NULL CONSTRAINT [DF_UserCustomerMaster_CreateDate]  DEFAULT (getdate()),
	[ProductId] [bigint] NOT NULL CONSTRAINT [DF_UserCustomerMaster_ProductId]  DEFAULT ((0)),
	[LeadStatus] [bigint] NOT NULL CONSTRAINT [DF_UserCustomerMaster_LeadStatus]  DEFAULT ((1)),
	[Remark] [varchar](1000) NOT NULL CONSTRAINT [DF_UserCustomerMaster_Remark]  DEFAULT (''),
	[MediumId] [bigint] NOT NULL CONSTRAINT [DF_UserCustomerMaster_MediumId]  DEFAULT ((0)),
	[BranchId] [bigint] NOT NULL CONSTRAINT [DF_UserCustomerMaster_BranchId]  DEFAULT ((0)),
	[LastUpdated] [datetime] NOT NULL CONSTRAINT [DF_UserCustomerMaster_LastUpdated]  DEFAULT (getdate()),
	[IsArchieved] [bit] NOT NULL CONSTRAINT [DF_UserCustomerMaster_IsArchieved]  DEFAULT ((0)),
	[PickBy] [bigint] NOT NULL CONSTRAINT [DF_UserCustomerMaster_PickBy]  DEFAULT ((0)),
	[LeadBy] [bigint] NOT NULL CONSTRAINT [DF_UserCustomerMaster_LeadBy]  DEFAULT ((0)),
	[Location] [bigint] NOT NULL CONSTRAINT [DF_UserCustomerMaster_Location]  DEFAULT ((0)),
	[Custom1] [varchar](500) NOT NULL CONSTRAINT [DF_UserCustomerMaster_Custom1]  DEFAULT ((0)),
	[Custom2] [varchar](500) NOT NULL CONSTRAINT [DF_UserCustomerMaster_Custom2]  DEFAULT ((0)),
	[Custom3] [varchar](500) NOT NULL CONSTRAINT [DF_UserCustomerMaster_Custom3]  DEFAULT ((0)),
	[Custom4] [varchar](500) NOT NULL CONSTRAINT [DF_UserCustomerMaster_Custom4]  DEFAULT ((0)),
	[Custom5] [varchar](500) NOT NULL CONSTRAINT [DF_UserCustomerMaster_Custom5]  DEFAULT ((0)),
	[Custom6] [varchar](500) NOT NULL CONSTRAINT [DF_UserCustomerMaster_Custom6]  DEFAULT ((0)),
	[Custom7] [varchar](500) NOT NULL CONSTRAINT [DF_UserCustomerMaster_Custom7]  DEFAULT ((0)),
	[Custom8] [varchar](500) NOT NULL CONSTRAINT [DF_UserCustomerMaster_Custom8]  DEFAULT ((0)),
	[Custom9] [varchar](500) NOT NULL CONSTRAINT [DF_UserCustomerMaster_Custom9]  DEFAULT ((0)),
	[Custom10] [varchar](500) NOT NULL CONSTRAINT [DF_UserCustomerMaster_Custom10]  DEFAULT ((0)),
	[NoOfLeads] [int] NOT NULL CONSTRAINT [DF_UserCustomerMaster_NoOfLeads]  DEFAULT ((0)),
	[Custom11] [varchar](500) NOT NULL CONSTRAINT [DF_UserCustomerMaster_Custom11]  DEFAULT ((0)),
	[Custom12] [varchar](500) NOT NULL CONSTRAINT [DF_UserCustomerMaster_Custom12]  DEFAULT ((0)),
	[Custom13] [varchar](500) NOT NULL CONSTRAINT [DF_UserCustomerMaster_Custom13]  DEFAULT ((0)),
	[Branch] [int] NULL CONSTRAINT [DF_UserCustomerMaster_Branch]  DEFAULT ((0)),
	[Custom14] [varchar](500) NOT NULL CONSTRAINT [DF_UserCustomerMaster_Custom14]  DEFAULT ((0)),
	[Custom15] [varchar](500) NOT NULL CONSTRAINT [DF_UserCustomerMaster_Custom15]  DEFAULT ((0))
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[BillingDetails] ON 

GO
INSERT [dbo].[BillingDetails] ([BDID], [Customerid], [CompanyName], [BillingAddress], [ContactPerson], [GSTINNumber], [CreateDate], [LastUpdated]) VALUES (1, 61011, N'ABC Pvt Ltd', N'Testing', N'Pallavi', N'22AAAAA00000A1Z5', CAST(N'2020-01-11 16:37:28.600' AS DateTime), CAST(N'2020-02-10 11:32:51.880' AS DateTime))
GO
INSERT [dbo].[BillingDetails] ([BDID], [Customerid], [CompanyName], [BillingAddress], [ContactPerson], [GSTINNumber], [CreateDate], [LastUpdated]) VALUES (2, 41178, N'innOvator web solutions Pvt. Ltd.', N'618/Ijmima Complex,Behind Infinity Mall, Link Road, Malad West, Mumbai 400 064', N'Baldesh', N'22AAAAA00000A1Z5', CAST(N'2020-10-17 12:27:48.213' AS DateTime), CAST(N'2020-10-17 12:27:48.213' AS DateTime))
GO
INSERT [dbo].[BillingDetails] ([BDID], [Customerid], [CompanyName], [BillingAddress], [ContactPerson], [GSTINNumber], [CreateDate], [LastUpdated]) VALUES (3, 1, N'ABC Pvt Ltd', N'test', N'Pallavi', N'22AAAAA00000A1Z5', CAST(N'2020-10-21 00:12:29.137' AS DateTime), CAST(N'2020-10-21 00:12:29.137' AS DateTime))
GO
INSERT [dbo].[BillingDetails] ([BDID], [Customerid], [CompanyName], [BillingAddress], [ContactPerson], [GSTINNumber], [CreateDate], [LastUpdated]) VALUES (4, 2, N'Alliance Holidays', N'abc
kandivali We', N'Amar Bargade', N'234sffgf', CAST(N'2020-10-21 04:08:12.593' AS DateTime), CAST(N'2020-10-21 04:08:12.593' AS DateTime))
GO
INSERT [dbo].[BillingDetails] ([BDID], [Customerid], [CompanyName], [BillingAddress], [ContactPerson], [GSTINNumber], [CreateDate], [LastUpdated]) VALUES (5, 3, N'U & I Holidays', N'andnkd', N'Shyam', N'3243dsdff', CAST(N'2020-10-21 04:16:33.140' AS DateTime), CAST(N'2020-10-21 04:16:33.140' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[BillingDetails] OFF
GO
SET IDENTITY_INSERT [dbo].[CustomerMaster] ON 

GO
INSERT [dbo].[CustomerMaster] ([CustomerId], [CompanyName], [CustomerName], [UserEmail], [LoginPassword], [AlertEmail], [AlertMobile], [CreatedDate], [ActiveStatus], [LeadSystem], [Endate], [FollupSms], [FollupEmail], [MassSms], [MassEmail], [AdminUserCount], [StaffUserCount], [EmailCount], [SmsCount], [ContactPerMob], [ServiceType], [DailySalesReport]) VALUES (11, N'Innovator Web Solutions', N'Hiren Vakhariya', N'hiren@innovatorgroup.in', N'admin', N'info@innovatorsol.com', N'9821580606', CAST(N'2018-01-22 04:45:51.370' AS DateTime), 1, 1, CAST(N'2020-01-20 00:00:00.000' AS DateTime), 1, 1, 1, 1, 1, 10, 1874, 0, N'9821580606', N'Free', 1)
GO
SET IDENTITY_INSERT [dbo].[CustomerMaster] OFF
GO
SET IDENTITY_INSERT [dbo].[CustomerMedium] ON 

GO
INSERT [dbo].[CustomerMedium] ([CustMediumId], [MediumId], [CustomerId], [IsActive]) VALUES (11, 16, 11, 1)
GO
INSERT [dbo].[CustomerMedium] ([CustMediumId], [MediumId], [CustomerId], [IsActive]) VALUES (12, 17, 11, 1)
GO
INSERT [dbo].[CustomerMedium] ([CustMediumId], [MediumId], [CustomerId], [IsActive]) VALUES (13, 18, 11, 1)
GO
INSERT [dbo].[CustomerMedium] ([CustMediumId], [MediumId], [CustomerId], [IsActive]) VALUES (14, 19, 11, 1)
GO
INSERT [dbo].[CustomerMedium] ([CustMediumId], [MediumId], [CustomerId], [IsActive]) VALUES (22, 30, 11, 1)
GO
INSERT [dbo].[CustomerMedium] ([CustMediumId], [MediumId], [CustomerId], [IsActive]) VALUES (34, 42, 11, 1)
GO
SET IDENTITY_INSERT [dbo].[CustomerMedium] OFF
GO
SET IDENTITY_INSERT [dbo].[InvoiceDetails] ON 

GO
INSERT [dbo].[InvoiceDetails] ([InvDtlID], [InvID], [Product], [Qty], [UnitPrice], [TotalAmount]) VALUES (1, 1, N'Testing', 100000, CAST(0.13 AS Numeric(18, 2)), CAST(13000 AS Numeric(18, 0)))
GO
INSERT [dbo].[InvoiceDetails] ([InvDtlID], [InvID], [Product], [Qty], [UnitPrice], [TotalAmount]) VALUES (2, 1, N'Testing1', 100, CAST(500.00 AS Numeric(18, 2)), CAST(50000 AS Numeric(18, 0)))
GO
INSERT [dbo].[InvoiceDetails] ([InvDtlID], [InvID], [Product], [Qty], [UnitPrice], [TotalAmount]) VALUES (10002, 2, N'Testing', 50, CAST(100.00 AS Numeric(18, 2)), CAST(5000 AS Numeric(18, 0)))
GO
INSERT [dbo].[InvoiceDetails] ([InvDtlID], [InvID], [Product], [Qty], [UnitPrice], [TotalAmount]) VALUES (10003, 2, N'Testing1', 100000, CAST(0.10 AS Numeric(18, 2)), CAST(10000 AS Numeric(18, 0)))
GO
INSERT [dbo].[InvoiceDetails] ([InvDtlID], [InvID], [Product], [Qty], [UnitPrice], [TotalAmount]) VALUES (20002, 10002, N'Testing', 100000, CAST(0.50 AS Numeric(18, 2)), CAST(50000 AS Numeric(18, 0)))
GO
INSERT [dbo].[InvoiceDetails] ([InvDtlID], [InvID], [Product], [Qty], [UnitPrice], [TotalAmount]) VALUES (20003, 10003, N'Testing', 100000, CAST(0.50 AS Numeric(18, 2)), CAST(50000 AS Numeric(18, 0)))
GO
INSERT [dbo].[InvoiceDetails] ([InvDtlID], [InvID], [Product], [Qty], [UnitPrice], [TotalAmount]) VALUES (20005, 10004, N'Testing1', 1000, CAST(0.75 AS Numeric(18, 2)), CAST(750 AS Numeric(18, 0)))
GO
INSERT [dbo].[InvoiceDetails] ([InvDtlID], [InvID], [Product], [Qty], [UnitPrice], [TotalAmount]) VALUES (20006, 10005, N'Domain Purchase (allianceholidays.in)', 1, CAST(999.00 AS Numeric(18, 2)), CAST(999 AS Numeric(18, 0)))
GO
SET IDENTITY_INSERT [dbo].[InvoiceDetails] OFF
GO
SET IDENTITY_INSERT [dbo].[InvoiceFooter] ON 

GO
INSERT [dbo].[InvoiceFooter] ([InvFootID], [Body], [CompanyID]) VALUES (2, N'The Company warrants that it has the right to provide the Goods but otherwise the Goods are provided on an "as-is" basis without warranty of any kind, express or implied, oral or written including, without limitation, the implied conditions of merchantable quality, fitness for purpose and description, all of which are specifically and unreservedly excluded. In particular, but without limitation, no warranty is given that the Goods are suitable for the purposes intended by the Client.
The Company warrants that the Goods will be supplied using reasonable care and skill. The Company does not warrant that the Goods supplied are error-free, accurate or complete.
Both parties warrant that they are registered under the Data Protection Act in respect of the collection, processing and use of the Goods. Each party will comply with the Act including but not limited to its obligations in respect of any personal data which it may supply to or receive from the other party.', 21)
GO
SET IDENTITY_INSERT [dbo].[InvoiceFooter] OFF
GO
SET IDENTITY_INSERT [dbo].[InvoiceHeader] ON 

GO
INSERT [dbo].[InvoiceHeader] ([InvHeadID], [Address], [ContactNumber], [CompanyID]) VALUES (1, N'SOHAM BUILDING, GROUND FLOOR, OPP. BRAMHAN SAHAYA SANGH HALL,NEAR VAIBHAV CATERERS, CHINA BISTRO LANE, DADAR (W) 400 028

', CAST(7458963210 AS Numeric(18, 0)), 21)
GO
SET IDENTITY_INSERT [dbo].[InvoiceHeader] OFF
GO
SET IDENTITY_INSERT [dbo].[InvoiceMaster] ON 

GO
INSERT [dbo].[InvoiceMaster] ([InvID], [CustomerID], [CompanyID], [InvoiceType], [InvoiceNo], [InvoiceNodigit], [InvAmount], [InvNetAmount], [ValidDate], [InvDate], [ModifyDate]) VALUES (1, 61011, 21, N'T', N'CHKINV1001', CAST(1001 AS Numeric(18, 0)), CAST(63000 AS Numeric(18, 0)), CAST(74340.00 AS Numeric(18, 2)), CAST(N'2020-01-20 00:00:00.000' AS DateTime), CAST(N'2020-01-20 10:29:51.853' AS DateTime), CAST(N'2020-01-20 10:29:51.853' AS DateTime))
GO
INSERT [dbo].[InvoiceMaster] ([InvID], [CustomerID], [CompanyID], [InvoiceType], [InvoiceNo], [InvoiceNodigit], [InvAmount], [InvNetAmount], [ValidDate], [InvDate], [ModifyDate]) VALUES (2, 61011, 21, N'T', N'CHKINV1002', CAST(1002 AS Numeric(18, 0)), CAST(15000 AS Numeric(18, 0)), CAST(17700.00 AS Numeric(18, 2)), CAST(N'2020-03-10 00:00:00.000' AS DateTime), CAST(N'2020-02-10 11:21:18.923' AS DateTime), CAST(N'2020-02-10 11:21:18.923' AS DateTime))
GO
INSERT [dbo].[InvoiceMaster] ([InvID], [CustomerID], [CompanyID], [InvoiceType], [InvoiceNo], [InvoiceNodigit], [InvAmount], [InvNetAmount], [ValidDate], [InvDate], [ModifyDate]) VALUES (10002, 41178, 11, N'T', N'IWSP-2021-101', CAST(1 AS Numeric(18, 0)), CAST(50000 AS Numeric(18, 0)), CAST(59000.00 AS Numeric(18, 2)), CAST(N'2020-11-18 00:00:00.000' AS DateTime), CAST(N'2020-10-17 12:37:00.667' AS DateTime), CAST(N'2020-10-17 12:37:00.667' AS DateTime))
GO
INSERT [dbo].[InvoiceMaster] ([InvID], [CustomerID], [CompanyID], [InvoiceType], [InvoiceNo], [InvoiceNodigit], [InvAmount], [InvNetAmount], [ValidDate], [InvDate], [ModifyDate]) VALUES (10003, 1, 11, N'P', N'', CAST(0 AS Numeric(18, 0)), CAST(750 AS Numeric(18, 0)), CAST(59000.00 AS Numeric(18, 2)), CAST(N'2020-10-21 00:00:00.000' AS DateTime), CAST(N'2020-10-21 00:15:24.423' AS DateTime), CAST(N'2020-10-21 00:15:24.423' AS DateTime))
GO
INSERT [dbo].[InvoiceMaster] ([InvID], [CustomerID], [CompanyID], [InvoiceType], [InvoiceNo], [InvoiceNodigit], [InvAmount], [InvNetAmount], [ValidDate], [InvDate], [ModifyDate]) VALUES (10004, 1, 11, N'T', N'IWSP-2021-101', CAST(1 AS Numeric(18, 0)), CAST(750 AS Numeric(18, 0)), CAST(885.00 AS Numeric(18, 2)), CAST(N'2020-10-31 00:00:00.000' AS DateTime), CAST(N'2020-10-21 00:15:55.580' AS DateTime), CAST(N'2020-10-21 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[InvoiceMaster] ([InvID], [CustomerID], [CompanyID], [InvoiceType], [InvoiceNo], [InvoiceNodigit], [InvAmount], [InvNetAmount], [ValidDate], [InvDate], [ModifyDate]) VALUES (10005, 3, 11, N'T', N'IWSP-2021-101', CAST(1 AS Numeric(18, 0)), CAST(999 AS Numeric(18, 0)), CAST(1178.82 AS Numeric(18, 2)), CAST(N'2020-10-21 00:00:00.000' AS DateTime), CAST(N'2020-10-21 04:18:03.170' AS DateTime), CAST(N'2020-10-21 00:00:00.000' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[InvoiceMaster] OFF
GO
SET IDENTITY_INSERT [dbo].[InvoiceNoMaster] ON 

GO
INSERT [dbo].[InvoiceNoMaster] ([InvnoID], [StartDate], [EndDate], [InvoicePrefix], [InvoiceDigit], [CompanyID]) VALUES (7, CAST(N'2020-01-01' AS Date), CAST(N'2020-12-31' AS Date), N'CHKINV', CAST(1001 AS Numeric(18, 0)), 21)
GO
INSERT [dbo].[InvoiceNoMaster] ([InvnoID], [StartDate], [EndDate], [InvoicePrefix], [InvoiceDigit], [CompanyID]) VALUES (8, CAST(N'2020-10-01' AS Date), CAST(N'2020-10-31' AS Date), N'IWSP-2021-10', CAST(1 AS Numeric(18, 0)), 11)
GO
SET IDENTITY_INSERT [dbo].[InvoiceNoMaster] OFF
GO
SET IDENTITY_INSERT [dbo].[InvoiceTaxDetails] ON 

GO
INSERT [dbo].[InvoiceTaxDetails] ([InvTaxID], [InvID], [TaxID], [TaxValue], [TaxAmount], [CreatedDate]) VALUES (3, 2, 3, CAST(18 AS Numeric(18, 0)), CAST(2340.00 AS Numeric(18, 2)), CAST(N'2020-01-13 18:02:56.723' AS DateTime))
GO
INSERT [dbo].[InvoiceTaxDetails] ([InvTaxID], [InvID], [TaxID], [TaxValue], [TaxAmount], [CreatedDate]) VALUES (8, 1, 2, CAST(9 AS Numeric(18, 0)), CAST(2070.00 AS Numeric(18, 2)), CAST(N'2020-01-14 16:58:44.387' AS DateTime))
GO
INSERT [dbo].[InvoiceTaxDetails] ([InvTaxID], [InvID], [TaxID], [TaxValue], [TaxAmount], [CreatedDate]) VALUES (9, 1, 1, CAST(9 AS Numeric(18, 0)), CAST(2070.00 AS Numeric(18, 2)), CAST(N'2020-01-14 16:58:44.390' AS DateTime))
GO
INSERT [dbo].[InvoiceTaxDetails] ([InvTaxID], [InvID], [TaxID], [TaxValue], [TaxAmount], [CreatedDate]) VALUES (10, 1, 2, CAST(9 AS Numeric(18, 0)), CAST(5670.00 AS Numeric(18, 2)), CAST(N'2020-01-20 10:29:51.863' AS DateTime))
GO
INSERT [dbo].[InvoiceTaxDetails] ([InvTaxID], [InvID], [TaxID], [TaxValue], [TaxAmount], [CreatedDate]) VALUES (11, 1, 1, CAST(9 AS Numeric(18, 0)), CAST(5670.00 AS Numeric(18, 2)), CAST(N'2020-01-20 10:29:51.880' AS DateTime))
GO
INSERT [dbo].[InvoiceTaxDetails] ([InvTaxID], [InvID], [TaxID], [TaxValue], [TaxAmount], [CreatedDate]) VALUES (10002, 2, 2, CAST(9 AS Numeric(18, 0)), CAST(1350.00 AS Numeric(18, 2)), CAST(N'2020-02-10 11:21:19.017' AS DateTime))
GO
INSERT [dbo].[InvoiceTaxDetails] ([InvTaxID], [InvID], [TaxID], [TaxValue], [TaxAmount], [CreatedDate]) VALUES (10003, 2, 1, CAST(9 AS Numeric(18, 0)), CAST(1350.00 AS Numeric(18, 2)), CAST(N'2020-02-10 11:21:19.020' AS DateTime))
GO
INSERT [dbo].[InvoiceTaxDetails] ([InvTaxID], [InvID], [TaxID], [TaxValue], [TaxAmount], [CreatedDate]) VALUES (20002, 10002, 10004, CAST(18 AS Numeric(18, 0)), CAST(9000.00 AS Numeric(18, 2)), CAST(N'2020-10-17 12:37:00.743' AS DateTime))
GO
INSERT [dbo].[InvoiceTaxDetails] ([InvTaxID], [InvID], [TaxID], [TaxValue], [TaxAmount], [CreatedDate]) VALUES (20003, 10003, 10003, CAST(9 AS Numeric(18, 0)), CAST(4500.00 AS Numeric(18, 2)), CAST(N'2020-10-21 00:15:24.610' AS DateTime))
GO
INSERT [dbo].[InvoiceTaxDetails] ([InvTaxID], [InvID], [TaxID], [TaxValue], [TaxAmount], [CreatedDate]) VALUES (20004, 10003, 10002, CAST(9 AS Numeric(18, 0)), CAST(4500.00 AS Numeric(18, 2)), CAST(N'2020-10-21 00:15:24.673' AS DateTime))
GO
INSERT [dbo].[InvoiceTaxDetails] ([InvTaxID], [InvID], [TaxID], [TaxValue], [TaxAmount], [CreatedDate]) VALUES (20005, 10004, 10003, CAST(9 AS Numeric(18, 0)), CAST(67.50 AS Numeric(18, 2)), CAST(N'2020-10-21 00:18:45.097' AS DateTime))
GO
INSERT [dbo].[InvoiceTaxDetails] ([InvTaxID], [InvID], [TaxID], [TaxValue], [TaxAmount], [CreatedDate]) VALUES (20006, 10004, 10002, CAST(9 AS Numeric(18, 0)), CAST(67.50 AS Numeric(18, 2)), CAST(N'2020-10-21 00:18:45.177' AS DateTime))
GO
INSERT [dbo].[InvoiceTaxDetails] ([InvTaxID], [InvID], [TaxID], [TaxValue], [TaxAmount], [CreatedDate]) VALUES (20007, 10005, 10004, CAST(18 AS Numeric(18, 0)), CAST(179.82 AS Numeric(18, 2)), CAST(N'2020-10-21 04:18:03.170' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[InvoiceTaxDetails] OFF
GO
SET IDENTITY_INSERT [dbo].[MediumMaster] ON 

GO
INSERT [dbo].[MediumMaster] ([MediumId], [MediumName], [CompanyId]) VALUES (16, N'Exhibition - OTM 2019', 11)
GO
INSERT [dbo].[MediumMaster] ([MediumId], [MediumName], [CompanyId]) VALUES (17, N'Exhibition - IITT 2019', 11)
GO
INSERT [dbo].[MediumMaster] ([MediumId], [MediumName], [CompanyId]) VALUES (18, N'Exhibition - SATTE 2019', 11)
GO
INSERT [dbo].[MediumMaster] ([MediumId], [MediumName], [CompanyId]) VALUES (19, N'IWS - Website', 11)
GO
INSERT [dbo].[MediumMaster] ([MediumId], [MediumName], [CompanyId]) VALUES (30, N'Just Dial', 11)
GO
INSERT [dbo].[MediumMaster] ([MediumId], [MediumName], [CompanyId]) VALUES (42, N'News Paper', 11)
GO
SET IDENTITY_INSERT [dbo].[MediumMaster] OFF
GO
SET IDENTITY_INSERT [dbo].[PaymentReceived] ON 

GO
INSERT [dbo].[PaymentReceived] ([PaymentID], [InvID], [Type], [PaidAmount], [PaymentMode], [ChequeNo], [TransactionID], [PaymentDate], [TaxValue], [TaxAmount], [Remark]) VALUES (1, 1, N'WO', CAST(74340.00 AS Numeric(18, 2)), N'CA', CAST(0 AS Numeric(18, 0)), N'', CAST(N'2020-01-22 13:04:27.467' AS DateTime), CAST(0 AS Numeric(18, 0)), CAST(0 AS Numeric(18, 0)), N'')
GO
INSERT [dbo].[PaymentReceived] ([PaymentID], [InvID], [Type], [PaidAmount], [PaymentMode], [ChequeNo], [TransactionID], [PaymentDate], [TaxValue], [TaxAmount], [Remark]) VALUES (2, 2, N'WT', CAST(15000.00 AS Numeric(18, 2)), N'  ', CAST(0 AS Numeric(18, 0)), N'', CAST(N'2020-02-10 11:22:26.483' AS DateTime), CAST(2 AS Numeric(18, 0)), CAST(300 AS Numeric(18, 0)), N'')
GO
INSERT [dbo].[PaymentReceived] ([PaymentID], [InvID], [Type], [PaidAmount], [PaymentMode], [ChequeNo], [TransactionID], [PaymentDate], [TaxValue], [TaxAmount], [Remark]) VALUES (3, 10004, N'WT', CAST(750.00 AS Numeric(18, 2)), N'  ', CAST(0 AS Numeric(18, 0)), N'', CAST(N'2020-10-21 00:33:55.063' AS DateTime), CAST(3 AS Numeric(18, 0)), CAST(23 AS Numeric(18, 0)), N'')
GO
SET IDENTITY_INSERT [dbo].[PaymentReceived] OFF
GO
SET IDENTITY_INSERT [dbo].[ProductMaster] ON 

GO
INSERT [dbo].[ProductMaster] ([ProductId], [ProductName], [ProductDescription], [CustomerId], [IsArchieved]) VALUES (26, N'Offers For Innovator Services', N'http://ads.innovatorwebsolutions.com/', 11, 0)
GO
INSERT [dbo].[ProductMaster] ([ProductId], [ProductName], [ProductDescription], [CustomerId], [IsArchieved]) VALUES (75, N'Kerla', N'Kerla Packages Handle only', 11, 1)
GO
INSERT [dbo].[ProductMaster] ([ProductId], [ProductName], [ProductDescription], [CustomerId], [IsArchieved]) VALUES (76, N'OTM 2019', N'All Leads are associated from OTM 2019', 11, 0)
GO
INSERT [dbo].[ProductMaster] ([ProductId], [ProductName], [ProductDescription], [CustomerId], [IsArchieved]) VALUES (77, N'SATTE 2019', N'All Leads are associated from SATTE 2019', 11, 0)
GO
INSERT [dbo].[ProductMaster] ([ProductId], [ProductName], [ProductDescription], [CustomerId], [IsArchieved]) VALUES (78, N'IITT 2019', N'All Leads are associated from IITT 2019', 11, 0)
GO
INSERT [dbo].[ProductMaster] ([ProductId], [ProductName], [ProductDescription], [CustomerId], [IsArchieved]) VALUES (104, N'CUSRELA', N'CRM', 11, 0)
GO
INSERT [dbo].[ProductMaster] ([ProductId], [ProductName], [ProductDescription], [CustomerId], [IsArchieved]) VALUES (105, N'Web Development ', N'This is fr Web Development..... :)', 11, 0)
GO
INSERT [dbo].[ProductMaster] ([ProductId], [ProductName], [ProductDescription], [CustomerId], [IsArchieved]) VALUES (106, N'Digital Marketing', N'This product is for Digital Marketing....', 11, 0)
GO
INSERT [dbo].[ProductMaster] ([ProductId], [ProductName], [ProductDescription], [CustomerId], [IsArchieved]) VALUES (109, N'Digital Marketing 27feb18', N'digital marketing', 11, 0)
GO
INSERT [dbo].[ProductMaster] ([ProductId], [ProductName], [ProductDescription], [CustomerId], [IsArchieved]) VALUES (110, N'Bulk Email', N'Bulk Email', 11, 0)
GO
INSERT [dbo].[ProductMaster] ([ProductId], [ProductName], [ProductDescription], [CustomerId], [IsArchieved]) VALUES (111, N'Bulk SMS', N'Bulk SMS', 11, 0)
GO
INSERT [dbo].[ProductMaster] ([ProductId], [ProductName], [ProductDescription], [CustomerId], [IsArchieved]) VALUES (112, N'B2B Email Marketing', N'B2B Email Marketing', 11, 0)
GO
INSERT [dbo].[ProductMaster] ([ProductId], [ProductName], [ProductDescription], [CustomerId], [IsArchieved]) VALUES (114, N'Lead Generation', N'', 11, 0)
GO
INSERT [dbo].[ProductMaster] ([ProductId], [ProductName], [ProductDescription], [CustomerId], [IsArchieved]) VALUES (120, N'inBound', N'', 11, 0)
GO
INSERT [dbo].[ProductMaster] ([ProductId], [ProductName], [ProductDescription], [CustomerId], [IsArchieved]) VALUES (121, N'Social Media - Digital Marketing', N'', 11, 0)
GO
INSERT [dbo].[ProductMaster] ([ProductId], [ProductName], [ProductDescription], [CustomerId], [IsArchieved]) VALUES (205, N'Video Production', N'', 11, 0)
GO
SET IDENTITY_INSERT [dbo].[ProductMaster] OFF
GO
SET IDENTITY_INSERT [dbo].[TaxMaster] ON 

GO
INSERT [dbo].[TaxMaster] ([TaxID], [Name], [Type], [Amount], [CompanyID]) VALUES (1, N'SGST', N'I', CAST(9 AS Numeric(18, 0)), 21)
GO
INSERT [dbo].[TaxMaster] ([TaxID], [Name], [Type], [Amount], [CompanyID]) VALUES (2, N'CGST', N'I', CAST(9 AS Numeric(18, 0)), 21)
GO
INSERT [dbo].[TaxMaster] ([TaxID], [Name], [Type], [Amount], [CompanyID]) VALUES (3, N'IGST', N'I', CAST(18 AS Numeric(18, 0)), 21)
GO
INSERT [dbo].[TaxMaster] ([TaxID], [Name], [Type], [Amount], [CompanyID]) VALUES (4, N'TDS', N'C', CAST(2 AS Numeric(18, 0)), 21)
GO
INSERT [dbo].[TaxMaster] ([TaxID], [Name], [Type], [Amount], [CompanyID]) VALUES (10002, N'IGST', N'I', CAST(9 AS Numeric(18, 0)), 11)
GO
INSERT [dbo].[TaxMaster] ([TaxID], [Name], [Type], [Amount], [CompanyID]) VALUES (10003, N'CGST', N'I', CAST(9 AS Numeric(18, 0)), 11)
GO
INSERT [dbo].[TaxMaster] ([TaxID], [Name], [Type], [Amount], [CompanyID]) VALUES (10004, N'SGST', N'I', CAST(18 AS Numeric(18, 0)), 11)
GO
SET IDENTITY_INSERT [dbo].[TaxMaster] OFF
GO
SET IDENTITY_INSERT [dbo].[TeamMaster] ON 

GO
INSERT [dbo].[TeamMaster] ([TeamId], [TeamName], [CompanyId], [UserName], [Password], [MobileNo], [FromEmail], [RoleId], [BranchID]) VALUES (3, N'Simran Ramrakhiani', 11, N'sales@innovatorsol.com', N'abc1234', N'7977006242', N'sales@innovatorsol.com', 3, N'1,2')
GO
INSERT [dbo].[TeamMaster] ([TeamId], [TeamName], [CompanyId], [UserName], [Password], [MobileNo], [FromEmail], [RoleId], [BranchID]) VALUES (81, N'Nilesh Jadhav', 11, N'njadhav@innovatorsol.com', N'abc1234', N'8888890995', N'', 2, N'1,2,3')
GO
INSERT [dbo].[TeamMaster] ([TeamId], [TeamName], [CompanyId], [UserName], [Password], [MobileNo], [FromEmail], [RoleId], [BranchID]) VALUES (82, N'Ruby', 11, N'Telesales2@innovatorsol.com', N'Ruby@1234', N'7337566625', N'Telesales2@innovatorsol.com', 2, N'1,2,3')
GO
INSERT [dbo].[TeamMaster] ([TeamId], [TeamName], [CompanyId], [UserName], [Password], [MobileNo], [FromEmail], [RoleId], [BranchID]) VALUES (111, N'Head Office 3', 21, N'amey@innovatorsol.com', N'Headoffice', N'8104613708', N'amey@innovatorsol.com', 0, N'14')
GO
INSERT [dbo].[TeamMaster] ([TeamId], [TeamName], [CompanyId], [UserName], [Password], [MobileNo], [FromEmail], [RoleId], [BranchID]) VALUES (112, N'Head Office 4', 21, N'ruby@innovatorsol.com', N'Headoffice', N'9082423242', N'ruby@innovatorsol.com', 0, N'14')
GO
INSERT [dbo].[TeamMaster] ([TeamId], [TeamName], [CompanyId], [UserName], [Password], [MobileNo], [FromEmail], [RoleId], [BranchID]) VALUES (113, N'Varun Jain', 10045, N'dev4@innovatorso.com', N'admin', N'9769523075', N'dev4@innovatorso.com', 64, N'16')
GO
INSERT [dbo].[TeamMaster] ([TeamId], [TeamName], [CompanyId], [UserName], [Password], [MobileNo], [FromEmail], [RoleId], [BranchID]) VALUES (114, N'Ruby Shah', 10043, N'ruby@innovatorsol.com', N'326554', N'9082423242', N'ruby@innovatorsol.com', 0, N'0')
GO
INSERT [dbo].[TeamMaster] ([TeamId], [TeamName], [CompanyId], [UserName], [Password], [MobileNo], [FromEmail], [RoleId], [BranchID]) VALUES (125, N'Karuna Motwani', 11, N'kmotwani@innovatorsol.com', N'Abc@1234', N'9372698374', N'kmotwani@innovatorsol.com', 2, N'1')
GO
SET IDENTITY_INSERT [dbo].[TeamMaster] OFF
GO
SET IDENTITY_INSERT [dbo].[UserCustomerMaster] ON 

GO
INSERT [dbo].[UserCustomerMaster] ([CustomerId], [Name], [EmailAddress], [Mobile], [Query], [CreateDate], [ProductId], [LeadStatus], [Remark], [MediumId], [BranchId], [LastUpdated], [IsArchieved], [PickBy], [LeadBy], [Location], [Custom1], [Custom2], [Custom3], [Custom4], [Custom5], [Custom6], [Custom7], [Custom8], [Custom9], [Custom10], [NoOfLeads], [Custom11], [Custom12], [Custom13], [Branch], [Custom14], [Custom15]) VALUES (1, N'Test', N'palla.kadam@gmail.com', N'8779129958', N'', CAST(N'2020-10-21 00:04:24.860' AS DateTime), 0, 1, N'', 0, 0, CAST(N'2020-10-21 00:04:24.860' AS DateTime), 0, 0, 11, 0, N'0', N'0', N'0', N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0, N'0', N'0', N'0', 0, N'0', N'0')
GO
INSERT [dbo].[UserCustomerMaster] ([CustomerId], [Name], [EmailAddress], [Mobile], [Query], [CreateDate], [ProductId], [LeadStatus], [Remark], [MediumId], [BranchId], [LastUpdated], [IsArchieved], [PickBy], [LeadBy], [Location], [Custom1], [Custom2], [Custom3], [Custom4], [Custom5], [Custom6], [Custom7], [Custom8], [Custom9], [Custom10], [NoOfLeads], [Custom11], [Custom12], [Custom13], [Branch], [Custom14], [Custom15]) VALUES (2, N'Alliance Group', N'amar@allianceholidays.in', N'9920066340', N'', CAST(N'2020-10-21 04:06:56.843' AS DateTime), 0, 1, N'', 0, 0, CAST(N'2020-10-21 04:06:56.843' AS DateTime), 0, 0, 11, 0, N'0', N'0', N'0', N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0, N'0', N'0', N'0', 0, N'0', N'0')
GO
INSERT [dbo].[UserCustomerMaster] ([CustomerId], [Name], [EmailAddress], [Mobile], [Query], [CreateDate], [ProductId], [LeadStatus], [Remark], [MediumId], [BranchId], [LastUpdated], [IsArchieved], [PickBy], [LeadBy], [Location], [Custom1], [Custom2], [Custom3], [Custom4], [Custom5], [Custom6], [Custom7], [Custom8], [Custom9], [Custom10], [NoOfLeads], [Custom11], [Custom12], [Custom13], [Branch], [Custom14], [Custom15]) VALUES (3, N'U & I Holidays', N'shyam@uandiholidays.com', N'9833425594', N'', CAST(N'2020-10-21 04:15:53.060' AS DateTime), 0, 1, N'', 0, 0, CAST(N'2020-10-21 04:15:53.060' AS DateTime), 0, 0, 11, 0, N'0', N'0', N'0', N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0, N'0', N'0', N'0', 0, N'0', N'0')
GO
SET IDENTITY_INSERT [dbo].[UserCustomerMaster] OFF
GO
/****** Object:  StoredProcedure [dbo].[SetProcedure]    Script Date: 10/23/2020 12:32:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SetProcedure] 
	@qry varchar(max)=''
	
AS
BEGIN
	SET NOCOUNT ON;
	if (@qry<>'')
	begin
		exec(@qry)
	end
END










GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'P=Pending,I=InProcess,S=Sold,L=Lost' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserCustomerMaster', @level2type=N'COLUMN',@level2name=N'LeadStatus'
GO
USE [master]
GO
ALTER DATABASE [iwsbilling] SET  READ_WRITE 
GO
