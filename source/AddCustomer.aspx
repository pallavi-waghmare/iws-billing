﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AddCustomer.aspx.vb" Inherits="AddCustomer" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Src="~/Header.ascx" TagName="UCHeader" TagPrefix="UC1" %>
<%@ Register Src="~/footer.ascx" TagName="UCFooter" TagPrefix="UC1" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Add Customer</title>
    <link rel="icon" href="Images/thumb.png" />
    <link href="Css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="Css/css.css" rel="stylesheet" type="text/css" />
    <link href="Css/Style.css" rel="Stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div>
                <UC1:UCHeader ID="UCHeader" runat="server" />
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="boxshadow">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="title">
                                    Create Customer
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 padding">
                                <%--<div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="email">
                                            <span class="red">*</span>Product
                                        </label>
                                        <asp:DropDownList ID="drpPro" runat="server" class="form-control">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="drpPro"
                                            ErrorMessage="<br/>Select Product" InitialValue="" SetFocusOnError="true" Display="Dynamic"
                                            ValidationGroup="Save"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="email">
                                            <span class="red">*</span>Medium
                                        </label>
                                        <asp:DropDownList ID="drpMed" runat="server" class="form-control">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="drpMed"
                                            ErrorMessage="<br/>Select Medium" InitialValue="" SetFocusOnError="true" Display="Dynamic"
                                            ValidationGroup="Save"></asp:RequiredFieldValidator>
                                    </div>
                                </div>--%>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="email">
                                            <span class="red">*</span>Company Name
                                        </label>
                                        <asp:TextBox ID="txtCName" runat="server" class="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="req" runat="server" ControlToValidate="txtCName" ErrorMessage="<br/>Enter Name"
                                            InitialValue="" SetFocusOnError="true" Display="Dynamic" ValidationGroup="Save"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 padding">
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="email">
                                            <span class="red">*</span>Billing Address
                                        </label>
                                        <asp:TextBox ID="txtbillingDetails" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox>
                                        <cc1:AutoCompleteExtender ServiceMethod="SearchCustomers" MinimumPrefixLength="2"
                                            CompletionInterval="100" EnableCaching="false" CompletionSetCount="10" TargetControlID="txtbillingDetails"
                                            ID="AutoCompleteExtender1" runat="server" FirstRowSelected="false">
                                        </cc1:AutoCompleteExtender>
                                    </div>
                                </div>
                                    </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 padding">
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="email">
                                            <span class="red">*</span>City
                                        </label>
                                        <asp:TextBox ID="TxtCity" runat="server" class="form-control"></asp:TextBox>
                                        <cc1:AutoCompleteExtender ServiceMethod="SearchCustomers" MinimumPrefixLength="2"
                                            CompletionInterval="100" EnableCaching="false" CompletionSetCount="10" TargetControlID="TxtCity"
                                            ID="AutoCompleteExtender2" runat="server" FirstRowSelected="false">
                                        </cc1:AutoCompleteExtender>
                                    </div>
                                </div>
                                    </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 padding">
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="email">
                                            <span class="red">*</span>State
                                        </label>
                                        <asp:TextBox ID="TxtState" runat="server" class="form-control"></asp:TextBox>
                                        <cc1:AutoCompleteExtender ServiceMethod="SearchCustomers" MinimumPrefixLength="2"
                                            CompletionInterval="100" EnableCaching="false" CompletionSetCount="10" TargetControlID="TxtState"
                                            ID="AutoCompleteExtender3" runat="server" FirstRowSelected="false">
                                        </cc1:AutoCompleteExtender>
                                    </div>
                                </div>
                                    </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 padding">
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="email">
                                            <span class="red">*</span>GST Number
                                        </label>
                                        <asp:TextBox ID="TxtGSTN" runat="server" class="form-control"></asp:TextBox>
                                        <cc1:AutoCompleteExtender ServiceMethod="SearchCustomers" MinimumPrefixLength="2"
                                            CompletionInterval="100" EnableCaching="false" CompletionSetCount="10" TargetControlID="TxtGSTN"
                                            ID="AutoCompleteExtender4" runat="server" FirstRowSelected="false">
                                        </cc1:AutoCompleteExtender>
                                    </div>
                                </div>
                                    </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 padding">
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="email">
                                            <span class="red">*</span>Contact Person Name
                                        </label>
                                        <asp:TextBox ID="txtContactNo" runat="server" class="form-control"></asp:TextBox>
                                        <cc1:AutoCompleteExtender ServiceMethod="SearchCustomers" MinimumPrefixLength="2"
                                            CompletionInterval="100" EnableCaching="false" CompletionSetCount="10" TargetControlID="txtContactNo"
                                            ID="AutoCompleteExtender5" runat="server" FirstRowSelected="false">
                                        </cc1:AutoCompleteExtender>
                                    </div>
                                </div>
                                    </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 padding">
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="email">
                                            <span class="red">*</span>Email
                                        </label>
                                        <asp:TextBox ID="txtEmail" runat="server" class="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtEmail"
                                            ErrorMessage="<br/>Enter Email" InitialValue="" SetFocusOnError="true" Display="Dynamic"
                                            ValidationGroup="Save"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtEmail"
                                            SetFocusOnError="true" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                            ErrorMessage="<br/>Enter Valid EmailID" Text="" Display="Dynamic" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                    </div>
                                </div>
                                    </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 padding">
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="email">
                                            <span class="red">*</span>Mobile
                                        </label>
                                        <asp:TextBox ID="txtMob" runat="server" MaxLength="15" class="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtMob"
                                            ErrorMessage="<br/>Enter Mobile" InitialValue="" SetFocusOnError="true" Display="Dynamic"
                                            ValidationGroup="Save"></asp:RequiredFieldValidator>
                                        <ajax:FilteredTextBoxExtender ID="fil" runat="server" TargetControlID="txtMob" FilterType="Numbers,Custom"
                                            ValidChars="+">
                                        </ajax:FilteredTextBoxExtender>
                                    </div>
                                </div>
                                    </div>
                                
                            </div>

                        
                        <div class="col-md-12 col-sm-12 col-xs-12 padding">
                            <div class="form-group">
                                <asp:Repeater ID="rep" runat="server">
                                    <ItemTemplate>
                                        <div class="col-md-3 col-sm-3 col-xs-12">
                                            <div class="form-group">
                                                <label for="email">
                                                    <asp:Label ID="lblField" runat="server"></asp:Label>
                                                </label>
                                                <asp:Label ID="lblMainField" runat="server" Style="display: none"></asp:Label>
                                                <asp:TextBox ID="txtField" runat="server" class="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="reqField" runat="server" ControlToValidate="txtField"
                                                    InitialValue="" SetFocusOnError="true" Display="Dynamic"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12 padding bottomspace">
                            <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
                                <div class="form-group">
                                    <asp:Button runat="server" ID="BtnSubmit" Text="Save" CssClass="dwnbtn" ValidationGroup="Save" />
                                    <asp:Button runat="server" ID="BtnCancel" Text="Cancel" CausesValidation="false"
                                        CssClass="dwnbtn" />
                                    <asp:Label runat="server" ID="LblMode" Visible="false" Text="Add"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <asp:Label runat="server" ID="LblId" Visible="false"></asp:Label>
                            <asp:Label runat="server" ID="LblMessage" CssClass="bold red"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <UC1:UCFooter ID="UCFooter" runat="server" />
        </div>
        </div>
    </form>
</body>
</html>

