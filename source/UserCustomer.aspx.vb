﻿Imports System.Data
Imports System.IO

Partial Class UserCustomer
    Inherits System.Web.UI.Page
    Dim obj As New Engine
    Private Sub page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Session("CompanyId") = Nothing Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            CType(UCHeader.FindControl("LblHeading"), Label).Text = "Customer List"
            'TxtFrom.Text = obj.GetFormattedDate(Date.Today.AddMonths(-1))
            'TxtTo.Text = obj.GetFormattedDate(Date.Today)

            TxtFrom.Attributes.Add("Readonly", "Readonly")
            TxtTo.Attributes.Add("Readonly", "Readonly")

            BindDrp()
            Bind()
        End If
    End Sub

    Public Sub BindDrp()
        Dim sql As String = ""
        sql &= "select CustomerId,CompanyName from CustomerMaster Where CustomerId=" & Session("CustId") & ";"
        'sql &= "select ProductId,ProductName from ProductMaster;"
        'sql &= "select * from MediumMaster;"
        Dim ds As New DataSet
        ds = obj.GetDataset(sql)

        obj.BindListControl1(drpSerCust, ds.Tables(0), "CompanyName", "CustomerId")
        'obj.BindListControl1(drpSerPro, ds.Tables(1), "ProductName", "ProductId")

        'drpSerCust.Items.Insert(0, New ListItem("All Customer", ""))
        drpSerPro.Items.Insert(0, New ListItem("All Product", ""))
        If drpSerCust.SelectedValue <> "" Then
            bindPro(drpSerCust.SelectedValue)
        End If

    End Sub
    Public Sub bindPro(ByVal id As Integer)
        obj.BindListControl(drpSerPro, "select ProductId,ProductName from ProductMaster where CustomerId=" & id, "ProductName", "ProductId")
    End Sub
    Public Sub Bind()
        Dim sql = "", strWhere As String = ""


        If TxtFrom.Text <> "" Then
            strWhere &= " and LM.CreateDate>='" & TxtFrom.Text.Replace("'", "''") & "'"
        End If

        If TxtTo.Text <> "" Then
            strWhere &= " and LM.CreateDate<=DATEADD(day,1,'" & TxtTo.Text.Replace("'", "''") & "')"
        End If

        sql &= "select CreateDate as CreateDate,LM.CustomerId as LeadId,CreateDate as LastUpdated, LM.* from UserCustomerMaster LM "
        'sql &= "left join ProductMaster PM on PM.ProductId=LM.ProductId "
        'sql &= "left join CustomerMaster CM on CM.CustomerId=PM.CustomerId "
        'sql &= "left join MediumM CM on CM.CustomerId=PM.CustomerId "
        sql &= " where 1=1 " & strWhere & " order by LM.CustomerID desc"
        Dim EndDate As String = ""
        Dim leadSystem As String = Session("LeadSystem")
        If Not IsDBNull(Session("Enddate")) Then
            EndDate = Session("Enddate")
        End If

        Dim led As New LeadEngine
        Dim dt As DataTable = obj.GetDataTable(sql)
        Dim StrString As String = ""
        'StrString = led.GetCustomer(dt, Session("CompanyId"), "Leads", Session("Role"), leadSystem, EndDate, 50, LblNoUserCount, LnkPreviousRecord, LnkNextRecord, DrpOnRecord)

        lblLeads.Text = StrString
        Dim IntRecourtCount As Integer
        IntRecourtCount = obj.BindGridControl(rep, sql, LblHideUserCount, LblDispUserCount, LblNoUserCount, "Customers", "LnkPreviousRecord", "LnkNextRecord", "DrpOnRecord", 50)
        If IntRecourtCount > 0 Then
            BtnDelete.Visible = True
        Else
            BtnDelete.Visible = False
        End If

    End Sub

    Public Sub PreviousUserRecord(ByVal sender As Object, ByVal e As System.EventArgs)
        LblHideUserCount.Text = CInt(LblHideUserCount.Text) - 1
        obj.PreviousRecord()
        Bind()
    End Sub

    Public Sub NextUserRecord(ByVal sender As Object, ByVal e As System.EventArgs)
        LblHideUserCount.Text = CInt(LblHideUserCount.Text) + 1
        obj.NextRecord()
        Bind()
    End Sub

    Protected Sub OnUserRecord(ByVal sender As Object, ByVal e As System.EventArgs)
        LblHideUserCount.Text = CType(rep.Controls(rep.Controls.Count - 1).FindControl("DrpOnRecord"), DropDownList).SelectedValue
        obj.OnRecord(LblHideUserCount.Text)
        Bind()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Bind()
    End Sub

    Private Sub rep_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) Handles rep.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            If Not IsDBNull(e.Item.DataItem("Name")) Then CType(e.Item.FindControl("lblName"), Label).Text = e.Item.DataItem("Name")
            If Not IsDBNull(e.Item.DataItem("EmailAddress")) Then CType(e.Item.FindControl("lblEmail"), Label).Text = e.Item.DataItem("EmailAddress")
            If Not IsDBNull(e.Item.DataItem("Mobile")) Then CType(e.Item.FindControl("lblMob"), Label).Text = e.Item.DataItem("Mobile")
            If Not IsDBNull(e.Item.DataItem("CreateDate")) Then CType(e.Item.FindControl("lblCDate"), Label).Text = obj.GetFormattedDateTime(e.Item.DataItem("CreateDate"))
            'If Not IsDBNull(e.Item.DataItem("ProductName")) Then CType(e.Item.FindControl("lblPro"), Label).Text = e.Item.DataItem("ProductName")
            CType(e.Item.FindControl("lblleadcount"), Label).Text = obj.GetField("Select Count(*) as Cnt from UserCustomerMaster l where Mobile='" & e.Item.DataItem("Mobile") & "' Group By l.Mobile", "Cnt")

            Dim cnt As Integer = 0
            cnt = obj.GetField("Select Count(*) as Cnt From BillingDetails where Customerid=" & e.Item.DataItem("Customerid"), "Cnt")
            If cnt = 0 Then
                CType(e.Item.FindControl("LnkInvoice"), LinkButton).Enabled = False
                CType(e.Item.FindControl("LnkInvoice"), LinkButton).ToolTip = "Please Add Billing Details To Generate Invoice."
            Else
                CType(e.Item.FindControl("LnkInvoice"), LinkButton).Enabled = True
            End If
        End If
    End Sub

    Private Sub rep_ItemCommand(ByVal source As Object, ByVal e As RepeaterCommandEventArgs) Handles rep.ItemCommand
        If e.CommandName = "Edit" Then
            Response.Redirect("AddCustomer.aspx?Id=" & e.CommandArgument)
        End If
        If e.CommandName = "GenerateInvoice" Then
            Response.Redirect("CreateInvoice.aspx?Id=" & e.CommandArgument )
        End If
        If e.CommandName = "ViewInvoice" Then
            Response.Redirect("InvoiceList.aspx?CustId=" & e.CommandArgument)
        End If
        If e.CommandName = "AddBillingDtl" Then
            Response.Redirect("BillingDetails.aspx?Id=" & e.CommandArgument)
        End If
    End Sub
    Protected Sub BtnDelete_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnDelete.Click
        Dim strId As String = ""
        For i As Integer = 0 To rep.Items.Count - 1
            If CType(rep.Items(i).FindControl("ChkUserInfo"), CheckBox).Checked = True Then
                strId &= CType(rep.Items(i).FindControl("LblChk"), Label).Text & ","
            End If
        Next

        If strId <> "" Then

            strId = Left(strId, strId.Length - 1)

            Dim Sql As String = "Delete From UserCustomerMaster where LeadId in (" & strId & ")"
            obj.ExecuteCommand(Sql)
            Bind()
            LblMessage.Text = "Sucessfully Deleted !"
        End If

    End Sub
    Private Sub WriteToCSV(ByVal dt As DataTable)
        Dim attachment As String = "attachment; filename=Lead_list.csv" 'string attachment = "attachment; filename=PersonList.csv";
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.ClearHeaders()
        HttpContext.Current.Response.ClearContent()
        HttpContext.Current.Response.AddHeader("content-disposition", attachment)
        HttpContext.Current.Response.ContentType = "text/csv"
        HttpContext.Current.Response.AddHeader("Pragma", "public")
        WriteColumnName()
        Dim DR As DataRow
        For Each DR In dt.Rows
            WriteUserInfo(DR)
        Next
        HttpContext.Current.Response.End()
    End Sub
    Private Sub WriteColumnName()
        Dim columnNames As String = "Name,Email,Mobile,Query,CreateDate,Remark,Medium,Product,Customer,Status,LastUpdate"
        HttpContext.Current.Response.Write(columnNames)
        HttpContext.Current.Response.Write(Environment.NewLine)
    End Sub
    Private Sub WriteUserInfo(ByVal dr As DataRow)
        Dim sb As New StringBuilder()
        'Dim CSVTotalDue As Long

        Dim Rsread As New RecordSetRead
        If Not IsDBNull(dr.Item("Name")) Then
            sb.Append(dr.Item("Name").ToString.Replace(",", " ") & ", ")
        Else
            sb.Append(" " & ", ")
        End If

        If Not IsDBNull(dr.Item("EmailAddress")) Then
            sb.Append(dr.Item("EmailAddress").ToString.Replace(",", " ") & ", ")
        Else
            sb.Append(" " & ", ")
        End If

        If Not IsDBNull(dr.Item("Mobile")) Then
            sb.Append(dr.Item("Mobile").ToString.Replace(",", " ") & ", ")
        Else
            sb.Append(" " & ", ")
        End If

        If Not IsDBNull(dr.Item("Query")) Then
            sb.Append(dr.Item("Query").ToString.Replace(",", " ") & ", ")
        Else
            sb.Append(" " & ", ")
        End If

        If Not IsDBNull(dr.Item("CreateDate")) Then
            sb.Append(obj.GetFormattedDateTime(dr.Item("CreateDate").ToString.Replace(",", " ")) & ", ")
        Else
            sb.Append(" " & ", ")
        End If

        If Not IsDBNull(dr.Item("Remark")) Then
            sb.Append(dr.Item("Remark").ToString.Replace(",", " ") & ", ")
        Else
            sb.Append(" " & ", ")
        End If

        If Not IsDBNull(dr.Item("MediumName")) Then
            sb.Append(dr.Item("MediumName").ToString.Replace(",", " ") & ", ")
        Else
            sb.Append(" " & ", ")
        End If

        If Not IsDBNull(dr.Item("ProductName")) Then
            sb.Append(dr.Item("ProductName").ToString.Replace(",", " ") & ", ")
        Else
            sb.Append(" " & ", ")
        End If

        If Not IsDBNull(dr.Item("CompanyName")) Then
            sb.Append(dr.Item("CompanyName").ToString.Replace(",", " ") & ", ")
        Else
            sb.Append(" " & ", ")
        End If

        If Not IsDBNull(dr.Item("LeadStatus")) Then
            Dim strStat As String = ""

            Select Case Trim(dr.Item("LeadStatus"))
                Case "P"
                    strStat = "Pending"
                Case "I"
                    strStat = "In Process"
                Case "S"
                    strStat = "Sold"
                Case "L"
                    strStat = "Lost"
            End Select

            sb.Append(strStat & ", ")
        Else
            sb.Append(" " & ", ")
        End If

        If Not IsDBNull(dr.Item("LastUpdated")) Then
            sb.Append(obj.GetFormattedDateTime(dr.Item("LastUpdated").ToString.Replace(",", " ")) & ", ")
        Else
            sb.Append(" " & ", ")
        End If


        HttpContext.Current.Response.Write(sb.ToString())
        HttpContext.Current.Response.Write(Environment.NewLine)
    End Sub

    Private Sub BtnCsv_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnCsv.Click
        Dim sql = "", strWhere As String = ""



        If drpSerCust.SelectedValue <> "" Then
            strWhere &= " and CM.CustomerId=" & drpSerCust.SelectedValue
        End If


        If drpSerPro.SelectedValue <> "" Then
            strWhere &= " and LM.ProductId=" & drpSerPro.SelectedValue
        End If

        If TxtFrom.Text <> "" Then
            strWhere &= " and LM.CreateDate>='" & TxtFrom.Text.Replace("'", "''") & "'"
        End If

        If TxtTo.Text <> "" Then
            strWhere &= " and LM.CreateDate<=DATEADD(day,1,'" & TxtTo.Text.Replace("'", "''") & "')"
        End If

        'sql &= "select LM.Name,LM.EmailAddress as Email,LM.Mobile,LM.Query,LM.CreateDate,LM.Remark,MM.MediumName as Medium,PM.ProductName as Product,CM.CompanyName as Customer,"
        'sql &= "case when LM.LeadStatus='P' then 'Pending' when LM.LeadStatus='I' then 'InProcess' when LM.LeadStatus='S' then 'Sold' when LM.LeadStatus='L' then 'Lost' End as Status,"
        'sql &= "LM.LastUpdated from UserCustomerMaster LM "
        'sql &= "left join MediumMaster MM on MM.MediumId=LM.MediumId "
        'sql &= "left join ProductMaster PM on PM.ProductId=LM.ProductId "
        'sql &= "left join CustomerMaster CM on CM.CustomerId=PM.CustomerId "
        'sql &= " where 1=1 " & strWhere & " order by LM.LeadId desc"
        Dim dtCustom As New DataTable
        dtCustom = obj.GetDataTable("select * from CustomFields where CompanyId=" & drpSerCust.SelectedValue & "and CustomName<>'' and ViewInList = 1")
        sql &= "select LM.Name,LM.EmailAddress as Email,LM.Mobile,LM.CreateDate,LM.Remark,PM.ProductName as Product"
        If Not IsNothing(dtCustom) Then
            If dtCustom.Rows.Count > 0 Then
                For i As Integer = 0 To dtCustom.Rows.Count - 1
                    Dim j As Integer = i + 1
                    sql &= ",LM.Custom" & j & " as " & Trim(dtCustom.Rows(i).Item("CustomName").ToString.Replace(" ", ""))
                Next
            End If
        End If
        'sql &= "case when LM.LeadStatus='P' then 'Pending' when LM.LeadStatus='I' then 'InProcess' when LM.LeadStatus='S' then 'Sold' when LM.LeadStatus='L' then 'Lost' End as Status,"
        sql &= ",TM.TeamNAme as PickedBy,Sm.Status as Status ,LM.LastUpdated from UserCustomerMaster LM "
        sql &= "left join ProductMaster PM on PM.ProductId=LM.ProductId "
        sql &= " left Join TeamMaster TM on LM.pickby = Tm.TeamId "
        sql &= "Left Join StatusMaster SM on LM.LeadStatus = SM.Statusid "
        sql &= "left join CustomerMaster CM on CM.CustomerId=PM.CustomerId "
        sql &= " where 1=1 " & strWhere & " order by LM.CustomerId desc"

        Dim dt As New DataTable
        dt = obj.GetDataTable(sql)

        ' WriteToCSV(dt)
        ExportElx(dt)
    End Sub
    Public Sub ExportElx(ByVal dt As DataTable)
        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=Lead_list.xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"
        Dim GridView1 As New GridView
        Using sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)

            'To Export all pages
            GridView1.DataSource = dt
            GridView1.DataBind()

            GridView1.RenderControl(hw)
            Response.Output.Write(sw.ToString())
            Response.Flush()
            Response.[End]()
        End Using
    End Sub

    Protected Sub drpSerCust_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles drpSerCust.SelectedIndexChanged
        drpSerPro.Items.Clear()
        If drpSerCust.SelectedValue <> "" Then
            bindPro(drpSerCust.SelectedValue)
        End If
        drpSerPro.Items.Insert(0, New ListItem("Select Product", ""))
    End Sub

    Private Sub rep_Load(sender As Object, e As EventArgs) Handles rep.Load

    End Sub
End Class
