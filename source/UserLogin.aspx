﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="UserLogin.aspx.vb" Inherits="UserLogin" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Cusrela Login</title>
    <link rel="icon" href="Images/thumb.png" />
    <link href="Css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="Css/css.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css">
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div class="container-fluid loginbg">
            <div class="container">
                <div align="center" class="row">
                    <div class="col-md-4 col-md-offset-5 col-sm-12 col-xs-12">
                        <div>
                            <img src="Images/logo.png" class="logospace max" />
                        </div>
                    </div>
                </div>
                <div align="center" class="row">
                    <div class="col-md-4 col-md-offset-5 col-sm-12 col-xs-12">
                        <div class="mainshadow">
                            <div class="logintxt">
                                Login
                            </div>
                            <div>
                                <div class="form-group">
                                    <label for="email" class="username">
                                        User Id</label>
                                    <asp:TextBox ID="TxtUserName" runat="server" Text="" class="form-control"></asp:TextBox>
                                    <i class="fas fa-user myicon"></i>
                                    <asp:RequiredFieldValidator ID="RfvUserName" runat="server" ErrorMessage="Please Enter <b>UserName</b>!"
                                        ControlToValidate="TxtUserName" Display="None"></asp:RequiredFieldValidator>
                                </div>
                                <div class="form-group">
                                    <label for="pwd" class="username">
                                        Password</label>
                                    <asp:TextBox TextMode="password" ID="TxtPassword" runat="server" class="form-control"></asp:TextBox><i
                                        class="fas fa-unlock-alt myicon"></i>
                                    <asp:RequiredFieldValidator ID="RfvPWD" runat="server" ErrorMessage="Please Enter <b>Password</b>!"
                                        ControlToValidate="TxtPassword" Display="None" ></asp:RequiredFieldValidator>
                                </div>
                                <div class="form-group">
                                    <asp:ValidationSummary ID="ValSum" runat="server" CssClass="alerttxt" />
                                    <asp:Label ID="LblError" runat="server" CssClass="alerttxt"></asp:Label>
                                </div>
                                <div class="form-group">
                                    <asp:Button ID="BtnLogin" runat="server" CssClass="btn btn-default" Text="Login" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div align="center" class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="companyname">
                            By innOvator Web Solutions
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
