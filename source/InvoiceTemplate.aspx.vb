﻿Imports System.Data
Partial Class InvoiceTemplate
    Inherits System.Web.UI.Page
    Dim Obj As New Engine

    Private Sub InvoiceHeader_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("CompanyId") = Nothing Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            CType(UCHeader.FindControl("LblHeading"), Label).Text = "Invoice Header Templete"

            bind()
        End If
    End Sub
    Public Sub bind()
        Dim StrSQL As String
        Dim search As String = ""


        StrSQL = "Select * from InvoiceHeader where CompanyId=" & Session("CompanyId") & search & " Order By InvHeadId desc"

        Dim IntRecourtCount As Integer
        IntRecourtCount = Obj.BindGridControl(rep, StrSQL, LblHideUserCount, LblDispUserCount, LblNoUserCount, "Templete", "LnkPreviousRecord", "LnkNextRecord", "DrpOnRecord")
        If IntRecourtCount > 0 Then
            BtnDelete.Visible = True
        Else
            BtnDelete.Visible = False
        End If
    End Sub
    Private Function ValidateEnter() As String
        Dim StrError As String = ""
        Dim StrWhere As String = ""
        Dim cnt As Integer = 0


        cnt = Obj.GetField("select COUNT(InvHeadId) as cnt from InvoiceHeader where  CompanyId=" & Session("CompanyId") & StrWhere, "cnt")

        If cnt <> 0 Then
            LblMode.Text = "Edit"
            Dim Dt As New DataTable
            Dt = Obj.GetDataTable("select * from InvoiceHeader where  CompanyId=" & Session("CompanyId"))
            If Dt.Rows.Count > 0 Then
                LblId.Text = Dt(0)("InvHeadID")
            End If
        End If

        BtnCancel.Visible = True

        Return StrError
    End Function

    Private Sub BtnSubmit_Click(sender As Object, e As EventArgs) Handles BtnSubmit.Click
        Dim rs As New RecorsetUpdate
        Dim vals As String = ValidateEnter()
        If vals = "" Then

            If LblMode.Text = "Edit" Then
                rs.TableParameter("InvoiceHeader", "Edit", "InvHeadID=" & LblId.Text)
            Else
                rs.TableParameter("InvoiceHeader", "Add")
            End If
            rs.SetField("Address", TxtAddress.Text.Replace("'", "''"), "String")
            rs.SetField("ContactNumber", TxtMobile.Text.Replace("'", "''"), "Number")
            rs.SetField("CompanyId", Session("CompanyId"), "Number")
            rs.Execute()

            ClearText()
            bind()
            LblMessage.Text = "Sucessfully Updated!"
            LblMode.Text = "Add"
        Else
            lblErr.Text = vals
        End If
    End Sub
    Public Sub ClearText()
        TxtAddress.Text = ""
        TxtMobile.Text = ""

        LblMode.Text = "Add"
        LblMessage.Text = ""
        LblId.Text = ""
        lblErr.Text = ""
    End Sub

    Private Sub BtnCancel_Click(sender As Object, e As EventArgs) Handles BtnCancel.Click
        ClearText()
    End Sub
    Protected Sub BtnDelete_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnDelete.Click
        Dim strId As String = ""
        Dim StrErr As String = ""
        For i As Integer = 0 To rep.Items.Count - 1
            If CType(rep.Items(i).FindControl("ChkUserInfo"), CheckBox).Checked = True Then
                strId &= CType(rep.Items(i).FindControl("LblChk"), Label).Text & ","
            End If
        Next

        If strId <> "" Then

            strId = Left(strId, strId.Length - 1)

            Dim Sql As String = "Delete From InvoiceHeader where InvHeadId in (" & strId & ")"
            Obj.ExecuteCommand(Sql)
            bind()
            If StrErr <> "" Then
                StrErr = Left(StrErr, StrErr.Length - 1)
                LblMessage.Text = "Unable to delete " & StrErr
            Else
                LblMessage.Text = "Sucessfully Deleted !"
            End If
        End If

    End Sub


    Public Sub PreviousUserRecord(ByVal sender As Object, ByVal e As System.EventArgs)
        LblHideUserCount.Text = CInt(LblHideUserCount.Text) - 1
        Obj.PreviousRecord()
        bind()
    End Sub

    Public Sub NextUserRecord(ByVal sender As Object, ByVal e As System.EventArgs)
        LblHideUserCount.Text = CInt(LblHideUserCount.Text) + 1
        Obj.NextRecord()
        bind()
    End Sub

    Protected Sub OnUserRecord(ByVal sender As Object, ByVal e As System.EventArgs)
        LblHideUserCount.Text = CType(rep.Controls(rep.Controls.Count - 1).FindControl("DrpOnRecord"), DropDownList).SelectedValue
        Obj.OnRecord(LblHideUserCount.Text)
        bind()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        bind()
    End Sub

    Private Sub rep_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rep.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            If Not IsDBNull(e.Item.DataItem("Address")) Then CType(e.Item.FindControl("LblAdd"), Label).Text = e.Item.DataItem("Address")
            If Not IsDBNull(e.Item.DataItem("ContactNumber")) Then CType(e.Item.FindControl("LblContact"), Label).Text = e.Item.DataItem("ContactNumber")

        End If
    End Sub

    Private Sub rep_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rep.ItemCommand
        If e.CommandName = "Edit" Then
            Dim dt As New DataTable
            dt = Obj.GetDataTable("Select * from InvoiceHeader where InvHeadId=" & e.CommandArgument)
            If dt.Rows.Count > 0 Then
                TxtAddress.Text = dt.Rows(0).Item("Address")
                TxtMobile.Text = dt.Rows(0).Item("ContactNumber")

                LblMode.Text = "Edit"
                LblMessage.Text = ""
                LblId.Text = e.CommandArgument
            End If
        End If
    End Sub
End Class
