﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="InvoicenoMaster.aspx.vb" Inherits="InvoicenoMaster" %>

<!DOCTYPE html>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Src="~/Header.ascx" TagName="UCHeader" TagPrefix="UC1" %>
<%@ Register Src="~/footer.ascx" TagName="UCFooter" TagPrefix="UC1" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Invoice No Master</title>
    <link rel="icon" href="Images/thumb.png" />
    <link href="Css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="Css/css.css" rel="stylesheet" type="text/css" />
    <link href="Css/Style.css" rel="Stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div>
                <UC1:UCHeader ID="UCHeader" runat="server" />
            </div>
            <div class="container-fluid">
               <%-- <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="Updt">
                    <ProgressTemplate>
                        <asp:Image ID="imgloading1" runat="server" ImageUrl="~/Admin/Images/loader.gif"
                            CssClass="loading" />
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <asp:UpdatePanel ID="Updt" runat="server">
                    <ContentTemplate>--%>
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="boxshadow">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="title">
                                            Create Invoice No
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="email">
                                                <span class="red">*</span>Start Date:
                                            </label>
                                            <asp:TextBox ID="TxtStartDate" runat="server" class="form-control"></asp:TextBox>
                                          <ajax:CalendarExtender ID="CalStartDate" runat="server" TargetControlID="TxtStartDate" CssClass="cal" Format="dd-MMM-yyyy"></ajax:CalendarExtender>
                                            <asp:RequiredFieldValidator ID="req" runat="server" ControlToValidate="TxtStartDate" ErrorMessage="Enter Start Date !"
                                                InitialValue="" SetFocusOnError="true" Display="Dynamic" ValidationGroup="Save"></asp:RequiredFieldValidator>
                                        </div>
                                        <div class="form-group space">
                                            <label for="email">
                                                End Date
                                            </label>
                                           <asp:TextBox ID="TxtEndDate" runat="server" class="form-control" ></asp:TextBox>
                                            <ajax:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="TxtEndDate" CssClass="cal" Format="dd-MMM-yyyy"></ajax:CalendarExtender>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TxtEndDate" ErrorMessage="Enter End Date !"
                                                InitialValue="" SetFocusOnError="true" Display="Dynamic" ValidationGroup="Save"></asp:RequiredFieldValidator>
                                        </div>
                                        <div class="form-group">
                                            <label for="email">
                                                <span class="red">*</span>Invoive Prefix
                                            </label>
                                            <asp:TextBox ID="TxtInvPrefix" runat="server" class="form-control" ></asp:TextBox>
                                          <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TxtInvPrefix"  ErrorMessage="Enter Invoice Prefix"
                                                InitialValue="" SetFocusOnError="true" Display="Dynamic" ValidationGroup="Save"></asp:RequiredFieldValidator>
                                        </div>
                                        <div class="form-group">
                                            <label for="email">
                                                <span class="red">*</span>Invoive Start Digit
                                            </label>
                                            <asp:TextBox ID="TxtInvDigit" runat="server" class="form-control" ></asp:TextBox>
                                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="TxtInvDigit" FilterType="Numbers"></ajax:FilteredTextBoxExtender>
                                           
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <asp:Button runat="server" ID="BtnSubmit" Text="Submit" CssClass="dwnbtn" ValidationGroup="Save" />
                                        <asp:Button runat="server" ID="BtnCancel" Text="Cancel" CausesValidation="false"
                                            CssClass="dwnbtn" /><br />
                                        <asp:Label runat="server" ID="LblMode" Visible="false" Text="Add"></asp:Label>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label runat="server" ID="LblId" Visible="false"></asp:Label>
                                        <asp:Label runat="server" ID="LblMessage" CssClass="bold red"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                                <div class="boxshadow">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="title">
                                            Search Parameter
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label for="email">
                                                    Invoice No
                                                </label>
                                                <asp:TextBox runat="server" ID="TxtSerName" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-3 col-xs-12">
                                            <div class="form-group">
                                                <asp:Button runat="server" ID="btnSearch" Text="Search" CssClass="button1" CausesValidation="false" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 space1">
                                        <asp:Label ID="LblDispUserCount" runat="server" class="showtxt"></asp:Label>
                                        <asp:Label ID="LblHideUserCount" runat="server" Visible="false" class="showtxt"></asp:Label>
                                        <asp:Label ID="LblNoUserCount" runat="server" class="showtxt"></asp:Label>
                                    </div>
                                    <%--<asp:UpdatePanel ID="Updt" runat="server">

                                <ContentTemplate>--%>
                                    <div class="col-md-12 col-sm-12 col-xs-12 space1">
                                        <asp:Repeater runat="server" ID="rep">
                                            <HeaderTemplate>
                                                <table class="table">
                                                    <tr>
                                                        <th></th>
                                                        <th>Strat Date
                                                        </th>
                                                        <th>End Date
                                                        </th>
                                                        <th>Invoice Prefix
                                                        </th>
                                                        <th>Invoice Digit
                                                        </th>
                                                        <th></th>
                                                    </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td align="center">
                                                        <asp:CheckBox runat="server" ID="ChkUserInfo" />
                                                        <asp:Label runat="server" ID="LblChk" Text='<%#Container.DataItem("InvnoID")%>'
                                                            Visible="false"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="LblStartDate"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="LblEndDate"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="LblInvPrefix"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="LblInvDigits"></asp:Label>
                                                    </td>
                                                    <td align="center">
                                                        <asp:LinkButton ID="LnkEdit" runat="server" CommandName="Edit" CommandArgument='<%#Container.DataItem("InvnoID") %>'
                                                            Text="Edit" CausesValidation="false"></asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <tr>
                                                    <td colspan="6" align="center">
                                                        <asp:LinkButton ID="LnkPreviousRecord" Text="<< Prev" runat="server" OnClick="PreviousUserRecord"
                                                            CausesValidation="false" />&nbsp;
                                            <asp:DropDownList ID="DrpOnRecord" AutoPostBack="true" runat="server" OnSelectedIndexChanged="OnUserRecord" />
                                                        &nbsp;
                                            <asp:LinkButton ID="LnkNextRecord" Text="Next >>" runat="server" OnClick="NextUserRecord"
                                                CausesValidation="false" />
                                                    </td>
                                                </tr>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                    <%--</ContentTemplate>
                            </asp:UpdatePanel>--%>
                                    <div class="col-md-12 col-sm-12 col-xs-12 space1">
                                        <asp:Button runat="server" ID="BtnDelete" Text="Delete Invoice No(s)" CssClass="dwnbtn"
                                            CausesValidation="false" />
                                        <ajax:ConfirmButtonExtender ID="confDelete1" runat="server" TargetControlID="BtnDelete"
                                            DisplayModalPopupID="ModalPopupExtender1" />
                                        <ajax:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="BtnDelete"
                                            PopupControlID="PNL1" OkControlID="ButtonOk1" CancelControlID="ButtonCancel1"
                                            BackgroundCssClass="ModalBackground" />
                                        <asp:Panel ID="PNL1" runat="server" Style="display: none; padding: 20px;" CssClass="popup">
                                            <b>Are you sure you want to Delete this Invoice No ?</b>
                                            <br />
                                            <br />
                                            <div style="text-align: center;">
                                                <asp:Button ID="ButtonOk1" runat="server" Text="YES" CssClass="button" CausesValidation="false" />
                                                <asp:Button ID="ButtonCancel1" runat="server" Text="NO" CssClass="button" CausesValidation="false" />
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </div>
                        </div>
                   <%-- </ContentTemplate>
                </asp:UpdatePanel>--%>
            </div>
            <div>
                <UC1:UCFooter ID="UCFooter" runat="server" />
            </div>
        </div>
    </form>
</body>
</html>
