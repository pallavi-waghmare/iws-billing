﻿Imports System.Data
Imports System.IO
Imports System.Net

Partial Class Dashboard
    Inherits System.Web.UI.Page
    Dim obj As New Engine
    Dim crypt As New Cryptography
    Private Sub Dashboard_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("CompanyId") = Nothing Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then

            CType(UCHeader.FindControl("LblHeading"), Label).Text = "Dashboard"

            TxtFrom.Attributes.Add("Readonly", "Readonly")
            TxtTo.Attributes.Add("Readonly", "Readonly")

            calFrm.SelectedDate = DateTime.Today.Month & "-01" & "-" & DateTime.Today.Year
            calTo.SelectedDate = DateTime.Today.ToString("dd-MMM-yyyy")
            lblUrl.Text = crypt.Base64Encode("Dashboard")
            Session("Lstid") = Session("CustId")
            'bindDrp()
            'Bind()
        End If
    End Sub

    Public Sub bindDrp()
        TxtFrom.Attributes.Remove("Readonly")
        TxtTo.Attributes.Remove("Readonly")

        Dim strwhere As String = ""


        If Session("Role") = "Team" Then
            strwhere = " and ProductId in (select ProductId from TeamProduct where TeamId=" & Session("CustId") & ")"
        End If
        Dim sql As String = ""
        sql &= "select ProductId,ProductName from ProductMaster where CustomerId=" & Session("CompanyId").ToString & strwhere & ";"
        sql &= "select * from StatusMaster where CompanyId=0 or CompanyId=" & Session("CompanyId") & " order by StatusId;"
        sql &= "select BifurcationId,Bifurcation from BifurcationMaster where CompanyId=" & Session("CompanyId") & " order by Bifurcation;"
        sql &= "select SMSTempleteId,Title,ProductId from SMSTemplete where CompanyId=" & Session("CompanyId") & strwhere & " order by Title;"
        sql &= "select EmailTempleteId,Subject,ProductId from EmailTemplete where CompanyId=" & Session("CompanyId") & strwhere & " order by Subject;"
        sql &= "Select tm.TeamName,Tm.Teamid from Teammaster Tm left Join RoleMaster rm on tm.RoleId = rm.RoleId where tm.roleid >" & Session("RoleId") & " and Tm.CompanyId=" & Session("CompanyId") & " order by Tm.Teamid;"
        Dim ds As New DataSet
        ds = obj.GetDataset(sql)

        obj.BindListControl1(drpPro, ds.Tables(0), "ProductName", "ProductId")
        drpPro.Items.Insert(0, New ListItem("All Product", ""))
        If Session("Role") = "Team" Then
            obj.BindListControl(LstTeam, "Select tm.TeamName,Tm.Teamid from Teammaster Tm left Join RoleMaster rm on tm.RoleId = rm.RoleId where tm.roleid >" & Session("RoleId") & " and Tm.CompanyId=" & Session("CompanyId") & " order by Tm.Teamid;", "TeamName", "Teamid")
            Dim str As String() = Session("Company").split("-")
            LstTeam.Items.Insert(0, New ListItem(str(0), Session("CustId")))
            Dim dt As DataTable = obj.GetDataTable("Select ROW_NUMBER() over( order by (Tm.Teamid) ) id,tm.TeamName,Tm.Teamid,tm.Roleid from Teammaster Tm left Join RoleMaster rm on tm.RoleId = rm.RoleId where tm.roleid >" & Session("RoleId") & " and Tm.CompanyId=" & Session("CompanyId") & " order by Tm.Teamid;")
            LstTeam.SelectedValue = Session("CustId")
            'Dim ky As DataColumn = New DataColumn(1)
            'ky(0) = dt.Columns("id")
            dt.PrimaryKey = New DataColumn() {dt.Columns("id")}
            Dim dtTeam As DataTable = obj.GetDataTable("Select * from TeamMaster where CompanyId=" & Session("CompanyId") & "And Teamid != " & Session("CustId"))
            Dim branch As String = obj.GetField("Select Branchid from Teammaster where teamid= " & Session("CustId"), "Branchid")
            Dim branchsplt() As String = branch.Split(",")
            For i As Integer = 0 To dtTeam.Rows.Count - 1
                Dim branchTeam() As String = dtTeam.Rows(i).Item("Branchid").ToString().Split(",")
                For j As Integer = 0 To branchsplt.Length - 1
                    For k As Integer = 0 To branchTeam.Length - 1
                        If branchsplt(j) = branchTeam(k) Then
                            Dim name As String = dtTeam.Rows(i).Item("TeamName")
                            Dim cnt As Integer = obj.GetField("Select Count(Tm.Teamid) as cnt from Teammaster Tm left Join RoleMaster rm on tm.RoleId = rm.RoleId where tm.roleid >" & Session("RoleId") & " and Tm.CompanyId=" & Session("CompanyId") & "and Tm.Teamid = " & dtTeam.Rows(i).Item("Teamid"), "cnt")
                            'Dim foundRow As DataColumn = dt()
                            If cnt = 0 And dtTeam.Rows(i).Item("Roleid") > Session("RoleId") Then
                                LstTeam.Items.Insert(dt.Rows.Count + 1, New ListItem(dtTeam.Rows(i).Item("TeamName"), dtTeam.Rows(i).Item("TeamId")))
                            End If
                        End If
                    Next
                Next
            Next
            Session("Lstid") = Session("CustId")
        Else
            DivLstitem.Attributes.Add("style", "display:none")
        End If
        ViewState("Statuus") = ds.Tables(1)
        ViewState("Bifurcation") = ds.Tables(2)
        ViewState("SMS") = ds.Tables(3)
        ViewState("Email") = ds.Tables(4)



    End Sub

    Public Sub Bind()
        Dim str As String = ""
        Dim strWhere = "", StrDate As String = "", StrDateDa As String = ""
        TxtFrom.Attributes.Remove("Readonly")
        TxtTo.Attributes.Remove("Readonly")
        If drpPro.SelectedValue <> "" Then
            strWhere &= " and PM.ProductId=" & drpPro.SelectedValue
        End If


        If TxtFrom.Text <> "" Then
            StrDate &= " and LastUpdated>='" & TxtFrom.Text.Replace("'", "''") & "'"
            StrDateDa &= " and la.Createdate>='" & TxtFrom.Text & "'"
        Else
            StrDate &= " and LastUpdated>='" & DateTime.Today.Month & "-01" & "-" & DateTime.Today.Year & "'"
            StrDateDa &= " and la.Createdate>='" & DateTime.Today.Month & "-01" & "-" & DateTime.Today.Year & "'"

        End If


        If TxtTo.Text <> "" Then
            StrDate &= " and LastUpdated<=DATEADD(day,1,'" & TxtTo.Text.Replace("'", "''") & "')"
            StrDateDa &= " and la.Createdate<=DATEADD(day,1,'" & TxtTo.Text & "')"
        Else
            StrDate &= " and LastUpdated<=DATEADD(day,1,'" & DateTime.Today.ToString("dd-MMM-yyyy") & "')"
            StrDateDa &= " and la.Createdate<=DATEADD(day,1,'" & DateTime.Today.ToString("dd-MMM-yyyy") & "')"
        End If

        Dim strwhereLead As String = ""

        If Session("Role") = "Team" Then
            strWhere &= " and PM.ProductId in (select ProductId from TeamProduct where TeamId=" & Session("CustId") & ")"
            strwhereLead &= " and PickBy in (" & Session("Lstid") & ")"
        End If


        'str &= "select PM.ProductName,"
        'str &= "(select COUNT(LeadId) As cnt from LeadsMaster where ProductId=PM.ProductId And LeadStatus=1 " & StrDate & strwhereLead & ") as Pending,"
        'str &= "(select COUNT(LeadId) as cnt from LeadsMaster where ProductId=PM.ProductId and LeadStatus=2 " & StrDate & strwhereLead & ") as InProcess,"
        'str &= "(select COUNT(LeadId) As cnt from LeadsMaster where ProductId=PM.ProductId And LeadStatus=3 " & StrDate & strwhereLead & ") as Sold,"
        'str &= "(select COUNT(LeadId) as cnt from LeadsMaster where ProductId=PM.ProductId and LeadStatus=4 " & StrDate & strwhereLead & ") as Lost,"
        'str &= "(select COUNT(LeadId) As cnt from LeadsMaster where ProductId=PM.ProductId " & StrDate & strwhereLead & ") As Total "
        'str &= "from ProductMaster PM where PM.Isarchieved = 0 and PM.CustomerId=" & Session("CompanyId").ToString & strWhere
        Dim Status As String = "select * from StatusMaster where (CompanyId=0 or CompanyId=" & Session("CompanyId") & ") and StatusId not in (select StatusId from CustomerStatus where CustomerId=" & Session("CompanyId") & " and IsActive=0) order by StatusId"

        Dim dtsta As DataTable = obj.GetDataTable(Status)
        str &= "select PM.ProductName,"
        For i As Integer = 0 To dtsta.Rows.Count - 1

            Dim name As String = ""
            If Not IsDBNull(dtsta.Rows(i).Item("Abbreviation")) And dtsta.Rows(i).Item("Abbreviation") <> "" Then name = dtsta.Rows(i).Item("Abbreviation") Else name = dtsta.Rows(i).Item("Status")
            name = name.Replace(" ", "")
            str &= "(select COUNT(LeadId) As cnt from LeadsMaster where ProductId=PM.ProductId And LeadStatus= " & dtsta.Rows(i).Item("StatusId") & StrDate & strwhereLead & ") as " & name & "0" & dtsta.Rows(i).Item("StatusId") & ","

        Next
        str &= "(select COUNT(LeadId) As cnt from LeadsMaster where ProductId=PM.ProductId " & StrDate & strwhereLead & ") As Total "
        str &= "from ProductMaster PM where PM.Isarchieved = 0 and PM.CustomerId=" & Session("CompanyId").ToString & strWhere



        Dim led As New LeadEngine
        Dim dt As New DataTable
        dt = obj.GetDataTable(str)
        Dim strhtml As String = ""

        strhtml = led.GetleadsCounts(dt, Session("CompanyId"))
        lblLeads.Text = strhtml
        'rep.DataSource = dt.DefaultView
        'rep.DataBind()

        If Session("Role") = "Admin" Then

            Dim sqlTeam As String = ""
            Dim dtTeam As New DataTable

            sqlTeam &= "select TM.TeamName,"
            For i As Integer = 0 To dtsta.Rows.Count - 1
                Dim name As String = ""

                If Not IsDBNull(dtsta.Rows(i).Item("Abbreviation")) And dtsta.Rows(i).Item("Abbreviation") <> "" Then name = dtsta.Rows(i).Item("Abbreviation") Else name = dtsta.Rows(i).Item("Status")
                name = name.Replace(" ", "")
                sqlTeam &= "(select COUNT(LeadId) As cnt from LeadsMaster where PickBy=TM.TeamId And LeadStatus= " & dtsta.Rows(i).Item("StatusId") & StrDate & ") as '" & name & "',"
            Next
            'sqlTeam &= "(select COUNT(LeadId) as cnt from LeadsMaster where PickBy=TM.TeamId and LeadStatus=2 " & StrDate & ") as InProcess,"
            'sqlTeam &= "(select COUNT(LeadId) As cnt from LeadsMaster where PickBy=TM.TeamId And LeadStatus=3 " & StrDate & ") as Sold,"
            'sqlTeam &= "(select COUNT(LeadId) as cnt from LeadsMaster where PickBy=TM.TeamId and LeadStatus=4 " & StrDate & ") as Lost,"
            sqlTeam &= "(select COUNT(LeadId) As cnt from LeadsMaster where PickBy=TM.TeamId " & StrDate & ") As Total "
            sqlTeam &= "from TeamMaster TM where TM.CompanyId=" & Session("CompanyId").ToString

            dtTeam = obj.GetDataTable(sqlTeam)

            Dim strTeamHtml As String = ""
            strTeamHtml = led.GetleadsCounts(dtTeam, Session("CompanyId"))
            lblTeamsDetails.Text = strTeamHtml
            'repTeam.DataSource = dtTeam
            'repTeam.DataBind()

            TblTeam.Visible = True
        Else

            Dim sqlTeam As String = ""
            Dim dtTeam As New DataTable

            sqlTeam &= "select TM.TeamName,"
            For i As Integer = 0 To dtsta.Rows.Count - 1
                Dim name As String = ""

                If Not IsDBNull(dtsta.Rows(i).Item("Abbreviation")) And dtsta.Rows(i).Item("Abbreviation") <> "" Then name = dtsta.Rows(i).Item("Abbreviation") Else name = dtsta.Rows(i).Item("Status")
                name = name.Replace(" ", "")
                sqlTeam &= "(select COUNT(LeadId) As cnt from LeadsMaster where PickBy=TM.TeamId And LeadStatus= " & dtsta.Rows(i).Item("StatusId") & StrDate & ") as '" & name & "',"
            Next
            'sqlTeam &= "(select COUNT(LeadId) as cnt from LeadsMaster where PickBy=TM.TeamId and LeadStatus=2 " & StrDate & ") as InProcess,"
            'sqlTeam &= "(select COUNT(LeadId) As cnt from LeadsMaster where PickBy=TM.TeamId And LeadStatus=3 " & StrDate & ") as Sold,"
            'sqlTeam &= "(select COUNT(LeadId) as cnt from LeadsMaster where PickBy=TM.TeamId and LeadStatus=4 " & StrDate & ") as Lost,"
            sqlTeam &= "(select COUNT(LeadId) As cnt from LeadsMaster where PickBy=TM.TeamId " & StrDate & ") As Total "
            sqlTeam &= "from TeamMaster TM where TM.CompanyId=" & Session("CompanyId").ToString & "and TM.Teamid in (" & Session("Lstid") & ")"

            dtTeam = obj.GetDataTable(sqlTeam)

            Dim strTeamHtml As String = ""
            strTeamHtml = led.GetleadsCounts(dtTeam, Session("CompanyId"))
            lblTeamsDetails.Text = strTeamHtml
            'repTeam.DataSource = dtTeam
            'repTeam.DataBind()

            TblTeam.Visible = True

        End If
        If Session("Role") = "Admin" Then
            Dim sqlTeamDa As String = ""
            Dim dtTeam As New DataTable
            Dim strCreDate As String = " "

            If TxtFrom.Text <> "" And TxtTo.Text <> "" Then

                'Else
                '    StrDateDa &= " and la.Createdate>=getdate()"
                '    StrDateDa &= " and la.Createdate<=DATEADD(day,1,getdate())"
            End If




            sqlTeamDa &= "select TM.TeamName,"
            For i As Integer = 0 To dtsta.Rows.Count - 1
                Dim name As String = ""

                If Not IsDBNull(dtsta.Rows(i).Item("Abbreviation")) And dtsta.Rows(i).Item("Abbreviation") <> "" Then name = dtsta.Rows(i).Item("Abbreviation") Else name = dtsta.Rows(i).Item("Status")
                name = name.Replace(" ", "")
                sqlTeamDa &= "(select COUNT(la.LeadId) As cnt from LeadAlert la left Join LeadsMaster lm on la.leadid = lm.leadId   where TeamId=TM.TeamId and   Statusid= " & dtsta.Rows(i).Item("StatusId") & StrDateDa & " ) as '" & name & "',"
            Next
            'sqlTeam &= "(select COUNT(LeadId) as cnt from LeadsMaster where PickBy=TM.TeamId and LeadStatus=2 " & StrDate & ") as InProcess,"
            'sqlTeam &= "(select COUNT(LeadId) As cnt from LeadsMaster where PickBy=TM.TeamId And LeadStatus=3 " & StrDate & ") as Sold,"
            'sqlTeam &= "(select COUNT(LeadId) as cnt from LeadsMaster where PickBy=TM.TeamId and LeadStatus=4 " & StrDate & ") as Lost,"
            sqlTeamDa &= "(select COUNT( la.LeadId) As cnt from LeadAlert la left Join LeadsMaster lm on la.leadid = lm.leadId where TeamId=TM.TeamId " & StrDateDa & ") As Total "
            sqlTeamDa &= "from TeamMaster TM where TM.CompanyId=" & Session("CompanyId").ToString

            dtTeam = obj.GetDataTable(sqlTeamDa)

            Dim strTeamHtml As String = ""
            strTeamHtml = led.GetleadsCounts(dtTeam, Session("CompanyId"))
            lblTeamDailyReport.Text = strTeamHtml
            'repTeam.DataSource = dtTeam
            'repTeam.DataBind()

            TblDailyReport.Visible = True
        Else

            Dim sqlTeamDa As String = ""
            Dim dtTeam As New DataTable
            Dim strCreDate As String = " "

            If TxtFrom.Text <> "" And TxtTo.Text <> "" Then

                'Else
                '    StrDateDa &= " and la.Createdate>=getdate()"
                '    StrDateDa &= " and la.Createdate<=DATEADD(day,1,getdate())"
            End If




            sqlTeamDa &= "select TM.TeamName,"
            For i As Integer = 0 To dtsta.Rows.Count - 1
                Dim name As String = ""

                If Not IsDBNull(dtsta.Rows(i).Item("Abbreviation")) And dtsta.Rows(i).Item("Abbreviation") <> "" Then name = dtsta.Rows(i).Item("Abbreviation") Else name = dtsta.Rows(i).Item("Status")
                name = name.Replace(" ", "")
                sqlTeamDa &= "(select COUNT(la.LeadId) As cnt from LeadAlert la left Join LeadsMaster lm on la.leadid = lm.leadId   where TeamId=TM.TeamId and   Statusid= " & dtsta.Rows(i).Item("StatusId") & StrDateDa & " ) as '" & name & "',"
            Next
            'sqlTeam &= "(select COUNT(LeadId) as cnt from LeadsMaster where PickBy=TM.TeamId and LeadStatus=2 " & StrDate & ") as InProcess,"
            'sqlTeam &= "(select COUNT(LeadId) As cnt from LeadsMaster where PickBy=TM.TeamId And LeadStatus=3 " & StrDate & ") as Sold,"
            'sqlTeam &= "(select COUNT(LeadId) as cnt from LeadsMaster where PickBy=TM.TeamId and LeadStatus=4 " & StrDate & ") as Lost,"
            sqlTeamDa &= "(select COUNT( la.LeadId) As cnt from LeadAlert la left Join LeadsMaster lm on la.leadid = lm.leadId where TeamId=TM.TeamId " & StrDateDa & ") As Total "
            sqlTeamDa &= "from TeamMaster TM where TM.CompanyId=" & Session("CompanyId").ToString & "and TM.Teamid in (" & Session("Lstid") & ")"

            dtTeam = obj.GetDataTable(sqlTeamDa)

            Dim strTeamHtml As String = ""
            strTeamHtml = led.GetleadsCounts(dtTeam, Session("CompanyId"))
            lblTeamDailyReport.Text = strTeamHtml
            'repTeam.DataSource = dtTeam
            'repTeam.DataBind()

            TblDailyReport.Visible = True
        End If

        Dim dtAlert As New DataTable
        Dim strAlert As String = ""

        Dim strWhereAlert As String = ""

        If Session("Role") = "Team" Then
            strWhereAlert = " and LA.TeamId in (" & Session("Lstid") & ")"
        End If

        strAlert &= "SELECT top 10 LM.LeadId,LM.Name,LM.EmailAddress,LM.Mobile,LM.Query,SM.Status,LA.NextFollow as FollowUpDate,"

        If Session("Role") <> "Team" Then
            strAlert &= " tm.TeamName as PickedBy,"
        End If
        strAlert &= " LM.Remark,LM.CreateDate as LastUpdated,Isnull((Select Count(leadId) from LeadAlert le  where le.LeadId= lm.LeadId Group By le.LeadId),0) as LeadCount"
        strAlert &= " FROM LeadAlert LA"
        strAlert &= " LEFT JOIN LeadsMaster LM ON LM.LeadId=LA.LeadId"
        strAlert &= " LEFT JOIN ProductMaster PM ON PM.ProductId=LM.ProductId"
        strAlert &= " LEFT JOIN MediumMaster MM ON MM.MediumId=LM.MediumId"
        strAlert &= " LEFT JOIN BifurcationMaster BM ON BM.BifurcationId=LA.BifurcationId"
        strAlert &= " LEFT JOIN StatusMaster SM ON SM.StatusId=LM.LeadStatus"
        If Session("Role") <> "Team" Then
            strAlert &= " Left Join TeamMaster tm on la.teamid = Tm.Teamid"
        Else
            strAlert &= " Left Join TeamMaster tm on la.teamid = Tm.Teamid"
        End If
        strAlert &= " WHERE tm.CompanyId=" & Session("CompanyId") & " AND LA.NextFollow>='" & DateTime.Today.ToString("dd-MMM-yyyy") & "' AND LA.NextFollow<='" & DateAdd(DateInterval.Day, 1, DateTime.Today).ToString("dd-MMM-yyyy") & "'" & strWhereAlert

        dtAlert = obj.GetDataTable(strAlert)

        If dtAlert.Rows.Count = 0 Then
            lnkView.Visible = False
        End If
        Dim StrString As String = led.GetleadsAlert(dtAlert, Session("CompanyId"))
        lblTodayAlert.Text = StrString
        'repAlert.DataSource = dtAlert
        'repAlert.DataBind()
        If TxtFrom.Text <> "" And TxtTo.Text <> "" Then
            calFrm.SelectedDate = TxtFrom.Text
            calTo.SelectedDate = TxtTo.Text
        End If
    End Sub

    'Private Sub rep_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rep.ItemDataBound
    '    If e.Item.ItemIndex >= 0 Then
    '        'If Not IsDBNull(e.Item.DataItem("ProductName")) Then CType(e.Item.FindControl("lblPro"), Label).Text = e.Item.DataItem("ProductName")
    '        'If Not IsDBNull(e.Item.DataItem("Pending")) Then CType(e.Item.FindControl("lblPend"), Label).Text = e.Item.DataItem("Pending")
    '        'If Not IsDBNull(e.Item.DataItem("InProcess")) Then CType(e.Item.FindControl("lblInProc"), Label).Text = e.Item.DataItem("InProcess")
    '        'If Not IsDBNull(e.Item.DataItem("Sold")) Then CType(e.Item.FindControl("lblSold"), Label).Text = e.Item.DataItem("Sold")
    '        'If Not IsDBNull(e.Item.DataItem("Lost")) Then CType(e.Item.FindControl("lblLost"), Label).Text = e.Item.DataItem("Lost")
    '        'If Not IsDBNull(e.Item.DataItem("Total")) Then CType(e.Item.FindControl("lblTot"), Label).Text = e.Item.DataItem("Total")
    '    End If
    'End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Bind()
    End Sub

    'Private Sub repTeam_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles repTeam.ItemDataBound
    '    If e.Item.ItemIndex >= 0 Then
    '        If Not IsDBNull(e.Item.DataItem("TeamName")) Then CType(e.Item.FindControl("lblTeam"), Label).Text = e.Item.DataItem("TeamName")
    '        If Not IsDBNull(e.Item.DataItem("Pending")) Then CType(e.Item.FindControl("lblPend"), Label).Text = e.Item.DataItem("Pending")
    '        If Not IsDBNull(e.Item.DataItem("InProcess")) Then CType(e.Item.FindControl("lblInProc"), Label).Text = e.Item.DataItem("InProcess")
    '        If Not IsDBNull(e.Item.DataItem("Sold")) Then CType(e.Item.FindControl("lblSold"), Label).Text = e.Item.DataItem("Sold")
    '        If Not IsDBNull(e.Item.DataItem("Lost")) Then CType(e.Item.FindControl("lblLost"), Label).Text = e.Item.DataItem("Lost")
    '        If Not IsDBNull(e.Item.DataItem("Total")) Then CType(e.Item.FindControl("lblTot"), Label).Text = e.Item.DataItem("Total")
    '    End If
    'End Sub

    'Private Sub repAlert_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles repAlert.ItemDataBound
    '    If e.Item.ItemIndex >= 0 Then
    '        If Not IsDBNull(e.Item.DataItem("Name")) Then CType(e.Item.FindControl("lblName"), Label).Text = e.Item.DataItem("Name")
    '        If Not IsDBNull(e.Item.DataItem("EmailAddress")) Then CType(e.Item.FindControl("lblEmail"), Label).Text = e.Item.DataItem("EmailAddress")
    '        If Not IsDBNull(e.Item.DataItem("Mobile")) Then CType(e.Item.FindControl("lblMob"), Label).Text = e.Item.DataItem("Mobile")
    '        If Not IsDBNull(e.Item.DataItem("Query")) Then CType(e.Item.FindControl("lblQuery"), Label).Text = e.Item.DataItem("Query")
    '        If Not IsDBNull(e.Item.DataItem("CreateDate")) Then CType(e.Item.FindControl("lblCDate"), Label).Text = obj.GetFormattedDateTime(e.Item.DataItem("CreateDate"))
    '        If Not IsDBNull(e.Item.DataItem("LastUpdated")) Then CType(e.Item.FindControl("lblUpDate"), Label).Text = obj.GetFormattedDateTime(e.Item.DataItem("LastUpdated"))
    '        If Not IsDBNull(e.Item.DataItem("ProductName")) Then CType(e.Item.FindControl("lblPro"), Label).Text = e.Item.DataItem("ProductName")
    '        If Not IsDBNull(e.Item.DataItem("MediumName")) Then CType(e.Item.FindControl("lblMed"), Label).Text = e.Item.DataItem("MediumName")

    '        CType(e.Item.FindControl("lnkAlert"), LinkButton).PostBackUrl = "~/AddFollowUp.aspx?Id=" & e.Item.DataItem("LeadId") & "&uri=" & lblUrl.Text
    '        CType(e.Item.FindControl("lnkView"), LinkButton).PostBackUrl = "~/ViewLead.aspx?Id=" & e.Item.DataItem("LeadId") & "&uri=" & lblUrl.Text

    '        Dim drpSms, drpEmail As New DropDownList


    '        drpSms = CType(e.Item.FindControl("drpSms"), DropDownList)
    '        drpEmail = CType(e.Item.FindControl("drpEmail"), DropDownList)


    '        Dim dtSMS, dtEmail As New DataTable
    '        dtSMS = ViewState("SMS")
    '        dtEmail = ViewState("Email")

    '        Dim dtSmsTemp, dtEmailTemp As New DataTable

    '        Dim dvSMS As New DataView(dtSMS, "ProductId=" & e.Item.DataItem("ProductId"), "", DataViewRowState.CurrentRows)
    '        Dim dvEmail As New DataView(dtEmail, "ProductId=" & e.Item.DataItem("ProductId"), "", DataViewRowState.CurrentRows)

    '        dtSmsTemp = dvSMS.ToTable
    '        dtEmailTemp = dvEmail.ToTable

    '        obj.BindListControl1(drpSms, dtSmsTemp, "Title", "SMSTempleteId")
    '        drpSms.Items.Insert(0, New ListItem("Select Templete", ""))

    '        obj.BindListControl1(drpEmail, dtEmailTemp, "Subject", "EmailTempleteId")
    '        drpEmail.Items.Insert(0, New ListItem("Select Templete", ""))


    '        If Not IsDBNull(e.Item.DataItem("Status")) Then CType(e.Item.FindControl("lblStatus"), Label).Text = e.Item.DataItem("Status")
    '        If Not IsDBNull(e.Item.DataItem("LeadStatus")) Then CType(e.Item.FindControl("lblStatusId"), Label).Text = e.Item.DataItem("LeadStatus")

    '        '=====================================Validations=============================================

    '        CType(e.Item.FindControl("reqSMS"), RequiredFieldValidator).ValidationGroup = "SendSMS" & e.Item.ItemIndex
    '        CType(e.Item.FindControl("btnSmsOk"), Button).ValidationGroup = "SendSMS" & e.Item.ItemIndex

    '        CType(e.Item.FindControl("reqEmail"), RequiredFieldValidator).ValidationGroup = "SendEmail" & e.Item.ItemIndex
    '        CType(e.Item.FindControl("btnEmailOk"), Button).ValidationGroup = "SendEmail" & e.Item.ItemIndex

    '        '=====================================Custom Field=============================================

    '    End If
    'End Sub

    'Private Sub repAlert_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles repAlert.ItemCommand
    '    If e.CommandName = "SMS" Then

    '        Dim Dts As New DataTable
    '        Dim SMSUrl As String = ""
    '        Dim sql As String = ""

    '        sql &= "select * from SMSAlertSetting where CompanyId=" & Session("CompanyId") & ";"
    '        sql &= "select * from SMSTemplete where SMSTempleteId=" & CType(e.Item.FindControl("drpSms"), DropDownList).SelectedValue

    '        Dim ds As New DataSet

    '        ds = obj.GetDataset(sql)

    '        Dts = ds.Tables(0)

    '        If Dts.Rows.Count > 0 Then
    '            SMSUrl = Dts(0)("SMSUrl")
    '        End If

    '        Dim txtMob As New Label
    '        txtMob = CType(e.Item.FindControl("lblMob"), Label)

    '        If txtMob.Text <> "" Then
    '            Dim Mobile As String()

    '            Dim Url As String
    '            Url = SMSUrl

    '            Dim smsbody As String = ""

    '            If ds.Tables(1).Rows.Count > 0 Then
    '                smsbody = ds.Tables(1)(0)("SMSBody")
    '            End If


    '            If txtMob.Text.Contains(",") Then
    '                Mobile = txtMob.Text.Split(",")
    '                For I As Integer = 0 To Mobile.Length - 1
    '                    Url = Url.Replace("<mobile>", Mobile(I))
    '                    Url = Url.Replace("<message>", smsbody)

    '                    SendSMS(Url)
    '                    Url = SMSUrl
    '                Next
    '            Else

    '                Url = Url.Replace("<mobile>", txtMob.Text)
    '                Url = Url.Replace("<message>", smsbody)

    '                SendSMS(Url)
    '                Url = SMSUrl
    '            End If

    '        End If
    '        Dim rs As New RecorsetUpdate
    '        rs.TableParameter("LeadAlert", "Add")
    '        rs.SetField("LeadId", e.CommandArgument, "Number")
    '        rs.SetField("TeamId", Session("CustId"), "Number")
    '        rs.SetField("ActionId", 1, "Number")
    '        rs.SetField("StatusId", CType(e.Item.FindControl("lblStatusId"), Label).Text, "Number")
    '        rs.Execute()

    '        Dim rsHis As New RecorsetUpdate
    '        rsHis.TableParameter("SMSHistory", "Add")
    '        rsHis.SetField("TempleteId", CType(e.Item.FindControl("drpSms"), DropDownList).SelectedValue, "Number")
    '        rsHis.SetField("LeadId", e.CommandArgument, "Number")
    '        rsHis.SetField("TeamId", Session("CustId"), "Number")
    '        rsHis.SetField("CompanyId", Session("CompanyId"), "Number")
    '        rsHis.SetField("Mobile", txtMob.Text, "String")
    '        rsHis.Execute()
    '        Bind()
    '    End If
    '    If e.CommandName = "Email" Then

    '        Dim Dt As New DataTable
    '        Dim FromEmail As String = ""
    '        Dim FromName As String = Session("Company")
    '        Dim EmailHost As String = ""
    '        Dim EmailUserName As String = ""
    '        Dim EmailPassword As String = ""

    '        Dim ds As New DataSet

    '        Dim sql As String = ""

    '        sql &= "select top 1 * from EmailAlertSetting where CompanyId=" & Session("CompanyId") & ";"
    '        sql &= "select * from EmailTemplete where EmailTempleteId=" & CType(e.Item.FindControl("drpEmail"), DropDownList).SelectedValue


    '        ds = obj.GetDataset(sql)
    '        Dt = ds.Tables(0)
    '        If Dt.Rows.Count > 0 Then
    '            FromEmail = Dt(0)("FromEmailID")
    '            EmailHost = Dt(0)("MailHost")
    '            EmailUserName = Dt(0)("MailUserName")
    '            EmailPassword = Dt(0)("MailPassword")
    '        End If

    '        Dim strSubject = "", strBody As String = ""

    '        If ds.Tables(1).Rows.Count > 0 Then
    '            strSubject = ds.Tables(1)(0)("Subject")
    '            strBody = ds.Tables(1)(0)("body")
    '        End If

    '        Dim AlertEmail As String = CType(e.Item.FindControl("lblEmail"), Label).Text

    '        Dim EmailID As String()
    '        If AlertEmail.Contains(",") Then
    '            EmailID = AlertEmail.Split(",")
    '            For I As Integer = 0 To EmailID.Length - 1
    '                obj.iSendMail(EmailUserName, EmailPassword, EmailHost, FromEmail, EmailID(I), strSubject, strBody, FromName)
    '            Next
    '        Else
    '            obj.iSendMail(EmailUserName, EmailPassword, EmailHost, FromEmail, AlertEmail, strSubject, strBody, FromName)
    '        End If

    '        Dim rs As New RecorsetUpdate
    '        rs.TableParameter("LeadAlert", "Add")
    '        rs.SetField("LeadId", e.CommandArgument, "Number")
    '        rs.SetField("TeamId", Session("CustId"), "Number")
    '        rs.SetField("ActionId", 1, "Number")
    '        rs.SetField("StatusId", CType(e.Item.FindControl("lblStatusId"), Label).Text, "Number")
    '        rs.Execute()

    '        Dim rsHis As New RecorsetUpdate
    '        rsHis.TableParameter("EmailHistory", "Add")
    '        rsHis.SetField("TempleteId", CType(e.Item.FindControl("drpEmail"), DropDownList).SelectedValue, "Number")
    '        rsHis.SetField("LeadId", e.CommandArgument, "Number")
    '        rsHis.SetField("TeamId", Session("CustId"), "Number")
    '        rsHis.SetField("CompanyId", Session("CompanyId"), "Number")
    '        rsHis.SetField("EmailId", AlertEmail, "String")
    '        rsHis.Execute()

    '        Bind()
    '    End If
    'End Sub
    Public Sub SendSMS(ByVal SMSURL As String)
        Try
            Dim objURI As Uri = New Uri(SMSURL)

            Dim objWebRequest As WebRequest = WebRequest.Create(objURI)
            Dim objWebResponse As WebResponse = objWebRequest.GetResponse()
            Dim objStream As Stream = objWebResponse.GetResponseStream()
            Dim objStreamReader As StreamReader = New StreamReader(objStream)
            Dim strHTML As String = objStreamReader.ReadToEnd

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub LstTeam_SelectedIndexChanged(sender As Object, e As EventArgs) Handles LstTeam.SelectedIndexChanged
        Dim lstid As String = ""
        For i As Integer = 0 To LstTeam.Items.Count - 1
            If LstTeam.Items(i).Selected = True Then
                lstid &= LstTeam.Items(i).Value & ","
            End If

        Next
        If lstid <> "" Then
            lstid = Left(lstid, lstid.Length - 1)
            Session("Lstid") = lstid
            Bind()
        End If

    End Sub

End Class
