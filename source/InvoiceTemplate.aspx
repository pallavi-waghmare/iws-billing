﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="InvoiceTemplate.aspx.vb" Inherits="InvoiceTemplate" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Src="~/Header.ascx" TagName="UCHeader" TagPrefix="UC1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="HTMLEditor" %>
<%@ Register Src="~/footer.ascx" TagName="UCFooter" TagPrefix="UC1" %>
<%@ Register TagPrefix="FCKeditorV2" Namespace="FredCK.FCKeditorV2" Assembly="FredCK.FCKeditorV2" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Invoice Header Templete</title>
    <link rel="icon" href="Images/thumb.png" />
    <link href="Css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="Css/css.css" rel="stylesheet" type="text/css" />
    <link href="Css/Style.css" rel="Stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
            <UC1:UCHeader ID="UCHeader" runat="server" />
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-7 col-sm-7 col-xs-12">
                    <div class="boxshadow">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="title">
                                Create Invoice Templete
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 padding">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="Invoice">
                                        <span class="red">*</span>Compnay Address
                                    </label>
                                    <asp:TextBox ID="TxtAddress" runat="server" class="form-control" TextMode="MultiLine" Height="200px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TxtAddress"
                                        ErrorMessage="<br/>Enter Address !" InitialValue="" SetFocusOnError="true" Display="Dynamic"
                                        ValidationGroup="Save"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="Invoice">
                                        <span class="red">*</span>Company Phone Number
                                    </label>
                                    <asp:TextBox ID="TxtMobile" runat="server" class="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="req" runat="server" ControlToValidate="TxtMobile" ErrorMessage="<br/>Enter Contact Number"
                                        InitialValue="" SetFocusOnError="true" Display="Dynamic" ValidationGroup="Save"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                        <%--<div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="Invoice">
                                    <span class="red">*</span>Body
                                </label>
                                <%--<HTMLEditor:Editor ID="txtMessage" runat="server" Visible="false" />
                                <FCKeditorV2:FCKeditor ID="txtMessage" runat="server" Height="500px" Width="100%"
                                    ToolbarSet="Basic" BasePath="">
                                </FCKeditorV2:FCKeditor>
                                <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtMessage" ErrorMessage="<br/>Enter Body" InitialValue="" Display="Dynamic" ValidationGroup="Save"></asp:RequiredFieldValidator>
                            </div>
                        </div>--%>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <asp:Label ID="lblErr" runat="server" ForeColor="Red"></asp:Label><br />
                                <asp:Button runat="server" ID="BtnSubmit" Text="Submit" CssClass="dwnbtn" ValidationGroup="Save" />
                                <asp:Button runat="server" ID="BtnCancel" Text="Cancel" CausesValidation="false"
                                    CssClass="dwnbtn" /><br />
                                <asp:Label runat="server" ID="LblMode" Visible="false" Text="Add"></asp:Label>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                            <div class="form-group">
                                <asp:Label runat="server" ID="LblId" Visible="false"></asp:Label>
                                <asp:Label runat="server" ID="LblMessage" CssClass="bold red"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 col-sm-5 col-xs-12">
                    <div class="boxshadow">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="title">
                                Search Parameter
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 padding">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="Invoice">
                                        Keyword
                                    </label>
                                    <asp:TextBox runat="server" ID="TxtSerName" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <asp:Button runat="server" ID="btnSearch" Text="Search" CssClass="button" CausesValidation="false" />
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 space1">
                            <asp:Label ID="LblDispUserCount" runat="server" class="showtxt"></asp:Label>
                            <asp:Label ID="LblHideUserCount" runat="server" Visible="false" class="showtxt"></asp:Label>
                            <asp:Label ID="LblNoUserCount" runat="server" class="showtxt"></asp:Label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 space1">
                            <asp:Repeater runat="server" ID="rep">
                                <HeaderTemplate>
                                    <table class="table">
                                        <tr>
                                            <th>
                                            </th>
                                            <th>
                                                Address
                                            </th>
                                            <th>
                                                Contact Number
                                            </th>
                                                                                       <th>
                                            </th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td align="center">
                                            <asp:CheckBox runat="server" ID="ChkUserInfo" />
                                            <asp:Label runat="server" ID="LblChk" Text='<%#Container.DataItem("InvHeadId")%>'
                                                Visible="false"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="LblAdd"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="LblContact"></asp:Label>
                                        </td>
                                                                               <td align="center">
                                            <asp:LinkButton ID="LnkEdit" runat="server" CommandName="Edit" CommandArgument='<%#Container.DataItem("InvHeadId") %>'
                                                Text="Edit" CausesValidation="false"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <tr>
                                        <td colspan="5" align="center">
                                            <asp:LinkButton ID="LnkPreviousRecord" Text="<< Prev" runat="server" OnClick="PreviousUserRecord"
                                                CausesValidation="false" />&nbsp;
                                            <asp:DropDownList ID="DrpOnRecord" AutoPostBack="true" runat="server" OnSelectedIndexChanged="OnUserRecord" />
                                            &nbsp;
                                            <asp:LinkButton ID="LnkNextRecord" Text="Next >>" runat="server" OnClick="NextUserRecord"
                                                CausesValidation="false" />
                                        </td>
                                    </tr>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 space1">
                            <asp:Button runat="server" ID="BtnDelete" Text="Delete Templete(s)" CssClass="dwnbtn"
                                CausesValidation="false" />
                            <ajax:ConfirmButtonExtender ID="confDelete1" runat="server" TargetControlID="BtnDelete"
                                DisplayModalPopupID="ModalPopupExtender1" />
                            <ajax:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="BtnDelete"
                                PopupControlID="PNL1" OkControlID="ButtonOk1" CancelControlID="ButtonCancel1"
                                BackgroundCssClass="ModalBackground" />
                            <asp:Panel ID="PNL1" runat="server" Style="display: none; padding: 20px;" CssClass="popup">
                                <b>Are you sure you want to Delete this Templete ?</b>
                                <br />
                                <br />
                                <div style="text-align: center;">
                                    <asp:Button ID="ButtonOk1" runat="server" Text="YES" CssClass="button" CausesValidation="false" />
                                    <asp:Button ID="ButtonCancel1" runat="server" Text="NO" CssClass="button" CausesValidation="false" />
                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        <UC1:UCFooter ID="UCFooter" runat="server" />
    </div>
    </div>
    </form>
</body>
</html>
