﻿Imports System.Data
Imports AjaxControlToolkit
Partial Class TaxMaster
    Inherits System.Web.UI.Page
    Dim Obj As New Engine
    Private Sub TaxMaster_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("CompanyId") = Nothing Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            CType(UCHeader.FindControl("LblHeading"), Label).Text = "Tax Master"
            bind()
        End If
    End Sub
    Public Sub bind()
        Dim StrSQL As String
        Dim search As String = ""

        If TxtSerName.Text <> "" Then
            search &= " and Name like '%" & TxtSerName.Text.Replace("'", "''") & "%'"
        End If

        StrSQL = "Select * from TaxMaster  where CompanyId=" & Session("CompanyId") & search & " Order By TaxID desc"

        Dim IntRecourtCount As Integer
        IntRecourtCount = Obj.BindGridControl(rep, StrSQL, LblHideUserCount, LblDispUserCount, LblNoUserCount, "Tax", "LnkPreviousRecord", "LnkNextRecord", "DrpOnRecord")
        If IntRecourtCount > 0 Then
            BtnDelete.Visible = True
        Else
            BtnDelete.Visible = False
        End If
    End Sub
    Private Function ValidateEnter() As String
        Dim StrError As String = ""
        Dim StrWhere As String = ""
        Dim cnt As Integer = 0
        If LblMode.Text = "Edit" Then
            StrWhere = " and TaxID<>" & LblId.Text
        End If
        cnt = Obj.GetField("select COUNT(TaxID) as cnt from TaxMaster where Name='" & txtName.Text.Replace("'", "''") & "' and (CompanyId=0 and CompanyId=" & Session("CompanyId") & ")" & StrWhere, "cnt")

        If cnt <> 0 Then
            StrError = "Status Already Exists"
        End If

        If RdbType.SelectedValue = "P" Then

            If TxtAmount.Text > 100 Then
                StrError &= "Amount Should be less than 100 "

            End If
        End If

        BtnCancel.Visible = True

        Return StrError
    End Function

    Private Sub BtnSubmit_Click(sender As Object, e As EventArgs) Handles BtnSubmit.Click
        Dim rs As New RecorsetUpdate
        LblMessage.Text = ""

        Dim vals As String = ValidateEnter()
        If vals = "" Then
            Dim StrID As String = ""
            If LblMode.Text = "Add" Then
                rs.TableParameter("TaxMaster", "Add")
                StrID = "(select ISNULL(MAX(TaxID),0) AS StausId from TaxMaster where Status='" & txtName.Text.Replace("'", "''") & "' and CompanyId=" & Session("CompanyId") & ")"
            ElseIf LblMode.Text = "Edit" Then
                rs.TableParameter("TaxMaster", "Edit", "TaxID=" & LblId.Text)
                StrID = LblId.Text
            End If
            rs.SetField("Type", RdbType.SelectedValue, "String")
            rs.SetField("Name", txtName.Text.Replace("'", "''"), "String")
            rs.SetField("Amount", TxtAmount.Text, "Number")
            rs.SetField("CompanyId", Session("CompanyId"), "Number")
            rs.Execute()

            ClearText()
            bind()
            LblMessage.Text = "Sucessfully Updated!"
            LblMode.Text = "Add"

        ElseIf vals <> "" Then
            LblMessage.Text = ValidateEnter()
        End If
    End Sub
    Public Sub ClearText()
        RdbType.SelectedValue = "True"
        txtName.Text = ""
        TxtAmount.Text = ""
        LblMode.Text = "Add"
        LblMessage.Text = ""
        LblId.Text = ""
    End Sub

    Private Sub BtnCancel_Click(sender As Object, e As EventArgs) Handles BtnCancel.Click
        ClearText()
    End Sub
    Protected Sub BtnDelete_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnDelete.Click
        Dim strId As String = ""
        Dim StrErr As String = ""
        For i As Integer = 0 To rep.Items.Count - 1
            If CType(rep.Items(i).FindControl("ChkUserInfo"), CheckBox).Checked = True Then
                Dim cnt As Integer = 0
                'cnt = Obj.GetField("select COUNT(LeadId) as cnt from LeadsMaster where LeadStatus='" & CType(rep.Items(i).FindControl("LblChk"), Label).Text & "'", "cnt")
                If cnt = 0 Then
                    strId &= CType(rep.Items(i).FindControl("LblChk"), Label).Text & ","
                Else
                    StrErr &= CType(rep.Items(i).FindControl("lblName"), Label).Text & ","
                End If
            End If
        Next

        If strId <> "" Then

            strId = Left(strId, strId.Length - 1)

            Dim Sql As String = "Delete From TaxMaster where TaxID in (" & strId & ")"
            Obj.ExecuteCommand(Sql)
            bind()
            If StrErr <> "" Then
                StrErr = Left(StrErr, StrErr.Length - 1)
                LblMessage.Text = "Unable to delete " & StrErr
            Else
                LblMessage.Text = "Sucessfully Deleted !"
            End If
        End If

    End Sub


    Public Sub PreviousUserRecord(ByVal sender As Object, ByVal e As System.EventArgs)
        LblHideUserCount.Text = CInt(LblHideUserCount.Text) - 1
        Obj.PreviousRecord()
        bind()
    End Sub

    Public Sub NextUserRecord(ByVal sender As Object, ByVal e As System.EventArgs)
        LblHideUserCount.Text = CInt(LblHideUserCount.Text) + 1
        Obj.NextRecord()
        bind()
    End Sub

    Protected Sub OnUserRecord(ByVal sender As Object, ByVal e As System.EventArgs)
        LblHideUserCount.Text = CType(rep.Controls(rep.Controls.Count - 1).FindControl("DrpOnRecord"), DropDownList).SelectedValue
        Obj.OnRecord(LblHideUserCount.Text)
        bind()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        bind()
    End Sub

    Private Sub rep_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rep.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            If e.Item.DataItem("CompanyId") = 0 Then
                CType(e.Item.FindControl("ChkUserInfo"), CheckBox).Visible = False
                CType(e.Item.FindControl("LnkEdit"), LinkButton).Visible = False
            End If
            If Not IsDBNull(e.Item.DataItem("Amount")) Then CType(e.Item.FindControl("LblAmt"), Label).Text = e.Item.DataItem("Amount")
            If Not IsDBNull(e.Item.DataItem("Name")) Then CType(e.Item.FindControl("lblName"), Label).Text = e.Item.DataItem("Name")
            If e.Item.DataItem("Type") = "I" Then
                CType(e.Item.FindControl("LblMode"), Label).Text = "Add in Invoice"
            Else
                CType(e.Item.FindControl("LblMode"), Label).Text = "Deductable at Client"
            End If
        End If
    End Sub

    Private Sub rep_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rep.ItemCommand
        If e.CommandName = "Edit" Then
            Dim dt As New DataTable
            dt = Obj.GetDataTable("Select * from TaxMaster where TaxID=" & e.CommandArgument)
            If dt.Rows.Count > 0 Then
                txtName.Text = dt.Rows(0).Item("Name")
                RdbType.SelectedValue = dt.Rows(0).Item("Type")

                If Not IsDBNull(dt.Rows(0).Item("Amount")) Then TxtAmount.Text = dt.Rows(0).Item("Amount")
                LblMode.Text = "Edit"
                LblMessage.Text = ""
                LblId.Text = e.CommandArgument
            End If
        End If

    End Sub

    Private Sub TxtAmount_TextChanged(sender As Object, e As EventArgs) Handles TxtAmount.TextChanged

        If TxtAmount.Text > 100 Then
            LblMessage.Text = "Value Should be less than  100 "
        Else
            LblMessage.Text = ""
        End If

    End Sub
End Class
