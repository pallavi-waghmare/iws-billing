﻿Imports System.Data
Imports System.IO
Imports AjaxControlToolkit
Partial Class LedgerReport
    Inherits System.Web.UI.Page
    Dim obj As New Engine


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Session("CompanyId") = Nothing Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            CType(UCHeader.FindControl("LblHeading"), Label).Text = "Sales Report"
            'TxtFrom.Text = obj.GetFormattedDate(Date.Today.AddMonths(-1))
            'TxtTo.Text = obj.GetFormattedDate(Date.Today)

            TxtFrom.Attributes.Add("Readonly", "Readonly")
            TxtTo.Attributes.Add("Readonly", "Readonly")

            BindDrp()
            Bind()
        End If
    End Sub

    Public Sub BindDrp()
        Dim sql As String = ""
        sql &= "select CustomerId,CompanyName from CustomerMaster Where CustomerId=" & Session("CustId") & ";"
        'sql &= "select * from MediumMaster;"
        Dim ds As New DataSet
        ds = obj.GetDataset(sql)

        obj.BindListControl1(drpSerCust, ds.Tables(0), "CompanyName", "CustomerId")

        drpSerPro.Items.Insert(0, New ListItem("All Product", ""))
        If drpSerCust.SelectedValue <> "" Then
            bindPro(drpSerCust.SelectedValue)
        End If

    End Sub
    Public Sub bindPro(ByVal id As Integer)
        obj.BindListControl(drpSerPro, "select ProductId,ProductName from ProductMaster where CustomerId=" & id, "ProductName", "ProductId")
    End Sub
    Public Sub Bind()
        Dim sql = "", sqltax = "", strWhere As String = ""

        If Not IsNothing(Request.QueryString("CustID")) Then
            strWhere &= " and LM.CustomerId=" & Request.QueryString("CustID")
        End If

        If drpSerCust.SelectedValue <> "" Then
            strWhere &= " and CM.CustomerId=" & drpSerCust.SelectedValue
        End If

        If TxtFrom.Text <> "" Then
            strWhere &= " and LM.InvDate>='" & TxtFrom.Text.Replace("'", "''") & "'"
        End If

        If TxtTo.Text <> "" Then
            strWhere &= " and LM.InvDate<=DATEADD(day,1,'" & TxtTo.Text.Replace("'", "''") & "')"
        End If

        sqltax &= "select Cu.Name as CustomerName,Cu.Mobile,Lm.* from InvoiceMaster LM "
        sqltax &= "left join UserCustomerMaster Cu on LM.CustomerId=Cu.CustomerId "
        sqltax &= "left join CustomerMaster CM on CM.CustomerId=LM.CompanyID "
        sqltax &= " where 1=1 and InvoiceType='T' and CM.CustomerId= " & Session("CompanyId") & strWhere & " order by LM.InvID desc"


        Dim IntRecourtCount As Integer

        IntRecourtCount = obj.BindGridControl(reptax, sqltax, LblTaxHideUserCount, LblTaxDispUserCount, LblTaxNoUserCount, "Ledger Report", "LnkPreviousRecord", "LnkNextRecord", "DrpOnRecord", 50)

    End Sub

    Public Sub PreviousTaxUserRecord(ByVal sender As Object, ByVal e As System.EventArgs)
        LblTaxHideUserCount.Text = CInt(LblTaxHideUserCount.Text) - 1
        obj.PreviousRecord()
        Bind()
    End Sub

    Public Sub NextTaxUserRecord(ByVal sender As Object, ByVal e As System.EventArgs)
        LblTaxHideUserCount.Text = CInt(LblTaxHideUserCount.Text) + 1
        obj.NextRecord()
        Bind()
    End Sub

    Protected Sub OnUserRecordTax(ByVal sender As Object, ByVal e As System.EventArgs)
        LblTaxHideUserCount.Text = CType(reptax.Controls(reptax.Controls.Count - 1).FindControl("DrpOnRecord"), DropDownList).SelectedValue
        obj.OnRecord(LblTaxHideUserCount.Text)
        Bind()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Bind()
    End Sub

    Dim ToInAmt As Double = 0.0
    Dim ToInNetAmt As Double = 0.0
    Private Sub reptax_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles reptax.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            If Not IsDBNull(e.Item.DataItem("InvoiceNo")) Then
                If e.Item.DataItem("InvoiceNo") <> "" Then
                    CType(e.Item.FindControl("LnkInvNo"), LinkButton).Text = e.Item.DataItem("InvoiceNo")
                Else
                    CType(e.Item.FindControl("LnkInvNo"), LinkButton).Text = "XXXXXXXXXXXX"
                End If
            End If
            Dim Product As String = ""
            Dim dt As New DataTable
            dt = obj.GetDataTable("Select Product From InvoiceDetails where Invid=" & e.Item.DataItem("invID"))
            If dt.Rows.Count > 0 Then
                For i As Integer = 0 To dt.Rows.Count - 1
                    Product &= dt.Rows(i).Item("Product") & "</br>"
                Next
            End If

            If Product <> "" Then
                Product = Left(Product, Product.Length - 1)
            End If
            CType(e.Item.FindControl("LblPDesc"), Label).Text = Product
            If Not IsDBNull(e.Item.DataItem("CustomerName")) Then CType(e.Item.FindControl("LblName"), Label).Text = e.Item.DataItem("CustomerName")
            If Not IsDBNull(e.Item.DataItem("InvDate")) Then CType(e.Item.FindControl("LblInvDate"), Label).Text = obj.GetFormattedDate(e.Item.DataItem("InvDate"))
            If Not IsDBNull(e.Item.DataItem("InvAmount")) Then CType(e.Item.FindControl("LblInvAmt"), Label).Text = e.Item.DataItem("InvAmount")
            If Not IsDBNull(e.Item.DataItem("InvNetAmount")) Then CType(e.Item.FindControl("LblInvNetAmt"), Label).Text = e.Item.DataItem("InvNetAmount")

            ToInAmt += e.Item.DataItem("InvAmount")
            ToInNetAmt += e.Item.DataItem("InvNetAmount")

            Dim Dtr As New DataTable
            Dtr = obj.GetDataTable("Select * From PaymentReceived where InvID=" & e.Item.DataItem("InvID"))
            If Not IsNothing(Dtr) Then
                If Dtr.Rows.Count > 0 Then
                    CType(e.Item.FindControl("RptReceipt"), Repeater).DataSource = Dtr
                    CType(e.Item.FindControl("RptReceipt"), Repeater).DataBind()
                End If
            End If

        End If
        If e.Item.ItemType = ListItemType.Footer Then
            CType(e.Item.FindControl("LblToInAmt"), Label).Text = Math.Round(ToInAmt, 2).ToString("N", Globalization.CultureInfo.InvariantCulture)
            CType(e.Item.FindControl("LblToInNetAmt"), Label).Text = Math.Round(ToInNetAmt, 2).ToString("N", Globalization.CultureInfo.InvariantCulture)
        End If

    End Sub
    Dim PaidAmt As Double = 0.0
    Dim InvAmt As Double = 0.0
    Public Sub repreceipt_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        If e.Item.ItemIndex >= 0 Then
            InvAmt = obj.GetField("Select InvNetAmount From InvoiceMaster where InvID=" & e.Item.DataItem("InvID"), "InvNetAmount")
            If Not IsDBNull(e.Item.DataItem("PaidAmount")) Then CType(e.Item.FindControl("LblPaidAmount"), Label).Text = e.Item.DataItem("PaidAmount")
            PaidAmt += e.Item.DataItem("PaidAmount")
        End If
        If e.Item.ItemType = ListItemType.Footer Then
            CType(e.Item.FindControl("LblOutStanding"), Label).Text = Math.Round(InvAmt - PaidAmt, 2).ToString("N", Globalization.CultureInfo.InvariantCulture)
        End If
    End Sub
End Class
