﻿Imports System.Data
Imports System.IO
Imports AjaxControlToolkit
Partial Class OustandingReport
    Inherits System.Web.UI.Page
    Dim obj As New Engine

    'Dim ToReAmt As Double = 0.0

    Private Sub page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Session("CompanyId") = Nothing Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            CType(UCHeader.FindControl("LblHeading"), Label).Text = "Outstanding Report"
            'TxtFrom.Text = obj.GetFormattedDate(Date.Today.AddMonths(-1))
            'TxtTo.Text = obj.GetFormattedDate(Date.Today)

            TxtFrom.Attributes.Add("Readonly", "Readonly")
            TxtTo.Attributes.Add("Readonly", "Readonly")

            BindDrp()
            Bind()
        End If
    End Sub

    Public Sub BindDrp()
        Dim sql As String = ""
        sql &= "select CustomerId,CompanyName from CustomerMaster Where CustomerId=" & Session("CustId") & ";"
        'sql &= "select * from MediumMaster;"
        Dim ds As New DataSet
        ds = obj.GetDataset(sql)

        obj.BindListControl1(drpSerCust, ds.Tables(0), "CompanyName", "CustomerId")

        drpSerPro.Items.Insert(0, New ListItem("All Product", ""))
        If drpSerCust.SelectedValue <> "" Then
            bindPro(drpSerCust.SelectedValue)
        End If

    End Sub
    Public Sub bindPro(ByVal id As Integer)
        obj.BindListControl(drpSerPro, "select ProductId,ProductName from ProductMaster where CustomerId=" & id, "ProductName", "ProductId")
    End Sub
    Public Sub Bind()
        Dim sql = "", sqltax = "", strWhere As String = ""

        If Not IsNothing(Request.QueryString("CustID")) Then
            strWhere &= " and LM.CustomerId=" & Request.QueryString("CustID")
        End If

        If drpSerCust.SelectedValue <> "" Then
            strWhere &= " and CM.CustomerId=" & drpSerCust.SelectedValue
        End If

        If TxtFrom.Text <> "" Then
            strWhere &= " and LM.InvDate>='" & TxtFrom.Text.Replace("'", "''") & "'"
        End If

        If TxtTo.Text <> "" Then
            strWhere &= " and LM.InvDate<=DATEADD(day,1,'" & TxtTo.Text.Replace("'", "''") & "')"
        End If

        sqltax &= "Select * From (select (Select (Lm.InvNetAmount-Sum(PaidAmount)) From PaymentReceived where InvID=Lm.InvID) as PaidAmount, Cu.Name as CustomerName,Cu.Mobile,Lm.* from InvoiceMaster LM "
        sqltax &= "left join UserCustomerMaster Cu on LM.CustomerId=Cu.CustomerId "
        sqltax &= "left join CustomerMaster CM on CM.CustomerId=LM.CompanyID "
        sqltax &= " where 1=1 and InvoiceType='T' and CM.CustomerId= " & Session("CompanyId") & strWhere & ") a where PaidAmount<>0 order by InvID Desc"


        Dim IntRecourtCount As Integer

        IntRecourtCount = obj.BindGridControl(reptax, sqltax, LblTaxHideUserCount, LblTaxDispUserCount, LblTaxNoUserCount, "Outstanding Report", "LnkPreviousRecord", "LnkNextRecord", "DrpOnRecord", 50)

    End Sub

    Public Sub PreviousTaxUserRecord(ByVal sender As Object, ByVal e As System.EventArgs)
        LblTaxHideUserCount.Text = CInt(LblTaxHideUserCount.Text) - 1
        obj.PreviousRecord()
        Bind()
    End Sub

    Public Sub NextTaxUserRecord(ByVal sender As Object, ByVal e As System.EventArgs)
        LblTaxHideUserCount.Text = CInt(LblTaxHideUserCount.Text) + 1
        obj.NextRecord()
        Bind()
    End Sub

    Protected Sub OnUserRecordTax(ByVal sender As Object, ByVal e As System.EventArgs)
        LblTaxHideUserCount.Text = CType(reptax.Controls(reptax.Controls.Count - 1).FindControl("DrpOnRecord"), DropDownList).SelectedValue
        obj.OnRecord(LblTaxHideUserCount.Text)
        Bind()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Bind()
    End Sub
    Dim ToInvAmt As Double = 0.0
    Dim ToPaidAmt As Double = 0.0
    Dim TotRemAmt As Double = 0.0
    Private Sub reptax_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles reptax.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            If Not IsDBNull(e.Item.DataItem("InvoiceNo")) Then
                If e.Item.DataItem("InvoiceNo") <> "" Then
                    CType(e.Item.FindControl("LblInvNo"), Label).Text = e.Item.DataItem("InvoiceNo")
                Else
                    CType(e.Item.FindControl("LblInvNo"), Label).Text = "XXXXXXXXXXXX"
                End If
            End If
            Dim Product As String = ""
            Dim dt As New DataTable
            dt = obj.GetDataTable("Select Product From InvoiceDetails where Invid=" & e.Item.DataItem("invID"))
            If dt.Rows.Count > 0 Then
                For i As Integer = 0 To dt.Rows.Count - 1
                    Product &= dt.Rows(i).Item("Product") & "</br>"
                Next
            End If

            If Product <> "" Then
                Product = Left(Product, Product.Length - 1)
            End If
            CType(e.Item.FindControl("LblPDesc"), Label).Text = Product
            If Not IsDBNull(e.Item.DataItem("CustomerName")) Then CType(e.Item.FindControl("LblName"), Label).Text = e.Item.DataItem("CustomerName")
            If Not IsDBNull(e.Item.DataItem("Mobile")) Then CType(e.Item.FindControl("LblContactNo"), Label).Text = e.Item.DataItem("Mobile")
            If Not IsDBNull(e.Item.DataItem("InvNetAmount")) Then CType(e.Item.FindControl("LblInvAmt"), Label).Text = e.Item.DataItem("InvNetAmount")
            If Not IsDBNull(e.Item.DataItem("ValidDate")) Then CType(e.Item.FindControl("LblValid"), Label).Text = "Valid Till : " & obj.GetFormattedDate(e.Item.DataItem("ValidDate"))
            Dim dtpay As New DataTable
            dtpay = obj.GetDataTable("Select PaidAmount From PaymentReceived where InvID=" & e.Item.DataItem("InvID"))
            Dim PaidAmt As Integer = 0
            If Not IsNothing(dtpay) Then
                If dtpay.Rows.Count > 0 Then
                    For i As Integer = 0 To dtpay.Rows.Count - 1
                        PaidAmt += dtpay(i)("PaidAmount")
                    Next
                End If
            End If

            CType(e.Item.FindControl("LblPaidAmount"), Label).Text = PaidAmt
            CType(e.Item.FindControl("LblRemainingAmount"), Label).Text = Val(e.Item.DataItem("InvNetAmount")) - PaidAmt

            ToInvAmt += e.Item.DataItem("InvNetAmount")
            ToPaidAmt += PaidAmt
            TotRemAmt += Val(e.Item.DataItem("InvNetAmount")) - PaidAmt

        End If
        If e.Item.ItemType = ListItemType.Footer Then
            CType(e.Item.FindControl("LblInvAmt"), Label).Text = ToInvAmt
            CType(e.Item.FindControl("LblPaidAmt"), Label).Text = ToPaidAmt
            CType(e.Item.FindControl("LblReAmt"), Label).Text = TotRemAmt

        End If

    End Sub

End Class
