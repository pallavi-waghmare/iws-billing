﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SalesReport.aspx.vb" Inherits="SalesReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Src="~/Header.ascx" TagName="UCHeader" TagPrefix="UC1" %>
<%@ Register Src="~/footer.ascx" TagName="UCFooter" TagPrefix="UC1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Customer List</title>
    <link rel="icon" href="../Images/thumb.png" />
    <link href="Css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="Css/website/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="Css/css.css" rel="stylesheet" type="text/css" />
    <link type="text/css" rel="Stylesheet" href="Css/Style.css" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div>
                <UC1:UCHeader ID="UCHeader" runat="server" />
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="boxshadow">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="title">
                                    Search Parameter
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 padding">
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="email">
                                            <label for="email">
                                                Customer
                                            </label>
                                            <asp:DropDownList ID="drpSerCust" runat="server" AutoPostBack="true" class="form-control">
                                            </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="email">
                                            Product
                                        </label>
                                        <asp:DropDownList ID="drpSerPro" runat="server" class="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="email">
                                            From
                                        </label>
                                        <asp:TextBox ID="TxtFrom" runat="server" class="form-control"></asp:TextBox>
                                        <ajax:CalendarExtender ID="AjxCel1" runat="server" TargetControlID="TxtFrom" Format="dd-MMM-yyyy"
                                            CssClass="cal">
                                        </ajax:CalendarExtender>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="email">
                                            To
                                        </label>
                                        <asp:TextBox ID="TxtTo" runat="server" class="form-control"></asp:TextBox>
                                        <ajax:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="TxtTo"
                                            Format="dd-MMM-yyyy" CssClass="cal">
                                        </ajax:CalendarExtender>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="form-group">
                                        <asp:Button runat="server" ID="btnSearch" Text="Search" CssClass="button1" CausesValidation="false" />
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="col-md-12 col-sm-12 col-xs-12 space1">
                                    <asp:Label ID="LblTaxDispUserCount" runat="server" CssClass="showtxt"></asp:Label>
                                    <asp:Label ID="LblTaxHideUserCount" runat="server" Visible="false" CssClass="showtxt"></asp:Label>
                                    <asp:Label ID="LblTaxNoUserCount" runat="server" CssClass="showtxt"></asp:Label>
                                    <asp:Label ID="lblTaxinfo" runat="server" CssClass="showtxt"></asp:Label>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 space">
                                    <div>
                                        <asp:Label ID="lblTaxLeads" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 space1">
                                    <asp:Repeater runat="server" ID="reptax">
                                        <HeaderTemplate>
                                            <table class="table">
                                                <tr>
                                                    <th width="2%"></th>
                                                    <th width="10%">Invoice No
                                                    </th>
                                                    <th width="10%">Product Description
                                                    </th>
                                                    <th width="10%">Name
                                                    </th>
                                                    <th width="10%">Invoice Date
                                                    </th>
                                                    <th width="10%">Invoice Amount
                                                    </th>
                                                    <th width="10%">Invoice Net Amount</th>
                                                    <%--<th width="10%">Remaining Amount</th>--%>
                                                </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td align="center">
                                                    <asp:CheckBox runat="server" ID="ChkUserInfo" />
                                                    <asp:Label runat="server" ID="LblChk" Text='<%#Container.DataItem("InvID")%>' Visible="false"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label runat="server" ID="LblInvNo"></asp:Label><br />
                                                    <b><asp:Label ID="LblValid" runat="server"></asp:Label></b>
                                                </td>
                                                <td>
                                                    <asp:Label runat="server" ID="LblPDesc"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label runat="server" ID="lblName"></asp:Label>
                                                    <asp:HiddenField ID="HdnCustID" runat="server" Value='<%#Container.DataItem("CustomerId")%>' />
                                                </td>
                                                <td>
                                                    <asp:Label runat="server" ID="LblInvDate"></asp:Label>
                                                </td>

                                                <td style="text-align: right">
                                                    <asp:Label runat="server" ID="LblInvAmt"></asp:Label>
                                                </td>
                                                <td style="text-align: right">
                                                    <asp:Label runat="server" ID="LblInvNetAmt"></asp:Label>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <tr>
                                                <td colspan="5"></td>
                                                <td style="text-align: right">
                                                    <asp:Label runat="server" ID="LblToInAmt"></asp:Label>
                                                </td>
                                                <td style="text-align: right">
                                                    <asp:Label runat="server" ID="LblToInNetAmt"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="12" style="text-align: center;">
                                                    <asp:LinkButton ID="LnkPreviousRecord" Text="<< Prev" runat="server" OnClick="PreviousTaxUserRecord" Style="top: 0px;"
                                                        CausesValidation="false" class="btnnxt" />&nbsp;
                                            <asp:DropDownList ID="DrpOnRecord" AutoPostBack="true" runat="server" class="pagedrp" Style="margin-top: 0px; width: 50px; float: none; display: inherit;" OnSelectedIndexChanged="OnUserRecordTax" />&nbsp;
                                         
                                            <asp:LinkButton ID="LnkNextRecord" Text="Next >>" runat="server" OnClick="NextTaxUserRecord" Style="top: 0px;"
                                                CausesValidation="false" class="btnnxt" />&nbsp;
                                                </td>
                                            </tr>
                                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>


                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div>
                <UC1:UCFooter ID="UCFooter" runat="server" />
            </div>
        </div>
    </form>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap/bootstrap.min.js"></script>
</body>
</html>
