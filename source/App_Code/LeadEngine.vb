﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports Microsoft.VisualBasic

Public Class LeadEngine
    Dim obj As New Engine
    Dim crypt As New Cryptography
    Public Function BindGridControl(Optional ByVal strWhere As String = "", Optional ByVal CurrentPage As Integer = 1, Optional ByVal GridSize As Integer = 10, _
                                    Optional ByVal LblInfo As Label = Nothing, _
                        Optional ByVal LnkPrev As Object = Nothing, Optional ByVal LnkNext As Object = Nothing, _
                        Optional ByVal DrpPages As Object = Nothing) As DataTable

        Dim ds As New DataSet
        Try
            Dim cnn As New SqlConnection(CnnString)

            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If

            Dim cmd As New SqlCommand("GETLEADSWITHPAGING", cnn)
            cmd.Parameters.AddWithValue("@PageIndex", CurrentPage)
            cmd.Parameters.AddWithValue("@PageSize", GridSize)
            cmd.Parameters.AddWithValue("@StrWhere", strWhere)
            cmd.CommandType = CommandType.StoredProcedure
            Dim rs As New RecorsetUpdate
            rs.TableParameter("Temlog", "Add")
            rs.SetField("logid", strWhere, "String")
            rs.Execute()

            Dim Ad As New SqlDataAdapter(cmd)

            Ad.Fill(ds)
            Dim page As Double = 0
            Dim Total As Integer = 0
            If Not IsNothing(ds) Then
                If ds.Tables(1).Rows.Count > 0 Then
                    Total = ds.Tables(1).Rows(0).Item("CNT")
                    If (Total Mod GridSize) = 0 Then
                        page = Total / GridSize
                    Else
                        page = Total / GridSize
                        page = page + 1
                        Dim str As String()
                        str = page.ToString().Split(".")
                        page = str(0)
                    End If
                End If
            End If

            If Total = 0 Then
                LblInfo.Text = "Oops! No Data found"
            Else
                Dim current = 0, Last As Integer = 0
                current = ((CurrentPage - 1) * GridSize) + 1
                Last = CurrentPage * GridSize
                If Last > Total Then
                    LblInfo.Text = "Showing " & current & " to " & Total & " of " & Total
                Else
                    LblInfo.Text = "Showing " & current & " to " & Last & " of " & Total
                End If
            End If

            If page > 1 Then
                DrpPages.visible = True
                DrpPages.Items.Clear()
                For i As Integer = 1 To page
                    DrpPages.Items.insert(i - 1, New ListItem(i.ToString, i.ToString))
                Next
                DrpPages.SelectedValue = CurrentPage

                If CurrentPage = 1 Then
                    LnkPrev.visible = False
                Else
                    LnkPrev.visible = True
                End If

                If CurrentPage = page Then
                    LnkNext.visible = False
                Else
                    LnkNext.visible = True
                End If
            Else
                DrpPages.visible = False
                LnkPrev.visible = False
                LnkNext.visible = False
            End If

        Catch ex As Exception
            HttpContext.Current.Response.Write(ex.ToString)
        End Try

        Return ds.Tables(0)
    End Function

    Public Function Getstring(ByVal dt As DataTable, Optional ByVal CompanyId As Integer = 0, Optional ByVal PageName As String = "", _
                              Optional ByVal Role As String = "", Optional ByVal IsLeadSystem As String = "", Optional ByVal Enddate As String = "", _
                              Optional ByVal IsArchived As Boolean = False, Optional ByVal FollowUp As Boolean = True, Optional ByVal SMS As Boolean = True, _
                              Optional ByVal Email As Boolean = True, Optional ByVal View As Boolean = True, Optional ByVal Query As Boolean = True, _
                              Optional ByVal FromPage As String = "") As String
        Dim dtCust As New DataTable
        Dim sb As New StringBuilder
        Dim cnt As Integer = 0
        If dt.Rows.Count > 0 Then
            Dim ProdId As String = obj.GetField("Select CustomerId from ProductMaster where ProductId= " & dt.Rows(0).Item("ProductId"), "CustomerId")

            Dim DtFollowUp As DataTable = obj.GetDataTable("sELECT * FROM CustomerMaster where CustomerId = " & ProdId)
        End If
        Dim mainPage As String = ""

        mainPage = PageName

        PageName = crypt.Base64Encode(PageName)

        dtCust = obj.GetDataTable("SELECT * FROM CustomFields WHERE ViewInList=1 AND CompanyId=" & CompanyId & " ORDER BY sequence")

        If dtCust.Rows.Count > 0 Then
            cnt = dtCust.Rows.Count + 1
            sb.AppendLine("<table Class='table table-responsive'>")

            sb.AppendLine(" <tr>")
            sb.AppendLine("<th id='abc'>")
            'sb.AppendLine("<input type='checkbox' name='vehicle1' value='Bike'>")
            sb.AppendLine("</th>")
            'sb.AppendLine("<th>")
            ''sb.AppendLine("<img src='Images/flag.png' alt='Smiley face' width='42' height='42'>")
            'sb.AppendLine("</th>")
            For i As Integer = 0 To dtCust.Rows.Count - 1
                Dim strHead As String = ""

                If Not IsDBNull(dtCust.Rows(i).Item("CustomName")) And dtCust.Rows(i).Item("CustomName") <> "" Then
                    strHead = dtCust.Rows(i).Item("CustomName")
                Else
                    strHead = dtCust.Rows(i).Item("FiledName")
                End If

                If Role = "Team" Then
                    If strHead = "PickBy" Then
                        cnt = cnt - 1
                        GoTo HeadFin
                    End If
                End If
                If FromPage = "AvailableLeads" Then
                    If strHead = "Query" Then
                        sb.AppendLine("<th style='display:none'>")
                        sb.AppendLine(strHead)
                        sb.AppendLine("</th>")
                    Else
                        sb.AppendLine("<th>")
                        sb.AppendLine(strHead)
                        sb.AppendLine("</th>")
                    End If
                Else

                    If strHead = "Query" Then
                        sb.AppendLine("<th style='width:10%'>")
                        sb.AppendLine(strHead)
                        sb.AppendLine("</th>")
                    Else
                        sb.AppendLine("<th>")
                        sb.AppendLine(strHead)
                        sb.AppendLine("</th>")
                    End If
                End If

HeadFin:
            Next
            sb.AppendLine(" </tr>")

            For j As Integer = 0 To dt.Rows.Count - 1
                sb.AppendLine(" <tr>")
                'sb.AppendLine("<td>")
                'If dt.Rows(j).Item("flags") = 1 Then
                '    sb.AppendLine("<img src='Images/flag.png' alt='alert' width='42' height='42' title='You are viewing this flag because you have missed followup'>")
                'End If
                'sb.AppendLine("</td>")
                sb.AppendLine("<td>")
                sb.AppendLine("<input type='checkbox' runat='server' name='CkhSendBulkTransact' value='" & dt.Rows(j).Item("LeadId") & "'>")
                sb.AppendLine("</td>")
                For k As Integer = 0 To dtCust.Rows.Count - 1
                    Dim strName As String = dtCust.Rows(k).Item("FiledName")
                    Dim StrData As String = ""

                    Select Case strName
                        Case "Product"
                            strName = "ProductName"
                        Case "Medium"
                            strName = "MediumName"
                        Case "PickBy"
                            strName = "TeamName"
                        Case "Location"
                            strName = "LocationName"
                        Case Else
                            strName = strName
                    End Select

                    If Role = "Team" Then
                        If strName = "TeamName" Then
                            GoTo fin
                        End If
                    End If

                    If Not IsDBNull(dt.Rows(j).Item(strName)) Then
                        StrData = dt.Rows(j).Item(strName)
                    End If
                    If strName = "FollowUpDate" Then
                        StrData = obj.GetFormattedDateTime(StrData)
                    ElseIf strName = "LastUpdated" Then
                        StrData = obj.GetFormattedDateTime(StrData)
                    ElseIf strName = "CreateDate" Then
                        StrData = obj.GetFormattedDateTime(StrData)
                    End If
                    If FromPage = "AvailableLeads" Then
                        If strName = "Query" Then
                            sb.AppendLine("     <td style='display:none'>")
                            sb.AppendLine(StrData)
                            sb.AppendLine("     </td>")
                        Else
                            sb.AppendLine("     <td>")
                            sb.AppendLine(StrData)
                            sb.AppendLine("     </td>")
                        End If
                    Else
                        sb.AppendLine("     <td>")
                        sb.AppendLine(StrData)
                        sb.AppendLine("     </td>")
                    End If


fin:
                Next
                sb.AppendLine(" </tr>")
                If IsArchived = False Then

                    If IsLeadSystem = False Then
                        sb.AppendLine(" <tr style='display:none'>")
                    ElseIf IsLeadSystem = True And Enddate < DateTime.Now And Enddate <> "" Then
                        sb.AppendLine(" <tr style='display:none'>")
                    Else
                        sb.AppendLine("<tr align='left'>")
                    End If
                    sb.AppendLine("<td colspan='" + cnt.ToString + "'>")

                    If dt.Rows(j).Item("flags") = 1 Then

                        sb.AppendLine("<img src='Images/flag.png' alt='alert' width='42' height='42' title='You are viewing this flag because you have missed followup'>")
                    Else
                        sb.AppendLine("")
                    End If
                    sb.AppendLine("<a class='vbbtn' href='ViewLead.aspx?Id=" & dt.Rows(j).Item("LeadId") & "&uri=" & PageName & "'>View</a>")
                    If FollowUp = True Then
                        sb.AppendLine("")
                        If mainPage = "AvaliableLeads" Then
                            sb.AppendLine("<a class='vbbtn' href='AddFollowUp.aspx?Id=" & dt.Rows(j).Item("LeadId") & "&uri=" & PageName & "'>Pick</a>")
                        Else
                            If dt.Rows(j).Item("LeadCount") = 0 Then
                                sb.AppendLine("<a class='vbbtn' href='AddFollowUp.aspx?Id=" & dt.Rows(j).Item("LeadId") & "&uri=" & PageName & "'>Add FollowUp</a>")
                              

                            Else
                                sb.AppendLine("<a class='vbbtn' href='AddFollowUp.aspx?Id=" & dt.Rows(j).Item("LeadId") & "&uri=" & PageName & "'>Add FollowUp {" & dt.Rows(j).Item("LeadCount") & "}</a>")
                              
                            End If


                        End If
                    End If
                    If Not IsDBNull(dt.Rows(j).Item("PickBy")) And dt.Rows(j).Item("PickBy") <> 0 Then
                        sb.AppendLine("")
                        sb.AppendLine("<a class='vbbtn' href='Reassign.aspx?Id=" & dt.Rows(j).Item("LeadId") & "&uri=" & PageName & "'>Re-Assign</a>")
                    End If
                    If SMS = True Then
                        If Not IsDBNull(dt.Rows(j).Item("Mobile")) And dt.Rows(j).Item("Mobile") <> "" Then

                            sb.AppendLine("")
                            sb.AppendLine("<a class='vbbtn' href='LeadSMS.aspx?Id=" & dt.Rows(j).Item("LeadId") & "&uri=" & PageName & "'>Send SMS</a>")

                        End If

                    End If
                    If Email = True Then
                        If Not IsDBNull(dt.Rows(j).Item("EmailAddress")) And dt.Rows(j).Item("EmailAddress") <> "" Then

                            sb.AppendLine("")
                            sb.AppendLine("<a class='vbbtn' href='LeadEmail.aspx?Id=" & dt.Rows(j).Item("LeadId") & "&uri=" & PageName & "'>Send Email</a>")

                        End If
                    End If
                    sb.AppendLine("     </td>")
                    sb.AppendLine(" </tr>")
                End If
            Next

            sb.AppendLine("</table>")
        End If


        Return sb.ToString

    End Function

    Public Function GetleadsCounts(ByVal dt As DataTable, Optional ByVal CompanyId As Integer = 0, Optional ByVal PageName As String = "") As String
        Dim dtCust As New DataTable
        Dim sb As New StringBuilder
        Dim cnt As Integer = 0

        Dim mainPage As String = ""
        If Not IsNothing(dt) Then
            If dt.Rows.Count > 0 Then
                cnt = dt.Rows.Count
                sb.AppendLine("<table Class='table'>")

                sb.AppendLine(" <tr>")
                For i As Integer = 0 To dt.Columns.Count - 1
                    Dim strHead As String = ""

                    strHead = dt.Columns(i).ColumnName.ToString()
                    Dim str() As String = strHead.Split("0")

                    sb.AppendLine("     <th>")
                    sb.AppendLine(str(0))
                    sb.AppendLine("     </th>")

                Next
                sb.AppendLine(" </tr>")

                For j As Integer = 0 To dt.Rows.Count - 1
                    sb.AppendLine(" <tr>")
                    For k As Integer = 0 To dt.Columns.Count - 1
                        Dim strName As String = dt.Columns(k).ColumnName.ToString()
                        Dim StrData As String = ""

                        Select Case strName
                            Case "Product"
                                strName = "ProductName"
                            Case "Medium"
                                strName = "MediumName"
                            Case "PickBy"
                                strName = "TeamName"
                            Case "Location"
                                strName = "LocationName"
                            Case Else
                                strName = strName
                        End Select
                        If Not IsDBNull(dt.Rows(j).Item(strName)) Then
                            StrData = dt.Rows(j).Item(strName)
                        End If
                        If strName = "FollowUpDate" Then
                            StrData = obj.GetFormattedDateTime(StrData)
                        ElseIf strName = "LastUpdated" Then
                            StrData = obj.GetFormattedDateTime(StrData)
                        ElseIf strName = "CreateDate" Then
                            StrData = obj.GetFormattedDateTime(StrData)
                        End If
                        Dim Taxid() As String = strName.Split("0")
                        sb.AppendLine("     <td>")
                        If Taxid.Length <= 1 Then
                            sb.AppendLine(StrData)
                        Else
                            sb.AppendLine("         <a href='Leads.aspx?Id=" & Taxid(1) & "'>" & StrData & "</a>")
                        End If
                        sb.AppendLine("     </td>")
fin:
                    Next
                    sb.AppendLine(" </tr>")

                Next

                sb.AppendLine("</table>")
            End If
        End If



        Return sb.ToString

    End Function
    Public Function GetleadsAlert(ByVal dt As DataTable, Optional ByVal CompanyId As Integer = 0, Optional ByVal IsArchived As Boolean = False, _
                                  Optional ByVal PageName As String = "", Optional ByVal SMS As Boolean = True, _
                              Optional ByVal Email As Boolean = True) As String
        Dim dtCust As New DataTable
        Dim sb As New StringBuilder
        Dim cnt As Integer = 0

        Dim mainPage As String = ""

        mainPage = PageName

        PageName = crypt.Base64Encode(PageName)

        'dtCust = obj.GetDataTable("SELECT * FROM CustomFields WHERE ViewInList=1 AND CompanyId=" & CompanyId & " ORDER BY sequence")

        If dt.Rows.Count > 0 Then
            cnt = dt.Columns.Count
            sb.AppendLine("<table Class='table'>")

            sb.AppendLine(" <tr>")
            For i As Integer = 0 To dt.Columns.Count - 1
                Dim strHead As String = ""

                strHead = dt.Columns(i).ColumnName.ToString()
                sb.AppendLine("     <th>")
                sb.AppendLine(strHead)
                sb.AppendLine("     </th>")

            Next
            sb.AppendLine(" </tr>")

            For j As Integer = 0 To dt.Rows.Count - 1
                sb.AppendLine(" <tr>")
                For k As Integer = 0 To dt.Columns.Count - 1
                    Dim strName As String = dt.Columns(k).ColumnName.ToString()
                    Dim StrData As String = ""

                    Select Case strName
                        Case "Product"
                            strName = "ProductName"
                        Case "Medium"
                            strName = "MediumName"
                        Case "PickBy"
                            strName = "TeamName"
                        Case "Location"
                            strName = "LocationName"
                        Case Else
                            strName = strName
                    End Select

                    'If Role = "Team" Then
                    '    If strName = "TeamName" Then
                    '        GoTo fin
                    '    End If
                    'End If

                    If Not IsDBNull(dt.Rows(j).Item(strName)) Then
                        StrData = dt.Rows(j).Item(strName)
                    End If
                    If StrData <> "" And Not IsDBNull(StrData) Then
                        If strName = "FollowUpDate" Then
                            StrData = obj.GetFormattedDateTime(StrData)
                        ElseIf strName = "LastUpdated" Then
                            StrData = obj.GetFormattedDateTime(StrData)
                        ElseIf strName = "CreateDate" Then
                            StrData = obj.GetFormattedDateTime(StrData)
                        End If
                    End If
                    sb.AppendLine("     <td>")
                    sb.AppendLine(StrData)
                    sb.AppendLine("     </td>")
                    'End If


fin:
                Next
                sb.AppendLine(" </tr>")
                If IsArchived = False Then
                    sb.AppendLine(" <tr>")
                    sb.AppendLine("     <td colspan='" + cnt.ToString + "'>")
                    sb.AppendLine("         <a class='vbbtn' href='ViewLead.aspx?Id=" & dt.Rows(j).Item("LeadId") & "&uri=" & PageName & "'>View</a>")
                    sb.AppendLine("         ")
                  

                    ' sb.AppendLine("         <a class='vbbtn' href='AddFollowUp.aspx?Id=" & dt.Rows(j).Item("LeadId") & "&uri=" & PageName & "'>Add FollowUp</a>")
                    If dt.Rows(j).Item("LeadCount") = 0 Then
                        sb.AppendLine("<a class='vbbtn' href='AddFollowUp.aspx?Id=" & dt.Rows(j).Item("LeadId") & "&uri=" & PageName & "'>Add FollowUp</a>")


                    Else
                        sb.AppendLine("<a class='vbbtn' href='AddFollowUp.aspx?Id=" & dt.Rows(j).Item("LeadId") & "&uri=" & PageName & "'>Add FollowUp {" & dt.Rows(j).Item("LeadCount") & "}</a>")

                    End If



                If SMS = True Then
                        If Not IsDBNull(dt.Rows(j).Item("Mobile")) Then
                            If dt.Rows(j).Item("Mobile") <> "" Then
                                sb.AppendLine("         ")
                                sb.AppendLine("         <a class='vbbtn' href='LeadSMS.aspx?Id=" & dt.Rows(j).Item("LeadId") & "&uri=" & PageName & "'>Send SMS</a>")
                            End If
                        End If
                    End If
                    If Email = True Then
                        If Not IsDBNull(dt.Rows(j).Item("EmailAddress")) Then
                            If dt.Rows(j).Item("EmailAddress") <> "" Then
                                sb.AppendLine("         ")
                                sb.AppendLine("         <a class='vbbtn' href='LeadEmail.aspx?Id=" & dt.Rows(j).Item("LeadId") & "&uri=" & PageName & "'>Send Email</a>")
                            End If
                        End If
                    End If
                    sb.AppendLine("     </td>")
                    sb.AppendLine(" </tr>")
                End If
            Next

            sb.AppendLine("</table>")
        End If


        Return sb.ToString

    End Function
    Public Function GetleadsAlertPage(ByVal dt As DataTable, Optional ByVal CompanyId As Integer = 0, Optional ByVal IsArchived As Boolean = False, _
                                  Optional ByVal PageName As String = "", Optional ByVal SMS As Boolean = True, _
                              Optional ByVal Email As Boolean = True) As String
        Dim dtCust As New DataTable
        Dim sb As New StringBuilder
        Dim cnt As Integer = 0

        Dim mainPage As String = ""

        mainPage = PageName

        PageName = crypt.Base64Encode(PageName)

        'dtCust = obj.GetDataTable("SELECT * FROM CustomFields WHERE ViewInList=1 AND CompanyId=" & CompanyId & " ORDER BY sequence")

        If dt.Rows.Count > 0 Then
            cnt = dt.Columns.Count
            sb.AppendLine("<table Class='table'>")

            sb.AppendLine(" <tr>")
            For i As Integer = 0 To dt.Columns.Count - 1
                Dim strHead As String = ""

                strHead = dt.Columns(i).ColumnName.ToString()

                ' strHead = dtCust.Rows(i).Item("CustomName")


                'If Role = "Team" Then
                '    If strHead = "PickBy" Then
                '        cnt = cnt - 1
                '        GoTo HeadFin
                '    End If
                'End If
                sb.AppendLine("     <th>")
                sb.AppendLine(strHead)
                sb.AppendLine("     </th>")
                'HeadFin:
            Next
            sb.AppendLine(" </tr>")

            For j As Integer = 0 To dt.Rows.Count - 1
                sb.AppendLine(" <tr>")
                For k As Integer = 0 To dt.Columns.Count - 1
                    Dim strName As String = dt.Columns(k).ColumnName.ToString()
                    Dim StrData As String = ""

                    Select Case strName
                        Case "Product"
                            strName = "ProductName"
                        Case "Medium"
                            strName = "MediumName"
                        Case "PickBy"
                            strName = "TeamName"
                        Case "Location"
                            strName = "LocationName"
                        Case Else
                            strName = strName
                    End Select

                    'If Role = "Team" Then
                    '    If strName = "TeamName" Then
                    '        GoTo fin
                    '    End If
                    'End If

                    If Not IsDBNull(dt.Rows(j).Item(strName)) Then
                        StrData = dt.Rows(j).Item(strName)
                    End If
                    If strName = "FollowUpDate" Then
                        StrData = obj.GetFormattedDateTime(StrData)
                    ElseIf strName = "LastUpdated" Then
                        StrData = obj.GetFormattedDateTime(StrData)
                    ElseIf strName = "CreateDate" Then
                        StrData = obj.GetFormattedDateTime(StrData)
                    End If
                    'If FromPage = "AvailableLeads" Then
                    '    If strName = "Query" Then
                    '        sb.AppendLine("     <td style='display:none'>")
                    '        sb.AppendLine(StrData)
                    '        sb.AppendLine("     </td>")
                    '    Else
                    '        sb.AppendLine("     <td>")
                    '        sb.AppendLine(StrData)
                    '        sb.AppendLine("     </td>")
                    '    End If
                    'Else
                    sb.AppendLine("     <td>")
                    sb.AppendLine(StrData)
                    sb.AppendLine("     </td>")
                    'End If


fin:
                Next
                sb.AppendLine(" </tr>")
                If IsArchived = False Then
                    sb.AppendLine(" <tr>")
                    sb.AppendLine("     <td colspan='" + cnt.ToString + "'>")
                    sb.AppendLine("         <a class='vbbtn' href='ViewLead.aspx?Id=" & dt.Rows(j).Item("LeadId") & "&uri=" & PageName & "'>View</a>")
                    sb.AppendLine("         ")
                    If mainPage = "AvaliableLeads" Then
                        sb.AppendLine("         <a class='vbbtn' href='AddFollowUp.aspx?Id=" & dt.Rows(j).Item("LeadId") & "&uri=" & PageName & "'>Pick</a>")
                    Else
                       
                        sb.AppendLine("         <a class='vbbtn' href='AddFollowUp.aspx?Id=" & dt.Rows(j).Item("LeadId") & "&uri=" & PageName & "'>Add FollowUp</a>")
                    


                    End If

                    If SMS = True Then
                        If Not IsDBNull(dt.Rows(j).Item("Mobile")) And dt.Rows(j).Item("Mobile") <> "" Then
                            sb.AppendLine("         ")
                            sb.AppendLine("         <a class='vbbtn' href='LeadSMS.aspx?Id=" & dt.Rows(j).Item("LeadId") & "&uri=" & PageName & "'>Send SMS</a>")
                        End If

                    End If
                    If Email = True Then
                        If Not IsDBNull(dt.Rows(j).Item("EmailAddress")) And dt.Rows(j).Item("EmailAddress") <> "" Then
                            sb.AppendLine("         ")
                            sb.AppendLine("         <a class='vbbtn' href='LeadEmail.aspx?Id=" & dt.Rows(j).Item("LeadId") & "&uri=" & PageName & "'>Send Email</a>")
                        End If
                    End If
                    sb.AppendLine("     </td>")
                    sb.AppendLine(" </tr>")
                End If
            Next

            sb.AppendLine("</table>")
        End If


        Return sb.ToString

    End Function
    Public Function GetCustomer(ByVal dt As DataTable, Optional ByVal CompanyId As Integer = 0, Optional ByVal PageName As String = "", _
                              Optional ByVal Role As String = "", Optional ByVal IsLeadSystem As String = "", Optional ByVal Enddate As String = "", _
                              Optional ByVal GridSize As Integer = 50, Optional ByVal LblInfo As Label = Nothing, Optional ByVal LnkPrev As Object = Nothing, Optional ByVal LnkNext As Object = Nothing, _
                        Optional ByVal DrpPages As Object = Nothing, Optional ByVal CurrentPage As Integer = 1, Optional ByVal IsArchived As Boolean = False, Optional ByVal FollowUp As Boolean = True, Optional ByVal SMS As Boolean = True, _
                              Optional ByVal Email As Boolean = True, Optional ByVal View As Boolean = True, Optional ByVal Query As Boolean = True, _
                              Optional ByVal FromPage As String = "") As String



        Dim page As Double = 0
        Dim Total As Integer = 0
        If Not IsNothing(dt) Then
            If dt.Rows.Count > 0 Then
                Total = dt.Rows.Count + 1
                If (Total Mod GridSize) = 0 Then
                    page = Total / GridSize
                Else
                    page = Total / GridSize
                    page = page + 1
                    Dim str As String()
                    str = page.ToString().Split(".")
                    page = str(0)
                End If
            End If
        End If

        If Total = 0 Then
            LblInfo.Text = "Oops! No Data found"
        Else
            Dim current = 0, Last As Integer = 0
            current = ((CurrentPage - 1) * GridSize) + 1
            Last = CurrentPage * GridSize
            If Last > Total Then
                LblInfo.Text = "Showing " & current & " to " & Total & " of " & Total
            Else
                LblInfo.Text = "Showing " & current & " to " & Last & " of " & Total
            End If
        End If

        If page > 1 Then
            DrpPages.visible = True
            DrpPages.Items.Clear()
            For i As Integer = 1 To page
                DrpPages.Items.insert(i - 1, New ListItem(i.ToString, i.ToString))
            Next
            DrpPages.SelectedValue = CurrentPage

            If CurrentPage = 1 Then
                LnkPrev.visible = False
            Else
                LnkPrev.visible = True
            End If

            If CurrentPage = page Then
                LnkNext.visible = False
            Else
                LnkNext.visible = True
            End If
        Else
            DrpPages.visible = False
            LnkPrev.visible = False
            LnkNext.visible = False
        End If
        Dim dtCust As New DataTable
        Dim sb As New StringBuilder
        Dim cnt As Integer = 0
        If dt.Rows.Count > 0 Then
            Dim ProdId As String = obj.GetField("Select CustomerId from ProductMaster where ProductId= " & dt.Rows(0).Item("ProductId"), "CustomerId")

            Dim DtFollowUp As DataTable = obj.GetDataTable("sELECT * FROM CustomerMaster where CustomerId = " & ProdId)
        End If
        Dim mainPage As String = ""

        mainPage = PageName

        PageName = crypt.Base64Encode(PageName)
        Dim abc As String = "SELECT * FROM CustomFields WHERE ViewInCustomerList=1 and FiledName != 'BranchId' and FiledName != 'MediumId' and FiledName != 'Query' AND CompanyId=" & CompanyId & " ORDER BY sequence"
        dtCust = obj.GetDataTable("SELECT * FROM CustomFields WHERE ViewInCustomerList=1 and FiledName != 'BranchId' and FiledName != 'MediumId' and FiledName != 'Query' AND CompanyId=" & CompanyId & " ORDER BY sequence")
        'dtCust = dt
        If dtCust.Rows.Count > 0 Then
            cnt = dtCust.Rows.Count + 1
            sb.AppendLine("<table Class='table table-responsive'>")

            sb.AppendLine(" <tr>")
            sb.AppendLine("<th id='abc'>")
            'sb.AppendLine("<input type='checkbox' name='vehicle1' value='Bike'>")
            sb.AppendLine("</th>")
            'sb.AppendLine("<th>")
            ''sb.AppendLine("<img src='Images/flag.png' alt='Smiley face' width='42' height='42'>")
            'sb.AppendLine("</th>")
            For i As Integer = 0 To dtCust.Rows.Count - 1
                Dim strHead As String = ""

                If Not IsDBNull(dtCust.Rows(i).Item("CustomName")) And dtCust.Rows(i).Item("CustomName") <> "" Then
                    strHead = dtCust.Rows(i).Item("CustomName")
                Else
                    strHead = dtCust.Rows(i).Item("FiledName")
                End If

                If Role = "Team" Then
                    If strHead = "PickBy" Then
                        cnt = cnt - 1
                        GoTo HeadFin
                    End If
                End If
                If FromPage = "AvailableLeads" Then
                    If strHead = "Query" Then
                        sb.AppendLine("<th style='display:none'>")
                        sb.AppendLine(strHead)
                        sb.AppendLine("</th>")
                    Else
                        sb.AppendLine("<th>")
                        sb.AppendLine(strHead)
                        sb.AppendLine("</th>")
                    End If
                Else

                    If strHead = "Query" Then
                        sb.AppendLine("<th style='width:10%'>")
                        sb.AppendLine(strHead)
                        sb.AppendLine("</th>")
                    Else
                        sb.AppendLine("<th>")
                        sb.AppendLine(strHead)
                        sb.AppendLine("</th>")
                    End If
                End If

HeadFin:
            Next
            sb.AppendLine(" </tr>")

            For j As Integer = 0 To dt.Rows.Count - 1
                sb.AppendLine(" <tr>")
                'sb.AppendLine("<td>")

                sb.AppendLine("<td>")
                sb.AppendLine("<input type='checkbox' runat='server' name='CkhSendBulkTransact' value='" & dt.Rows(j).Item("LeadId") & "'>")
                sb.AppendLine("</td>")
                For k As Integer = 0 To dtCust.Rows.Count - 1
                    Dim strName As String = dtCust.Rows(k).Item("FiledName")
                    Dim StrData As String = ""

                    Select Case strName
                        Case "Product"
                            strName = "ProductName"
                        Case "Medium"
                            strName = "MediumName"
                        Case "PickBy"
                            strName = "TeamName"
                        Case "Location"
                            strName = "LocationName"
                        Case Else
                            strName = strName
                    End Select

                    If Role = "Team" Then
                        If strName = "TeamName" Then
                            GoTo fin
                        End If
                    End If
                    If strName <> "MediumName" And strName <> "Tax" And strName <> "TeamName" And strName <> "LocationName" Then
                        If Not IsDBNull(dt.Rows(j).Item(strName)) Then
                            StrData = dt.Rows(j).Item(strName)
                        End If
                    End If
                    If strName = "FollowUpDate" Then
                        StrData = obj.GetFormattedDateTime(StrData)
                    ElseIf strName = "LastUpdated" Then
                        StrData = obj.GetFormattedDateTime(StrData)
                    ElseIf strName = "CreateDate" Then
                        StrData = obj.GetFormattedDateTime(StrData)
                    End If
                    If FromPage = "AvailableLeads" Then
                        If strName = "Query" Then
                            sb.AppendLine("     <td style='display:none'>")
                            sb.AppendLine(StrData)
                            sb.AppendLine("     </td>")
                        Else
                            sb.AppendLine("     <td>")
                            sb.AppendLine(StrData)
                            sb.AppendLine("     </td>")
                        End If
                    Else
                        sb.AppendLine("     <td>")
                        sb.AppendLine(StrData)
                        sb.AppendLine("     </td>")
                    End If


fin:
                Next
                sb.AppendLine(" </tr>")
                'If IsArchived = False Then

                '    If IsLeadSystem = False Then
                '        sb.AppendLine(" <tr style='display:none'>")
                '    ElseIf IsLeadSystem = True And Enddate < DateTime.Now And Enddate <> "" Then
                '        sb.AppendLine(" <tr style='display:none'>")
                '    Else
                '        sb.AppendLine("<tr align='left'>")
                '    End If
                '    sb.AppendLine("<td colspan='" + cnt.ToString + "'>")
                '    'If dt.Rows(j).Item("flags") = 1 Then
                '    '    sb.AppendLine("<img src='Images/flag.png' alt='alert' width='42' height='42' title='You are viewing this flag because you have missed followup'>")
                '    'End If
                '    sb.AppendLine("<a class='vbbtn' href='ViewLead.aspx?Id=" & dt.Rows(j).Item("LeadId") & "&uri=" & PageName & "'>View</a>")
                '    If FollowUp = True Then
                '        sb.AppendLine("")
                '        If mainPage = "AvaliableLeads" Then
                '            sb.AppendLine("<a class='vbbtn' href='AddFollowUp.aspx?Id=" & dt.Rows(j).Item("LeadId") & "&uri=" & PageName & "'>Pick</a>")
                '        Else
                '            If dt.Rows(j).Item("LeadCount") = 0 Then
                '                sb.AppendLine("<a class='vbbtn' href='AddFollowUp.aspx?Id=" & dt.Rows(j).Item("LeadId") & "&uri=" & PageName & "'>Add FollowUp</a>")


                '            Else
                '                sb.AppendLine("<a class='vbbtn' href='AddFollowUp.aspx?Id=" & dt.Rows(j).Item("LeadId") & "&uri=" & PageName & "'>Add FollowUp {" & dt.Rows(j).Item("LeadCount") & "}</a>")

                '            End If


                '        End If
                '    End If
                '    If Not IsDBNull(dt.Rows(j).Item("PickBy")) And dt.Rows(j).Item("PickBy") <> 0 Then
                '        sb.AppendLine("")
                '        sb.AppendLine("<a class='vbbtn' href='Reassign.aspx?Id=" & dt.Rows(j).Item("LeadId") & "&uri=" & PageName & "'>Re-Assign</a>")
                '    End If
                '    If SMS = True Then
                '        If Not IsDBNull(dt.Rows(j).Item("Mobile")) And dt.Rows(j).Item("Mobile") <> "" Then

                '            sb.AppendLine("")
                '            sb.AppendLine("<a class='vbbtn' href='LeadSMS.aspx?Id=" & dt.Rows(j).Item("LeadId") & "&uri=" & PageName & "'>Send SMS</a>")

                '        End If

                '    End If
                '    If Email = True Then
                '        If Not IsDBNull(dt.Rows(j).Item("EmailAddress")) And dt.Rows(j).Item("EmailAddress") <> "" Then

                '            sb.AppendLine("")
                '            sb.AppendLine("<a class='vbbtn' href='LeadEmail.aspx?Id=" & dt.Rows(j).Item("LeadId") & "&uri=" & PageName & "'>Send Email</a>")

                '        End If
                '    End If
                '    sb.AppendLine("     </td>")
                '    sb.AppendLine(" </tr>")
                'End If
            Next

            sb.AppendLine("</table>")
        End If


        Return sb.ToString

    End Function
    Public Function GetArchstring(ByVal dt As DataTable, Optional ByVal CompanyId As Integer = 0, Optional ByVal PageName As String = "", _
                              Optional ByVal Role As String = "", Optional ByVal IsLeadSystem As String = "", Optional ByVal Enddate As String = "", _
                              Optional ByVal IsArchived As Boolean = False, Optional ByVal FollowUp As Boolean = True, Optional ByVal SMS As Boolean = True, _
                              Optional ByVal Email As Boolean = True, Optional ByVal View As Boolean = True, Optional ByVal Query As Boolean = True, _
                              Optional ByVal FromPage As String = "") As String
        Dim dtCust As New DataTable
        Dim sb As New StringBuilder
        Dim cnt As Integer = 0
        If dt.Rows.Count > 0 Then
            Dim ProdId As String = obj.GetField("Select CustomerId from ProductMaster where ProductId= " & dt.Rows(0).Item("ProductId"), "CustomerId")

            Dim DtFollowUp As DataTable = obj.GetDataTable("sELECT * FROM CustomerMaster where CustomerId = " & ProdId)
        End If
        Dim mainPage As String = ""

        mainPage = PageName

        PageName = crypt.Base64Encode(PageName)

        dtCust = obj.GetDataTable("SELECT * FROM CustomFields WHERE ViewInList=1 AND CompanyId=" & CompanyId & " ORDER BY sequence")

        If dtCust.Rows.Count > 0 Then
            cnt = dtCust.Rows.Count + 1
            sb.AppendLine("<table Class='table table-responsive'>")

            sb.AppendLine(" <tr>")
            sb.AppendLine("<th id='abc'>")
            'sb.AppendLine("<input type='checkbox' name='vehicle1' value='Bike'>")
            sb.AppendLine("</th>")
            'sb.AppendLine("<th>")
            ''sb.AppendLine("<img src='Images/flag.png' alt='Smiley face' width='42' height='42'>")
            'sb.AppendLine("</th>")
            For i As Integer = 0 To dtCust.Rows.Count - 1
                Dim strHead As String = ""

                If Not IsDBNull(dtCust.Rows(i).Item("CustomName")) And dtCust.Rows(i).Item("CustomName") <> "" Then
                    strHead = dtCust.Rows(i).Item("CustomName")
                Else
                    strHead = dtCust.Rows(i).Item("FiledName")
                End If

                If Role = "Team" Then
                    If strHead = "PickBy" Then
                        cnt = cnt - 1
                        GoTo HeadFin
                    End If
                End If
                If FromPage = "AvailableLeads" Then
                    If strHead = "Query" Then
                        sb.AppendLine("<th style='display:none'>")
                        sb.AppendLine(strHead)
                        sb.AppendLine("</th>")
                    Else
                        sb.AppendLine("<th>")
                        sb.AppendLine(strHead)
                        sb.AppendLine("</th>")
                    End If
                Else

                    If strHead = "Query" Then
                        sb.AppendLine("<th style='width:10%'>")
                        sb.AppendLine(strHead)
                        sb.AppendLine("</th>")
                    Else
                        sb.AppendLine("<th>")
                        sb.AppendLine(strHead)
                        sb.AppendLine("</th>")
                    End If
                End If

HeadFin:
            Next
            sb.AppendLine(" </tr>")

            For j As Integer = 0 To dt.Rows.Count - 1
                sb.AppendLine(" <tr>")
                'sb.AppendLine("<td>")
                'If dt.Rows(j).Item("flags") = 1 Then
                '    sb.AppendLine("<img src='Images/flag.png' alt='alert' width='42' height='42' title='You are viewing this flag because you have missed followup'>")
                'End If
                'sb.AppendLine("</td>")
                sb.AppendLine("<td>")
                sb.AppendLine("<input type='checkbox' runat='server' name='CkhSendBulkTransact' value='" & dt.Rows(j).Item("LeadId") & "'>")
                sb.AppendLine("</td>")
                For k As Integer = 0 To dtCust.Rows.Count - 1
                    Dim strName As String = dtCust.Rows(k).Item("FiledName")
                    Dim StrData As String = ""

                    Select Case strName
                        Case "Product"
                            strName = "ProductName"
                        Case "Medium"
                            strName = "MediumName"
                        Case "PickBy"
                            strName = "TeamName"
                        Case "Location"
                            strName = "LocationName"
                        Case Else
                            strName = strName
                    End Select

                    If Role = "Team" Then
                        If strName = "TeamName" Then
                            GoTo fin
                        End If
                    End If

                    If Not IsDBNull(dt.Rows(j).Item(strName)) Then
                        StrData = dt.Rows(j).Item(strName)
                    End If
                    If strName = "FollowUpDate" Then
                        StrData = obj.GetFormattedDateTime(StrData)
                    ElseIf strName = "LastUpdated" Then
                        StrData = obj.GetFormattedDateTime(StrData)
                    ElseIf strName = "CreateDate" Then
                        StrData = obj.GetFormattedDateTime(StrData)
                    End If
                    If FromPage = "AvailableLeads" Then
                        If strName = "Query" Then
                            sb.AppendLine("     <td style='display:none'>")
                            sb.AppendLine(StrData)
                            sb.AppendLine("     </td>")
                        Else
                            sb.AppendLine("     <td>")
                            sb.AppendLine(StrData)
                            sb.AppendLine("     </td>")
                        End If
                    Else
                        sb.AppendLine("     <td>")
                        sb.AppendLine(StrData)
                        sb.AppendLine("     </td>")
                    End If


fin:
                Next
                sb.AppendLine(" </tr>")
                If IsArchived = False Then

                    If IsLeadSystem = False Then
                        sb.AppendLine(" <tr style='display:none'>")
                    ElseIf IsLeadSystem = True And Enddate < DateTime.Now And Enddate <> "" Then
                        sb.AppendLine(" <tr style='display:none'>")
                    Else
                        sb.AppendLine("<tr align='left'>")
                    End If
                    sb.AppendLine("<td colspan='" + cnt.ToString + "'>")

                    If dt.Rows(j).Item("flags") = 1 Then

                        sb.AppendLine("<img src='Images/flag.png' alt='alert' width='42' height='42' title='You are viewing this flag because you have missed followup'>")
                    Else
                        sb.AppendLine("")
                    End If
                    sb.AppendLine("<a class='vbbtn' href='ViewLead.aspx?Id=" & dt.Rows(j).Item("LeadId") & "&uri=" & PageName & "'>View</a>")
                    If FollowUp = True Then
                        sb.AppendLine("")
                        If mainPage = "AvaliableLeads" Then
                            sb.AppendLine("<a class='vbbtn' href='AddFollowUp.aspx?Id=" & dt.Rows(j).Item("LeadId") & "&uri=" & PageName & "'>Pick</a>")
                        Else
                            If dt.Rows(j).Item("LeadCount") = 0 Then
                                sb.AppendLine("<a class='vbbtn' href='AddFollowUp.aspx?Id=" & dt.Rows(j).Item("LeadId") & "&uri=" & PageName & "'>Add FollowUp</a>")


                            Else
                                sb.AppendLine("<a class='vbbtn' href='AddFollowUp.aspx?Id=" & dt.Rows(j).Item("LeadId") & "&uri=" & PageName & "'>Add FollowUp {" & dt.Rows(j).Item("LeadCount") & "}</a>")

                            End If


                        End If
                    End If
                    If Not IsDBNull(dt.Rows(j).Item("PickBy")) And dt.Rows(j).Item("PickBy") <> 0 Then
                        sb.AppendLine("")
                        sb.AppendLine("<a class='vbbtn' href='Reassign.aspx?Id=" & dt.Rows(j).Item("LeadId") & "&uri=" & PageName & "'>Re-Assign</a>")
                    End If
                    If SMS = True Then
                        If Not IsDBNull(dt.Rows(j).Item("Mobile")) And dt.Rows(j).Item("Mobile") <> "" Then

                            sb.AppendLine("")
                            sb.AppendLine("<a class='vbbtn' href='LeadSMS.aspx?Id=" & dt.Rows(j).Item("LeadId") & "&uri=" & PageName & "'>Send SMS</a>")

                        End If

                    End If
                    If Email = True Then
                        If Not IsDBNull(dt.Rows(j).Item("EmailAddress")) And dt.Rows(j).Item("EmailAddress") <> "" Then

                            sb.AppendLine("")
                            sb.AppendLine("<a class='vbbtn' href='LeadEmail.aspx?Id=" & dt.Rows(j).Item("LeadId") & "&uri=" & PageName & "'>Send Email</a>")

                        End If
                    End If
                    sb.AppendLine("     </td>")
                    sb.AppendLine(" </tr>")
                End If
            Next

            sb.AppendLine("</table>")
        End If


        Return sb.ToString

    End Function
End Class
