﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.HttpContext
Imports System.IO
Imports System.Net.Mail
Imports System.Net

Public Module ApplicationGlobal
    Public CnnString As String = ConfigurationManager.AppSettings("CnnStr")
    Public WebURL As String = ConfigurationManager.AppSettings("WebURL") 'Current.Request.Url.Scheme & "://" & Current.Request.Url.Host
    Public GridPageSize As Integer = 15

    'Public EmailUID1 As String = ConfigurationManager.AppSettings("EmailID1") ' "business@drippindia.com"
    'Public FromName1 As String = "Dripp India"
    'Public EmailHost As String = ConfigurationManager.AppSettings("mailHost")
    'Public EmailUserName As String = ConfigurationManager.AppSettings("mailUserName")
    'Public EmailPassword As String = ConfigurationManager.AppSettings("mailPassword")


    Public Function UploadFile(ByVal TmpUpload As FileUpload, Optional ByVal pat As String = "", Optional ByVal type As String = "") As String
        Dim Fext, FileName, FlName As String
        FileName = ""
        Try
            FlName = TmpUpload.PostedFile.FileName.ToString
            FileName = Path.GetFileNameWithoutExtension(FlName)
            If FileName <> "" Then
                Fext = LCase(Path.GetExtension(FlName))
                Dim bool As Boolean = False
                If type = "image" Then
                    If Fext = ".jpg" Or Fext = ".jpeg" Or Fext = ".gif" Or Fext = ".png" Or Fext = ".bmp" Then bool = True
                ElseIf type = "doc" Then
                    If Fext = ".doc" Or Fext = ".docx" Or Fext = ".pdf" Then bool = True
                ElseIf type = "" Then
                    bool = True
                End If

                If bool Then
                    FileName = Replace(FileName & "_" & Now.Day & "-" & Now.Month & "-" & Now.Year & "-" & Now.Hour & Now.Minute & Now.Second & Fext, " ", "")
                    pat = GetApplicationPath() & pat & FileName
                    If File.Exists(pat) = True Then File.Delete(pat)
                    TmpUpload.SaveAs(pat)
                End If
            End If
        Catch ex As Exception

        End Try
        
        Return FileName
    End Function

    Public Function GetApplicationPath() As String
        Dim ImgPath As String
        ImgPath = Current.Server.MapPath("Image")
        ImgPath = Left(ImgPath, Len(ImgPath) - 5)
        GetApplicationPath = ImgPath
    End Function
End Module


Public Class Engine
    Private cnn As New SqlConnection
    Public PgGridSize As Integer = 50
    Public pg As New PagedDataSource
    Public Pagecount As Long = 1
    Dim sp As Integer = 0
    Public Sub iSendMail(ByVal EmailUserName As String, ByVal EmailPassword As String, ByVal EmailHost As String, ByVal FromEmail As String, ByVal ToEmail As String, ByVal EmailSubject As String, _
                    ByVal EmailBody As String, Optional ByVal FromName As String = "", Optional ByVal ToName As String = "", Optional ByVal AttachFile As String = Nothing, Optional ByVal AttachFile1 As String = Nothing, Optional ByVal AttachFile2 As String = Nothing)
        Try
            Dim NewsMessage As New MailMessage
            Dim mailclient As New SmtpClient
            Dim basicAuthenticationInfo As New Net.NetworkCredential()
            basicAuthenticationInfo.UserName = EmailUserName
            basicAuthenticationInfo.Password = EmailPassword

            NewsMessage.From = New MailAddress(FromEmail, FromName)
            NewsMessage.Subject = EmailSubject
            NewsMessage.Priority = MailPriority.Normal
            NewsMessage.IsBodyHtml = True

            NewsMessage.Body = EmailBody
            NewsMessage.To.Add(New MailAddress(Trim(ToEmail), ToName))

            'mailclient.Host = "127.0.0.1"
            mailclient.Host = EmailHost

            mailclient.UseDefaultCredentials = False
            mailclient.Credentials = basicAuthenticationInfo
            mailclient.Timeout = 520000
            mailclient.Send(NewsMessage)
        Catch ex As Exception
        End Try
    End Sub
    Public Sub SetRoleLink(ByVal pag As Object)
        If Not IsNothing(HttpContext.Current.Session("Role")) Then
            Dim dtRight As New DataTable
            dtRight = HttpContext.Current.Session("Role")
            If Not IsNothing(dtRight) Then
                For i As Integer = 0 To dtRight.Rows.Count - 1
                    Try
                        CType(pag.FindControl("Lnk" & dtRight(i)("RightName")), LinkButton).Visible = True
                    Catch ex As Exception
                    End Try
                Next
            End If
        End If
    End Sub

    Public Function Random(Optional ByVal str As String = "0123456789", Optional ByVal len As Integer = 6) As String
        Dim s As String = str
        Dim r As New Random
        Dim sb As New StringBuilder
        For i As Integer = 1 To len
            Dim idx As Integer = r.Next(0, len)
            sb.Append(s.Substring(idx, 1))
        Next
        Return sb.ToString()
    End Function
    Public Function setUploadImage(ByVal categoryid As Integer, Optional ByVal QS As String = "") As String
        Dim str As String = ""
        If categoryid <> 0 Then
            str &= WebURL & "CompleteDesign.aspx?CategoryID=" & categoryid & IIf(QS <> "", "&" & QS, "")
        Else
            str &= WebURL & "CompleteDesign.aspx" & IIf(QS <> "", "?" & QS, "")
        End If
        
        'str = ""

        'If categoryid <> 0 Then
        '    str &= WebURL & "CustomisedDesign.aspx?CategoryID=" & categoryid & IIf(QS <> "", "&" & QS, "")
        'Else
        '    str &= WebURL & "CustomisedDesign.aspx" & IIf(QS <> "", "?" & QS, "")
        'End If

        Return str
    End Function

    Public Function setProductList(ByVal categoryid As Integer, Optional ByVal QS As String = "") As String
        Dim str As String = ""
        If categoryid <> 0 Then
            str &= WebURL & "ProductList.aspx?CategoryID=" & categoryid & IIf(QS <> "", "&" & QS, "")
        Else
            str &= WebURL & "ProductList.aspx" & IIf(QS <> "", "?" & QS, "")
        End If
        Return str
    End Function

    Public Function setStartLogo(ByVal categoryid As Integer, Optional ByVal QS As String = "") As String
        Dim str As String = ""
        If categoryid <> 0 Then
            str &= WebURL & "CustomisedDesign.aspx?CategoryID=" & categoryid & IIf(QS <> "", "&" & QS, "")
        Else
            str &= WebURL & "CustomisedDesign.aspx" & IIf(QS <> "", "?" & QS, "")
        End If
        Return str
    End Function

    Public Function star(Optional ByVal rate As Double = 5, Optional ByVal outof As Double = 5, Optional ByVal stars As Integer = 5, Optional ByVal width As Integer = 30) As String
        Dim str As String = ""
        Dim style As String = "height:" & width & "px; background-size: " & width & "px " & width & "px; float:left;"
        str &= "<div class='star-n' style='" & style & " width:" & (width * stars) & "px;' title='" & rate & " / " & outof & "'>"
        str &= "<div class='star-y' style='" & style & " width:" & (rate / outof) * 100 & "%;'  ></div>"
        str &= "</div>"
        Return str
    End Function

    Public Function SetIframe(ByVal src As String, Optional ByVal height As String = "400px", Optional ByVal width As String = "100%") As String
        Dim str As String = ""
        Dim strarr As String()
        src = src.Replace("""", "'")
        strarr = src.Split("'")
        If strarr.Length > 1 Then src = strarr(1)
        str &= "<iframe src='" & src & "'  width='" & width & "' height='" & height & "' frameborder='0' allowfullscreen></iframe>"
        Return str
    End Function

    Public Function SetRole(ByVal RoleID As Integer) As DataTable

        Dim dtRight As New DataTable
        If Not IsNothing(RoleID) Then
            Dim strsql As String = ""

            strsql = " Select RM.RightName from RightMaster RM " & _
                    " left join RoleRight RR on RM.RightID = RR.RightID " & _
                    " where RR.RoleID = " & RoleID
            dtRight = GetDataTable(strsql)
        End If
        Return dtRight

    End Function

    Public Function SetRoleCategory(ByVal RoleID As Integer) As DataTable

        Dim dtRight As New DataTable
        If Not IsNothing(RoleID) Then
            Dim strsql As String = ""

            strsql = " Select distinct RC.RightCategoryName from RightCategoryMaster RC " & _
                    " left join RightMaster RM on RC.RoleCategoryID=R.RoleCategoryID " & _
                    " left join RoleRight RR on RM.RightID = RR.RightID " & _
                    " where RR.RoleID = " & RoleID
            dtRight = GetDataTable(strsql)
        End If
        Return dtRight

    End Function

    Public Function TableToHTML(ByVal dt As DataTable, Optional ByVal cols As String = "", Optional ByVal TableProperties As String = "", Optional ByVal header As String = "Y") As String
        Dim str As String = ""
        If cols <> "" Then
            Dim col As String()
            col = cols.Split(",")
            str &= "<table " & TableProperties & ">"
            If header = "Y" Then
                str &= "<tr>"
                For I As Integer = 0 To col.Length - 1
                    If dt.Columns.Contains(col(I)) Then
                        str &= "<th>" & col(I) & "</th>"
                    End If
                Next
                str &= "</tr>"
            End If

            For I As Integer = 0 To dt.Rows.Count - 1
                str &= "<tr>"
                For j As Integer = 0 To col.Length - 1
                    If dt.Columns.Contains(col(j)) Then
                        str &= "<td>" & dt.Rows(I).Item(col(j)) & "</td>"
                    End If
                Next
                str &= "</tr>"
            Next
            str &= "</table>"
        Else
            str &= "<table " & TableProperties & ">"
            If header = "Y" Then
                str &= "<tr>"
                For I As Integer = 0 To dt.Columns.Count - 1
                    str &= "<th>" & dt.Columns(I).ToString & "</th>"
                Next
                str &= "</tr>"
            End If
            For I As Integer = 0 To dt.Rows.Count - 1
                str &= "<tr>"
                For j As Integer = 0 To dt.Columns.Count - 1
                    str &= "<td>" & dt.Rows(I).Item(j) & "</td>"
                Next
                str &= "</tr>"
            Next
            str &= "</table>"
        End If

        Return str
    End Function

    Public Function setdt(ByVal dt As DataTable, ByVal i As Integer, ByVal col As String) As String
        Dim str As String = ""
        If Not IsNothing(dt) Then
            If dt.Rows.Count >= i Then
                If dt.Columns.Contains(col) Then
                    If Not IsDBNull(dt.Rows(i).Item(col)) Then
                        str = dt.Rows(i).Item(col).ToString.Trim
                    End If
                End If
            End If
        End If
        Return str
    End Function

    Public Function GetFormatedDate1(ByVal StrDATE As String) As String
        If IsDate(StrDATE) = True Then
            Return Format(CDate(StrDATE), "yyyy/MM/dd")
        Else
            Return StrDATE
        End If
    End Function

    Public Function HTMLRemove(ByVal source As String) As String
        Return Regex.Replace(source, "<.*?>", String.Empty)
    End Function

    Public Sub PreventDuplicateEntry()
        Current.Response.Redirect(Current.Request.Url.ToString(), False)
    End Sub

    Public Function ReturnIPAddres() As String
        Return Current.Request.UserHostAddress.ToString()
    End Function

    'Public Sub SendMailAttachment(ByVal FromEmail As String, ByVal ToEmail As String, Optional ByVal EmailSubject As String = "", _
    '                Optional ByVal EmailBody As String = "", Optional ByVal CC As String = "", Optional ByVal Bcc As String = "", _
    '                Optional ByVal FromName As String = "Dripp India", Optional ByVal ToName As String = "", Optional ByVal AttachmentFiles As ArrayList = Nothing)
    '    Dim i, iCnt As Integer
    '    Try
    '        Dim NewsMessage As New MailMessage
    '        Dim mailclient As New SmtpClient
    '        Dim basicAuthenticationInfo As New Net.NetworkCredential()
    '        basicAuthenticationInfo.UserName = EmailUserName
    '        basicAuthenticationInfo.Password = EmailPassword

    '        If Not AttachmentFiles Is Nothing Then
    '            iCnt = AttachmentFiles.Count - 1
    '            For i = 0 To iCnt
    '                If AttachmentFiles(i) <> "" Then
    '                    If FileExists(AttachmentFiles(i)) Then
    '                        Dim Attachment As Attachment = New Attachment(AttachmentFiles(i))
    '                        NewsMessage.Attachments.Add(Attachment)
    '                    End If
    '                End If
    '            Next
    '        End If

    '        NewsMessage.From = New MailAddress(EmailUID1, FromName)
    '        NewsMessage.ReplyTo = New MailAddress("business@drippindia.com", FromName)

    '        NewsMessage.Subject = EmailSubject
    '        NewsMessage.Priority = MailPriority.Normal
    '        NewsMessage.IsBodyHtml = True
    '        NewsMessage.Body = EmailBody
    '        Dim To1() As String
    '        To1 = ToEmail.Split(";"c, ","c)
    '        For i = 0 To To1.Length - 1
    '            If To1(i).Trim <> "" Then NewsMessage.To.Add(New MailAddress(To1(i).Trim, ToName))
    '        Next
    '        Dim CC1() As String
    '        CC1 = CC.Split(";"c, ","c)
    '        For i = 0 To CC1.Length - 1
    '            If CC1(i).Trim <> "" Then NewsMessage.CC.Add(New MailAddress(CC1(i).Trim, ToName))
    '        Next
    '        NewsMessage.Bcc.Add(New MailAddress(EmailUID1.Trim, ToName))
    '        mailclient.Host = EmailHost

    '        mailclient.UseDefaultCredentials = False
    '        mailclient.Credentials = basicAuthenticationInfo
    '        mailclient.Timeout = 520000
    '        mailclient.Send(NewsMessage)

    '    Catch ex As Exception
    '        Current.Response.Write(ex.Message)
    '    End Try
    'End Sub

    Public Function SendSMS(ByVal StrMobileNo As String, ByVal StrSMSMessage As String) As Boolean
        Try
            If StrMobileNo <> "" Then
                Dim strRecip As String = StrMobileNo, strMsgText As String = StrSMSMessage

                Dim objURI As Uri = New Uri("http://trans.innovatorwebsolutions.com/sendsms.aspx?mobile=9029080606&pass=Abc@1234" & _
                                            "&to=" & StrMobileNo & "&msg=" & StrSMSMessage & "&senderid=INNVTR")

                Dim objWebRequest As WebRequest = WebRequest.Create(objURI)
                Dim objWebResponse As WebResponse = objWebRequest.GetResponse()
                Dim objStream As Stream = objWebResponse.GetResponseStream()
                Dim objStreamReader As StreamReader = New StreamReader(objStream)
                Dim strHTML As String = objStreamReader.ReadToEnd

                If strHTML.Contains("Invalid mobile number") Then
                    Return False
                Else
                    Return True
                End If
            Else
                Return False
            End If
            
        Catch ex As Exception
            Return False
        End Try
    End Function
    Private Function FileExists(ByVal FileFullPath As String) As Boolean
        If Trim(FileFullPath) = "" Then Return False
        Dim f As New IO.FileInfo(FileFullPath)
        Return f.Exists
    End Function

    Public Function GetFileName(ByVal FUploder As String) As String
        Dim FileNameOnly As String
        FileNameOnly = Path.GetFileNameWithoutExtension(FUploder)
        Return FileNameOnly
    End Function

    Public Function GetFileExtension(ByVal FUploder As String) As String
        Dim ExtensionOnly As String
        ExtensionOnly = Path.GetExtension(FUploder)
        Return ExtensionOnly
    End Function
    Public Function UplodeFileToServer(ByVal FullPath As String, ByVal FUploder As FileUpload)
        FUploder.SaveAs(FullPath)
    End Function

    Public Function download(ByVal body As String, ByVal path As String, ByVal file As String) As Boolean
        Try
            Using sw As New System.IO.StreamWriter(path + file)
                sw.WriteLine(body)
                sw.Close()
            End Using
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub OpenConnection()
        cnn.ConnectionString = CnnString
        cnn.Open()
    End Sub


    Public Function BindGriControlNotPaging(ByVal Grd As Object, ByVal sql As String) As Integer
        Dim count As Integer = 0
        If cnn.State = ConnectionState.Closed Then OpenConnection()

        Try
            Dim ds As New DataSet
            Dim Ad As New SqlDataAdapter
            Dim cmd As New SqlCommand("SetProcedure", cnn)

            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@qry", SqlDbType.VarChar)
            cmd.Parameters("@qry").Value = sql
            cmd.CommandTimeout = 36000000

            Ad = New SqlDataAdapter(cmd)
            Ad.Fill(ds, "tmp")
            Grd.DataSource = ds
            Grd.DataBind()

            count = ds.Tables(0).Rows.Count
        Catch ex As Exception
            Current.Response.Write(ex.Message)
        End Try

        If cnn.State = ConnectionState.Open Then cnn.Close()
        Return count
    End Function

    Public Function BindGriControlNotPaging1(ByVal Grd As Object, ByVal dt As DataTable) As Integer
        Dim count As Integer = 0
        Try
            Grd.DataSource = dt
            Grd.DataBind()
            count = dt.Rows.Count
        Catch ex As Exception
            Current.Response.Write(ex.Message)
        End Try
        Return count
    End Function

    Public Function GetFormattedDate(ByVal dt As Object) As String
        If IsDBNull(dt) = False Then
            Return (Format(CDate(dt), "dd-MMM-yyyy"))
        Else
            Return dt
        End If
    End Function

    Public Function GetFormatedAmount(ByVal Obj As Object) As Object
        If IsNumeric(Obj) Then
            Return FormatNumber(Obj, 0)
        Else
            Return Obj
        End If
    End Function

    Public Function GetFormatedNumber(ByVal Obj As Object) As Object
        If IsNumeric(Obj) Then
            Return FormatNumber(Obj, 2).ToString.Replace(",", "")
        Else
            Return Obj
        End If
    End Function

    Public Function GetFormatedNumberWithOutDecimal(ByVal Obj As Object) As Object
        If Not IsDBNull(Obj) Then
            Return FormatNumber(Obj, 0).ToString.Replace(",", "")
        Else
            Return Obj
        End If
    End Function

    Public Function GetFormattedDateTime(ByVal dt As Object)
        If IsDBNull(dt) = False Then
            Return (Format(CDate(dt), "dd-MM-yy hh:mm tt"))
        Else
            Return dt
        End If
    End Function

    Public Sub SendEMail(ByVal ToName As String, ByVal ToAddress As String, ByVal subject As String, ByVal content As String, ByVal FromName As String, ByVal FromEmail As String, ByVal EmailPass As String, ByVal SMTPHost As String, ByVal EmailBCC As String)
        Try
            Dim StrHtmlbody As String
            Dim NewsMessage As New MailMessage
            Dim mailclient As New SmtpClient
            Dim basicAuthenticationInfo As New Net.NetworkCredential()
            basicAuthenticationInfo.UserName = FromEmail
            basicAuthenticationInfo.Password = EmailPass

            NewsMessage.From = New MailAddress(FromEmail, FromName)
            NewsMessage.Subject = subject
            NewsMessage.Priority = MailPriority.Normal
            NewsMessage.IsBodyHtml = True

            StrHtmlbody = content

            NewsMessage.Body = StrHtmlbody

            NewsMessage.To.Add(New MailAddress(ToAddress, ToName))
            If EmailBCC <> "" Then
                NewsMessage.Bcc.Add(New MailAddress(EmailBCC))
            End If

            mailclient.Host = SMTPHost
            mailclient.UseDefaultCredentials = False
            mailclient.Credentials = basicAuthenticationInfo
            mailclient.Send(NewsMessage)
        Catch ex As Exception
            Current.Response.Write(ex.Message)
        End Try
    End Sub

    Public Sub PreviousRecord()
        Pagecount = Pagecount - 1
    End Sub

    Public Sub NextRecord()
        Pagecount = Pagecount + 1
    End Sub

    Public Sub OnRecord(ByVal Cnt As Long)
        Pagecount = Cnt
    End Sub

    Public Function BindGridControl(ByVal TmpGrid As Object, Optional ByVal StrSQL As String = Nothing, Optional ByVal LblHidePGCount As Label = Nothing, _
                                    Optional ByVal LblDisplayPGCount As Label = Nothing, Optional ByVal LblNoDataFound As Label = Nothing, _
                                    Optional ByVal DisplayMsg As String = Nothing, Optional ByVal PreviousLinkName As String = Nothing, _
                                    Optional ByVal NextLinkName As String = Nothing, Optional ByVal DropdowlListName As String = Nothing, _
                                    Optional ByVal MyGridPageSize As Long = 0) As Object
        Try
            If MyGridPageSize <> 0 Then PgGridSize = MyGridPageSize
            pg = New PagedDataSource
            Dim dt As New DataTable
            dt = GetDataTable(StrSQL)
            Pagecount = 1
            If Not IsNothing(LblHidePGCount) And LblHidePGCount.Text <> "" Then
                Pagecount = Val(LblHidePGCount.Text)
            End If

            Dim intReturned As Integer = dt.Rows.Count

            If dt.Rows.Count = 0 Then
                TmpGrid.Visible = False
                LblNoDataFound.Text = "Oops! " & DisplayMsg & " not found."
                LblDisplayPGCount.Text = ""
            Else
                LblNoDataFound.Text = ""
                pg.AllowPaging = True
                pg.PageSize = PgGridSize
                pg.DataSource = dt.DefaultView

                If Pagecount <= 0 Then Pagecount = Pagecount + 1
                If Pagecount > pg.PageCount Then Pagecount = pg.PageCount

                Dim li As ListItem
                pg.CurrentPageIndex = Pagecount - 1
                If Not IsNothing(LblDisplayPGCount) Then
                    Dim pageno As Integer = pg.CurrentPageIndex
                    If intReturned < (pg.PageSize * (pageno + 1)) Then
                        ''If itS on 1st Pg OR LasT Pg
                        LblDisplayPGCount.Text = DisplayMsg & " : " & (pg.PageSize * (pg.CurrentPageIndex + 1) - pg.PageSize) + 1 & "-" & intReturned & "/" & intReturned
                    Else
                        ''itS on Other PgS.
                        LblDisplayPGCount.Text = DisplayMsg & " : " & ((pg.PageSize - pg.PageSize * (pg.CurrentPageIndex + 1)) - 1) * -1 & "-" & pg.PageSize * (pg.CurrentPageIndex + 1) & "/" & intReturned
                    End If

                End If
                TmpGrid.DataSource = pg
                TmpGrid.Visible = True
                TmpGrid.DataBind()

                If DropdowlListName <> Nothing Then
                    CType(TmpGrid.Controls(TmpGrid.Controls.Count - 1).FindControl(DropdowlListName), DropDownList).Items.Clear()
                    For i As Integer = 0 To (pg.PageCount - 1)
                        li = New ListItem(i + 1, i + 1)
                        CType(TmpGrid.Controls(TmpGrid.Controls.Count - 1).FindControl(DropdowlListName), DropDownList).Items.Add(li)
                    Next
                    CType(TmpGrid.Controls(TmpGrid.Controls.Count - 1).FindControl(DropdowlListName), DropDownList).SelectedValue = LblHidePGCount.Text
                End If

                If pg.PageCount > 1 Then
                    If pg.IsFirstPage = True Then
                        CType(TmpGrid.Controls(TmpGrid.Controls.Count - 1).FindControl(PreviousLinkName), LinkButton).Visible = False
                    ElseIf pg.IsLastPage = True Then
                        CType(TmpGrid.Controls(TmpGrid.Controls.Count - 1).FindControl(NextLinkName), LinkButton).Visible = False
                    Else
                        CType(TmpGrid.Controls(TmpGrid.Controls.Count - 1).FindControl(NextLinkName), LinkButton).Visible = True
                        CType(TmpGrid.Controls(TmpGrid.Controls.Count - 1).FindControl(PreviousLinkName), LinkButton).Visible = True
                    End If
                Else
                    CType(TmpGrid.Controls(TmpGrid.Controls.Count - 1).FindControl(NextLinkName), LinkButton).Visible = False
                    CType(TmpGrid.Controls(TmpGrid.Controls.Count - 1).FindControl(PreviousLinkName), LinkButton).Visible = False
                End If

                If IsNothing(LblHidePGCount) = False Then
                    LblHidePGCount.Text = Pagecount
                End If
            End If
            Return intReturned
        Catch ex As Exception
            Current.Response.Write(ex.Message)
        Finally
            If cnn.State = ConnectionState.Open Then cnn.Close()

        End Try
    End Function

    Public Sub BindListControl(ByVal Lst As Object, ByVal sql As String, ByVal TextField As String, Optional ByVal ValueFld As String = "")

        If cnn.State = ConnectionState.Closed Then OpenConnection()
        Try
            Dim ds As New DataSet
            Dim Ad As New SqlDataAdapter

            Dim cmd As New SqlCommand("SetProcedure", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@qry", SqlDbType.VarChar)
            cmd.Parameters("@qry").Value = sql
            cmd.CommandTimeout = 36000000

            Ad = New SqlDataAdapter(cmd)
            Ad.Fill(ds, "tmp")
            Lst.DataTextField = TextField
            If ValueFld <> "" Then
                Lst.DataValueField = ValueFld
            End If
            Lst.DataSource = ds
            Lst.DataBind()

        Catch ex As Exception
            Current.Response.Write(ex.Message)
        End Try
        If cnn.State = ConnectionState.Open Then cnn.Close()
    End Sub

    Public Sub BindListControl1(ByVal Lst As Object, ByVal dt As DataTable, ByVal TextField As String, Optional ByVal ValueFld As String = "")

        Try
            Lst.DataTextField = TextField
            If ValueFld <> "" Then
                Lst.DataValueField = ValueFld
            End If
            Lst.DataSource = dt
            Lst.DataBind()
        Catch ex As Exception
            Current.Response.Write(ex.Message)
        End Try
    End Sub

    Public Function GetField(ByVal SqlQuery As String, ByVal FieldName As String) As Object
        Dim ob As New Object
        ob = ""
        If cnn.State = ConnectionState.Closed Then OpenConnection()
        Try
            Dim ds As New DataSet
            Dim Ad As New SqlDataAdapter

            Dim cmd As New SqlCommand("SetProcedure", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@qry", SqlDbType.VarChar)
            cmd.Parameters("@qry").Value = SqlQuery
            cmd.CommandTimeout = 36000000

            Ad = New SqlDataAdapter(cmd)
            Ad.Fill(ds, "tmp")
            If Not IsNothing(ds) Then
                If ds.Tables(0).Rows.Count > 0 Then
                    ob = ds.Tables(0).Rows(0).Item(FieldName)
                End If
                ds.Clear()
            End If

        Catch ex As Exception
            Current.Response.Write(ex.Message)
        End Try
        If cnn.State = ConnectionState.Open Then cnn.Close()
        Return ob.ToString
    End Function

    Public Sub ExecuteCommand(ByVal SqlQuery As String)
        If cnn.State = ConnectionState.Closed Then OpenConnection()
        Try
            Dim cmd As New SqlCommand("SetProcedure", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@qry", SqlDbType.VarChar)
            cmd.Parameters("@qry").Value = SqlQuery
            cmd.CommandTimeout = 36000000
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Current.Response.Write(ex.Message)
        End Try
        If cnn.State = ConnectionState.Open Then cnn.Close()
    End Sub

    Public Function GetTable(ByVal SqlQuery As String) As DataTable
        Dim dt As New DataTable
        If cnn.State = ConnectionState.Closed Then OpenConnection()
        Try
            Dim ds As New DataSet
            Dim Ad As New SqlDataAdapter
            Dim cmd As New SqlCommand("SetProcedure", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@qry", SqlDbType.VarChar)
            cmd.Parameters("@qry").Value = SqlQuery
            cmd.CommandTimeout = 36000000

            Ad = New SqlDataAdapter(cmd)
            Ad.Fill(ds, "tmp")
            If cnn.State = ConnectionState.Open Then cnn.Close()
            dt = ds.Tables(0)
        Catch ex As Exception
            Current.Response.Write(ex.Message)
        End Try
        If cnn.State = ConnectionState.Open Then cnn.Close()
        Return dt
    End Function

    Public Function ExcelGetTable(ByVal SqlQuery As String, ByVal ExcelCnnString As String) As DataTable
        Dim cnn1 As New OleDb.OleDbConnection
        Dim ds As New DataTable
        Dim cmd1 As New OleDb.OleDbCommand
        Dim Ad As New OleDb.OleDbDataAdapter
        Try
            If cnn1.State = ConnectionState.Closed Then
                cnn1.ConnectionString = ExcelCnnString
                cnn1.Open()
            End If
            With cmd1
                .Connection = cnn1
                .CommandType = CommandType.Text
                .CommandText = SqlQuery
                .CommandTimeout = 36000000
            End With
            Ad = New OleDb.OleDbDataAdapter(cmd1)
            Ad.Fill(ds, "tmp")
            If cnn1.State = ConnectionState.Open Then cnn1.Close()


        Catch ex As Exception
            Current.Response.Write(ex.Message)
        End Try
        If cnn1.State = ConnectionState.Open Then cnn1.Close()
        Return ds
    End Function

    Public Function GetDataset(ByVal SqlQuery As String) As DataSet
        Dim ds As New DataSet
        Dim cmd As New SqlCommand("SetProcedure", cnn)
        Dim Ad As New SqlDataAdapter
        Try
            If cnn.State = ConnectionState.Closed Then OpenConnection()
            With cmd
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add("@qry", SqlDbType.VarChar)
                .Parameters("@qry").Value = SqlQuery
                .CommandTimeout = 36000000
            End With

            Ad = New SqlDataAdapter(cmd)
            Ad.Fill(ds, "tmp")
            If cnn.State = ConnectionState.Open Then cnn.Close()

            Return ds
        Catch ex As Exception
            Current.Response.Write(ex.Message)
        Finally
            If cnn.State = ConnectionState.Open Then cnn.Close()

        End Try
    End Function

    Public Function GetDataTable(ByVal SqlQuery As String) As DataTable

        If cnn.State = ConnectionState.Closed Then OpenConnection()
        Dim ds As New DataSet
        Try
            Dim Ad As New SqlDataAdapter
            Dim cmd As New SqlCommand("SetProcedure", cnn)

            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@qry", SqlDbType.VarChar)
            cmd.Parameters("@qry").Value = SqlQuery
            cmd.CommandTimeout = 36000000

            Ad = New SqlDataAdapter(cmd)
            Ad.Fill(ds, "tmp")
            If cnn.State = ConnectionState.Open Then cnn.Close()
            Return ds.Tables(0)

        Catch ex As Exception
            Current.Response.Write(ex.Message)
        Finally
            If cnn.State = ConnectionState.Open Then cnn.Close()
        End Try
    End Function

    Public Function RemoveFormat(ByVal data As String) As String
        Dim dataarr() As String
        Dim res As String = ""
        data = data.Replace("<", "< ")
        data = data.Replace(">", " >")
        dataarr = data.Split(" ")
        For i As Integer = 0 To dataarr.Length - 1
            If isformat(dataarr(i)) Then
                sp = 1
            Else
                sp = 0
            End If
            If sp = 0 Then
                res += dataarr(i) & " "
            Else

            End If
        Next
        res = res.Replace("< ", "<")
        res = res.Replace(" >", ">")
        Return res
    End Function

    Public Function isformat(ByVal str As String) As Boolean
        str = str.ToLower
        If Left(str, 5) = "style" Or Left(str, 5) = "class" Then
            Return True
        Else
            If sp = 0 And Right(str, 1) = "'" Then
                Return True
            Else
                Return False
            End If
        End If
    End Function




End Class


Public Class RecordSetRead
    Private Ds As New DataSet
    Private DsPosition As Long
    Private cnn As New SqlConnection

    Private Sub OpenConnection()
        cnn.ConnectionString = CnnString
        cnn.Open()
    End Sub

    Public Sub Open(ByVal SqlQuery As String)
        If cnn.State = ConnectionState.Closed Then OpenConnection()
        Try
            Dim Ad As New SqlDataAdapter
            Dim cmd As New SqlCommand("SetProcedure", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@qry", SqlDbType.VarChar)
            cmd.Parameters("@qry").Value = SqlQuery
            cmd.ExecuteNonQuery()
            cmd.CommandTimeout = 36000000

            Ad = New SqlDataAdapter(cmd)
            Ad.Fill(Ds, "tmp")

        Catch ex As Exception
            Current.Response.Write(ex.Message)
        End Try
        DsPosition = 0
        If cnn.State = ConnectionState.Open Then cnn.Close()
    End Sub

    Public Sub Close()
        Ds.Clear()
        DsPosition = 0
    End Sub

    Public Function GetField(ByVal FieldName As Object)
        GetField = Ds.Tables(0).Rows(DsPosition).Item(FieldName).ToString
    End Function

    Public Function GetFieldNumber(ByVal FieldNumber As Integer)
        GetFieldNumber = Ds.Tables(0).Rows(DsPosition).Item(FieldNumber).ToString
    End Function

    Public Function GetCursorPosition()
        Return DsPosition
    End Function

    Public Function GetRecordCount()
        Return Ds.Tables(0).Rows.Count
    End Function

    Public Sub GoToRecordCount(ByVal RecordNumber As Long)
        DsPosition = RecordNumber
    End Sub

    Public Sub MoveLast()
        DsPosition = Ds.Tables(0).Rows.Count - 1
    End Sub

    Public Sub MoveFirst()
        DsPosition = 0
    End Sub

    Public Sub MovePrevious()
        'If DsPosition > -1 Then
        DsPosition = DsPosition - 1
        'End If
    End Sub

    Public Sub MoveNext()
        'If DsPosition < Ds.Tables(0).Rows.Count Then
        DsPosition = DsPosition + 1
        'End If
    End Sub

    Public Function EOF() As Boolean
        If DsPosition >= Ds.Tables(0).Rows.Count Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function BOF() As Boolean
        If DsPosition < 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function GetFieldName(ByVal FieldNum As Integer) As String
        GetFieldName = Ds.Tables(0).Columns(FieldNum).ToString
    End Function

    Public Function GeatAllFieldName() As String()
        Dim str() As String
        Dim i As Integer
        For i = 0 To Ds.Tables(0).Columns.Count - 1
            ReDim Preserve str(i)
            str(i) = Ds.Tables(0).Columns(i).ToString
        Next
        Return str
    End Function

    Public Function GetFieldcounts() As Integer
        GetFieldcounts = Ds.Tables(0).Columns.Count
    End Function

End Class

Public Class RecorsetUpdate
    Private StrComma, StrSql, StrFields, strValues, gMode, gTableName, StrWhere As String
    Private cnn As New SqlConnection

    Private Sub OpenConnection()
        cnn.ConnectionString = CnnString
        cnn.Open()
    End Sub


    Public Sub TableParameter(ByVal TableName As Object, ByVal Mode As Object, Optional ByVal WhereParameter As Object = Nothing)
        gTableName = TableName
        gMode = Mode
        If gMode = "Add" Then
            StrFields = "Insert into " & gTableName & " ("
            strValues = " values ("
        ElseIf gMode = "Edit" Then
            StrWhere = WhereParameter
            StrSql = "Update " & TableName & " set "
        End If
        StrComma = ""
    End Sub

    Public Sub SetField(ByVal FieldName As Object, ByVal Value As Object, ByVal datatype As String)
        If gMode = "Add" Then
            StrFields = StrFields & StrComma & FieldName
            If datatype = "Number" Then
                If IsNothing(Value) Then Value = 0
                strValues = strValues & StrComma & Value
            Else
                If IsNothing(Value) Then Value = ""
                Value = Value.ToString.Replace("''", "'").Replace("'", "''")
                strValues = strValues & StrComma & "'" & Value & "'"

            End If
        ElseIf gMode = "Edit" Then
            If datatype = "Number" Then
                If IsNothing(Value) Then Value = 0
                StrSql = StrSql & StrComma & FieldName & "=" & Value
            Else
                If IsNothing(Value) Then Value = ""
                Value = Value.ToString.Replace("''", "'").Replace("'", "''")
                StrSql = StrSql & StrComma & FieldName & "='" & Value & "'"
            End If
        End If

        StrComma = ","
    End Sub

    Public Sub Execute1()
        Dim gCmd As New SqlClient.SqlCommand
        If cnn.State = ConnectionState.Closed Then OpenConnection()

        With gCmd
            .Connection = cnn
            .CommandType = CommandType.Text
            If gMode = "Add" Then
                StrSql = StrFields & ")" & strValues & ")"
                .CommandText = StrSql
                'MsgBox(StrSql)
            ElseIf gMode = "Edit" Then
                StrSql = StrSql & " Where " & StrWhere
                .CommandText = StrSql
            End If
            .ExecuteNonQuery()
        End With
        If cnn.State = ConnectionState.Open Then cnn.Close()
    End Sub

    Public Sub Execute()

        If cnn.State = ConnectionState.Closed Then OpenConnection()

        If gMode = "Add" Then
            StrSql = StrFields & ")" & strValues & ")"
        ElseIf gMode = "Edit" Then
            StrSql = StrSql & " Where " & StrWhere
        End If


        Dim cmd As New SqlCommand("SetProcedure", cnn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@qry", SqlDbType.VarChar)
        cmd.Parameters("@qry").Value = StrSql
        cmd.ExecuteNonQuery()

        If cnn.State = ConnectionState.Open Then cnn.Close()
    End Sub
End Class

Public Class AppGlobal
    Public ReadOnly Property SqlConnectionString() As String
        Get
            Dim StrCnnString As String
            'StrCnnString = "Server=(local);Initial Catalog=swansoft;UID=sa;PWD=s;"
            'StrCnnString = "Server=(local);Initial Catalog=DLBuilders;UID=sa;PWD=p;"
            'Dim StrCnnString As String = ConfigurationManager.AppSettings("StrCnn")
            StrCnnString = CnnString
            Return StrCnnString
        End Get
    End Property

    Public Function BindGridControlWithPaging(ByVal Grd As Object, ByVal StrQuery As String, _
                        ByVal PageToSelect As Integer, ByVal Pagesize As Integer, _
                        Optional ByVal LblInfo As Label = Nothing, Optional ByVal StrInfo As String = "", _
                        Optional ByVal LnkPrev As Object = Nothing, Optional ByVal LnkNext As Object = Nothing, _
                        Optional ByVal DrpPages As Object = Nothing, Optional ByVal lblDrpTitle As Label = Nothing) As Object

        Dim ObjPageDatasource As New PagedDataSource
        Dim ObjDs As System.Data.DataSet
        Dim ObjEng As New Engine

        ObjDs = ObjEng.GetDataset(StrQuery)

        If Not IsNothing(ObjDs) Then

            If ObjDs.Tables(0).Rows.Count > 0 Then
                Grd.Visible = True
                If Not IsNothing(lblDrpTitle) Then
                    lblDrpTitle.Visible = True
                End If

                ObjPageDatasource.AllowPaging = True
                ObjPageDatasource.PageSize = Pagesize

                ObjPageDatasource.DataSource = ObjDs.Tables(0).DefaultView
                ObjPageDatasource.CurrentPageIndex = PageToSelect - 1

                Grd.DataSource = ObjPageDatasource
                Grd.DataBind()

                'Binds fropdown items
                Dim TotalPageCount, i As Integer

                If Not IsNothing(DrpPages) Then
                    DrpPages.visible = True
                    DrpPages.Items.Clear()
                    While i * Pagesize < ObjDs.Tables(0).Rows.Count
                        Dim DrpItem As New ListItem
                        DrpItem.Text = i + 1
                        DrpItem.Value = i + 1
                        DrpPages.Items.Insert(i, DrpItem)
                        TotalPageCount = TotalPageCount + 1
                        i = i + 1
                    End While
                    DrpPages.Items.FindByValue(PageToSelect).Selected = True
                Else
                    While i * Pagesize < ObjDs.Tables(0).Rows.Count
                        TotalPageCount = TotalPageCount + 1
                        i = i + 1
                    End While
                End If

                If Not IsNothing(LnkNext) And Not IsNothing(LnkPrev) Then
                    LnkPrev.visible = True
                    LnkNext.visible = True
                    If PageToSelect = TotalPageCount And PageToSelect = 1 Then
                        LnkPrev.visible = False
                        LnkNext.visible = False
                    ElseIf PageToSelect = TotalPageCount Then
                        LnkNext.visible = False
                        LnkPrev.visible = True
                    ElseIf PageToSelect = 1 Then
                        LnkNext.visible = True
                        LnkPrev.visible = False
                    End If
                End If

                If Not IsNothing(LblInfo) Then
                    LblInfo.Visible = True
                    If (Pagesize * (PageToSelect - 1)) + Pagesize > ObjDs.Tables(0).Rows.Count Then
                        LblInfo.Text = StrInfo & " " & (Pagesize * (PageToSelect - 1)) + 1 & " to " & _
                                                ObjDs.Tables(0).Rows.Count & " of " & ObjDs.Tables(0).Rows.Count
                    Else
                        LblInfo.Text = StrInfo & " " & (Pagesize * (PageToSelect - 1)) + 1 & " to " & _
                                                (Pagesize * (PageToSelect - 1)) + Pagesize & " of " & ObjDs.Tables(0).Rows.Count
                    End If
                End If
            Else
                Grd.Visible = False
                If Not IsNothing(DrpPages) Then
                    DrpPages.visible = False
                End If

                If Not IsNothing(LnkNext) And Not IsNothing(LnkPrev) Then
                    LnkNext.visible = False
                    LnkPrev.visible = False
                End If

                If Not IsNothing(lblDrpTitle) Then
                    lblDrpTitle.Visible = False
                End If
                If Not IsNothing(LblInfo) Then
                    LblInfo.Visible = False
                End If
            End If
            Return ObjDs.Tables(0).Rows.Count
        Else
            Return 0

        End If



    End Function


    Public Function BindDataTableWithPaging(ByVal Grd As Object, ByVal Dt As DataTable, _
                        ByVal PageToSelect As Integer, ByVal Pagesize As Integer, _
                        Optional ByVal LblInfo As Label = Nothing, Optional ByVal StrInfo As String = "", _
                        Optional ByVal LnkPrev As Object = Nothing, Optional ByVal LnkNext As Object = Nothing, _
                        Optional ByVal DrpPages As Object = Nothing, Optional ByVal lblDrpTitle As Label = Nothing) As Object

        Dim ObjPageDatasource As New PagedDataSource
        Dim ObjDs As System.Data.DataSet
        Dim ObjEng As New Engine


        If Dt.Rows.Count > 0 Then
            Grd.Visible = True
            If Not IsNothing(lblDrpTitle) Then
                lblDrpTitle.Visible = True
            End If

            ObjPageDatasource.AllowPaging = True
            ObjPageDatasource.PageSize = Pagesize

            ObjPageDatasource.DataSource = Dt.DefaultView
            ObjPageDatasource.CurrentPageIndex = PageToSelect - 1

            Grd.DataSource = ObjPageDatasource
            Grd.DataBind()

            'Binds fropdown items
            Dim TotalPageCount, i As Integer

            If Not IsNothing(DrpPages) Then
                DrpPages.visible = True
                DrpPages.Items.Clear()
                While i * Pagesize < Dt.Rows.Count
                    Dim DrpItem As New ListItem
                    DrpItem.Text = i + 1
                    DrpItem.Value = i + 1
                    DrpPages.Items.Insert(i, DrpItem)
                    TotalPageCount = TotalPageCount + 1
                    i = i + 1
                End While
                DrpPages.Items.FindByValue(PageToSelect).Selected = True
            Else
                While i * Pagesize < Dt.Rows.Count
                    TotalPageCount = TotalPageCount + 1
                    i = i + 1
                End While
            End If

            If Not IsNothing(LnkNext) And Not IsNothing(LnkPrev) Then
                LnkPrev.visible = True
                LnkNext.visible = True
                If PageToSelect = TotalPageCount And PageToSelect = 1 Then
                    LnkPrev.visible = False
                    LnkNext.visible = False
                ElseIf PageToSelect = TotalPageCount Then
                    LnkNext.visible = False
                    LnkPrev.visible = True
                ElseIf PageToSelect = 1 Then
                    LnkNext.visible = True
                    LnkPrev.visible = False
                End If
            End If

            If Not IsNothing(LblInfo) Then
                LblInfo.Visible = True
                If (Pagesize * (PageToSelect - 1)) + Pagesize > Dt.Rows.Count Then
                    LblInfo.Text = StrInfo & " " & (Pagesize * (PageToSelect - 1)) + 1 & " to " & _
                                            Dt.Rows.Count & " of " & Dt.Rows.Count
                Else
                    LblInfo.Text = StrInfo & " " & (Pagesize * (PageToSelect - 1)) + 1 & " to " & _
                                            (Pagesize * (PageToSelect - 1)) + Pagesize & " of " & Dt.Rows.Count
                End If
            End If
        Else
            Grd.Visible = False
            If Not IsNothing(DrpPages) Then
                DrpPages.visible = False
            End If

            If Not IsNothing(LnkNext) And Not IsNothing(LnkPrev) Then
                LnkNext.visible = False
                LnkPrev.visible = False
            End If

            If Not IsNothing(lblDrpTitle) Then
                lblDrpTitle.Visible = False
            End If
            If Not IsNothing(LblInfo) Then
                LblInfo.Visible = False
            End If
        End If

        Return Dt.Rows.Count
    End Function
    Public Function GetFormatedNumber(ByVal Obj As Object) As Object
        Return FormatNumber(Obj, 2).ToString.Replace(",", "")
    End Function

    Public Function GetFormatedNumberWithOutDecimal(ByVal Obj As Object) As Object
        If Not IsDBNull(Obj) Then
            Return FormatNumber(Obj, 0).ToString.Replace(",", "")
        End If
    End Function

    Public Function GetFormatedMonthYear(ByVal Obj As Object) As Object
        Return Format(Obj, "MMM - yyyy")
    End Function

    Public Function GetFormatedDateTime(ByVal StrDATE As String)
        If IsDate(StrDATE) = True Then
            Return Format(CDate(StrDATE), "dd-MMM-yyyy HH:mm:ss tt")
        Else
            Return StrDATE
        End If
    End Function

    Public Function GetFormatedDate(ByVal StrDATE As String)
        If IsDate(StrDATE) = True Then
            Return Format(CDate(StrDATE), "dd-MMM-yyyy")
        Else
            Return StrDATE
        End If
    End Function

    Public Function GetRendomCode(Optional ByVal length As Integer = 6) As String
        Dim s As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
        Dim r As New Random
        Dim sb As New StringBuilder
        For i As Integer = 1 To length     ' 8 is length of rendom word.
            Dim idx As Integer = r.Next(0, 62)    ' 62 is length of string s.
            sb.Append(s.Substring(idx, 1))
        Next
        Return sb.ToString()
    End Function


    Public Function GetFormatedDBDate(ByVal StrDATE As String)
        If IsDate(StrDATE) = True Then
            Return Format(CDate(StrDATE), "MM-dd-yyyy")
        Else
            'Return StrDATE
        End If
    End Function


End Class

