﻿Imports Microsoft.VisualBasic

Public Class Cryptography
    Public Function Base64Encode(ByVal plainText As String) As String
        Dim plainTextBytes = Encoding.UTF8.GetBytes(plainText)
        Return Convert.ToBase64String(plainTextBytes)
    End Function

    Public Function Base64Decode(ByVal base64EncodedData As String) As String
        Dim base64EncodedBytes = Convert.FromBase64String(base64EncodedData)
        Return Encoding.UTF8.GetString(base64EncodedBytes)
    End Function
End Class
