﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="webheader.ascx.vb" Inherits="webheader" %>
<link href="Css/website/bootstrap.css" rel="Stylesheet" type="text/css" />
<link href="Css/website/css.css" rel="Stylesheet" type="text/css" />
<link rel="stylesheet" href="Css/website/menu.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css">
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-134791397-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-134791397-1');
</script>

<div class="container-fluid topbg">
    <div class="container">
        <div align="center" class="row">
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="call">
                    <i class="fas fa-mobile-alt"></i><a href="tel:+91 22 4971 4222">+91 22 4971 4222</a><a> | </a><a href="tel:+917977006242">7977006242</a>
                </div>
            </div>
            <div class="col-md-9 col-sm-6 col-xs-6">
                <div class="call">
                    <i class="fas fa-envelope"></i><a href="mailto:info@innovatorsol.com ">info@innovatorsol.com
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div align="center" class="row">
        <div class="col-md-3 col-sm-3 col-xs-12">
            <a href="Default.aspx">
                <img src="Images/web/logo.png" class="max" />
            </a>
        </div>
        <div class="col-md-7 col-sm-6 col-xs-12">
            <nav class="animenu" role="navigation" aria-label="Menu">
            <div class="animenu__toggle">
            <span class="animenu__toggle__bar"></span>
            <span class="animenu__toggle__bar"></span>
            <span class="animenu__toggle__bar"></span>Menu
            </div>
            <ul class="animenu__nav">
                <li><a href="Default.aspx">Home</a></li>
                <li><a href="about-us.aspx">About Us</a></li>
                  <li><a href="feature.aspx">Features</a></li>
                   <%-- <li><a href="pricing.aspx">Pricing</a></li>--%>
                      <li><a href="contact-us.aspx">Contact Us</a></li>
               
              
            </ul>
            </nav>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-12">
            <div class="login">
                <a href="UserLogin.aspx">Customer Login </a>
            </div>
        </div>
    </div>
</div>

<script src="js/jquery.min.js"></script>

<script src="js/menu.js"></script>

