﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="BillingDetails.aspx.vb" Inherits="BillingDetails" %>

<!DOCTYPE html>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Src="~/Header.ascx" TagName="UCHeader" TagPrefix="UC1" %>
<%@ Register Src="~/footer.ascx" TagName="UCFooter" TagPrefix="UC1" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Billing Details</title>
    <link rel="icon" href="Images/thumb.png" />
    <link href="Css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="Css/css.css" rel="stylesheet" type="text/css" />
    <link href="Css/Style.css" rel="Stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div>
                <UC1:UCHeader ID="UCHeader" runat="server" />
            </div>
            <div class="container-fluid">
                <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="Updt">
                    <ProgressTemplate>
                        <asp:Image ID="imgloading1" runat="server" ImageUrl="~/Admin/Images/loader.gif"
                            CssClass="loading" />
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <asp:UpdatePanel ID="Updt" runat="server">
                    <ContentTemplate>
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="boxshadow">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="title">
                                            Create Billing Details
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="email">
                                                <span class="red">*</span>Company Name :
                                            </label>
                                            <asp:TextBox ID="txtCompanyName" runat="server" class="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="req" runat="server" ControlToValidate="txtCompanyName" ErrorMessage="<br/><br/>Enter Tax"
                                                InitialValue="" SetFocusOnError="true" Display="Dynamic" ValidationGroup="Save"></asp:RequiredFieldValidator>
                                        </div>
                                        <div class="form-group space">
                                            <label for="email">
                                                <span class="red">*</span>Billing Address :
                                            </label>
                                            <asp:TextBox ID="TxtAddress" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TxtAddress" ErrorMessage="<br/><br/>Enter Billing Address !"
                                                InitialValue="" SetFocusOnError="true" Display="Dynamic" ValidationGroup="Save"></asp:RequiredFieldValidator>
                                        </div>
                                        <div class="form-group space">
                                            <label for="email">
                                                <span class="red">*</span>Contact Person :
                                            </label>
                                            <asp:TextBox ID="TxtContactPerson" runat="server" class="form-control" AutoPostBack="true"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TxtContactPerson" ErrorMessage="<br/><br/>Enter Contact Person !"
                                                InitialValue="" SetFocusOnError="true" Display="Dynamic" ValidationGroup="Save"></asp:RequiredFieldValidator>
                                        </div>
                                        <div class="form-group space">
                                            <label for="email">
                                                GSTIN Number :
                                            </label>
                                            <asp:TextBox ID="TxtGstNo" runat="server" class="form-control" AutoPostBack="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <asp:Button runat="server" ID="BtnSubmit" Text="Submit" CssClass="dwnbtn" ValidationGroup="Save" />
                                        <asp:Button runat="server" ID="BtnCancel" Text="Cancel" CausesValidation="false"
                                            CssClass="dwnbtn" /><br />
                                        <asp:Label runat="server" ID="LblMode" Visible="false" Text="Add"></asp:Label>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label runat="server" ID="LblId" Visible="false"></asp:Label>
                                        <asp:Label runat="server" ID="LblMessage" CssClass="bold red"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div>
                <UC1:UCFooter ID="UCFooter" runat="server" />
            </div>
        </div>
    </form>
</body>
</html>
