﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="webfooter.ascx.vb" Inherits="webfooter" %>
<link href="Css/website/bootstrap.css" rel="Stylesheet" type="text/css" />
<link href="Css/website/css.css" rel="Stylesheet" type="text/css" />
<div class="container-fluid footerbg">
    
        <div align="center" class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 padding">
                <div class="col-md-11 col-sm-11 col-xs-12">
                    <div class="copytxt">
                        Copyright © InnOvator Web Solutions Pvt.Ltd
                    </div>
                </div>
                <div class="col-md-1 col-sm-1 col-xs-12">
                    <p id="bottom">
                        <a href="javascript:scrollToTop()">
                            <img src="Images/web/arrowtop.png" class="scrollerbottom" /></a>
                    </p>
                </div>
            </div>
       
    </div>
</div>

<script type="text/javascript">
    function scrollToBottom() {
        $('html, body').animate({ scrollTop: $(document).height() }, 'slow');
    }
    function scrollToTop() {
        $('html, body').animate({ scrollTop: 0 }, 'slow');
    }
    </script>

