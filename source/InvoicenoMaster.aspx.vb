﻿Imports System.Data
Imports AjaxControlToolkit
Partial Class InvoicenoMaster
    Inherits System.Web.UI.Page
    Dim Obj As New Engine
    Private Sub InvoiceNoMaster_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("CompanyId") = Nothing Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            CType(UCHeader.FindControl("LblHeading"), Label).Text = "Invoice No Master"

            TxtStartDate.Attributes.Add("readonly", "readonly")
            TxtEndDate.Attributes.Add("readonly", "readonly")
            bind()
        End If
    End Sub
    Public Sub bind()
        Dim StrSQL As String
        Dim search As String = ""

        If TxtSerName.Text <> "" Then
            search &= " and StartDate like '%" & TxtStartDate.Text.Replace("'", "''") & "%'"
        End If

        StrSQL = "Select * from InvoiceNoMaster  where CompanyId=" & Session("CompanyId") & search & " Order By InvnoID desc"

        Dim IntRecourtCount As Integer
        IntRecourtCount = Obj.BindGridControl(rep, StrSQL, LblHideUserCount, LblDispUserCount, LblNoUserCount, "Invoice No", "LnkPreviousRecord", "LnkNextRecord", "DrpOnRecord")
        If IntRecourtCount > 0 Then
            BtnDelete.Visible = True
        Else
            BtnDelete.Visible = False
        End If
    End Sub
    Private Function ValidateEnter() As String
        Dim StrError As String = ""
        Dim StrWhere As String = ""
        Dim cnt As Integer = 0
        If LblMode.Text = "Edit" Then
            StrWhere = " and InvnoID<>" & LblId.Text
        End If
        If LblMode.Text <> "Edit" Then
            cnt = Obj.GetField("select COUNT(InvnoID) as cnt from InvoiceNoMaster where StartDate='" & Obj.GetFormattedDate(TxtStartDate.Text.Replace("'", "''")) & "' and EndDate='" & Obj.GetFormattedDate(TxtEndDate.Text.Replace("'", "''")) & "' and CompanyId=" & Session("CompanyId"), "cnt")

            If cnt > 0 Then
                StrError = "Invoice No Already Exists"
            End If
        End If


        BtnCancel.Visible = True

        Return StrError
    End Function

    Private Sub BtnSubmit_Click(sender As Object, e As EventArgs) Handles BtnSubmit.Click
        Dim rs As New RecorsetUpdate
        LblMessage.Text = ""

        Dim vals As String = ValidateEnter()
        If vals = "" Then
            Dim StrID As String = ""
            If LblMode.Text = "Add" Then
                rs.TableParameter("InvoiceNoMaster", "Add")
                StrID = "(select ISNULL(MAX(InvnoID),0) AS StausId from InvoiceNoMaster where StartDate='" & Obj.GetFormattedDate(TxtStartDate.Text.Replace("'", "''")) & "' and EndDate='" & Obj.GetFormattedDate(TxtEndDate.Text.Replace("'", "''")) & "' and CompanyId=" & Session("CompanyId") & ")"
            ElseIf LblMode.Text = "Edit" Then
                rs.TableParameter("InvoiceNoMaster", "Edit", "InvnoID=" & LblId.Text)
                StrID = LblId.Text
            End If
            rs.SetField("StartDate", TxtStartDate.Text, "String")
            rs.SetField("EndDate", TxtEndDate.Text.Replace("'", "''"), "String")
            rs.SetField("InvoicePrefix", TxtInvPrefix.Text, "String")
            rs.SetField("InvoiceDigit", TxtInvDigit.Text, "Number")
            rs.SetField("CompanyId", Session("CompanyId"), "Number")
            rs.Execute()

            ClearText()
            bind()
            LblMessage.Text = "Sucessfully Updated!"
            LblMode.Text = "Add"

        ElseIf vals <> "" Then
            LblMessage.Text = ValidateEnter()
        End If
    End Sub
    Public Sub ClearText()
        TxtEndDate.Text = ""
        TxtInvDigit.Text = ""
        TxtInvPrefix.Text = ""
        TxtStartDate.Text = ""
        LblMode.Text = "Add"
        LblMessage.Text = ""
        LblId.Text = ""
    End Sub

    Private Sub BtnCancel_Click(sender As Object, e As EventArgs) Handles BtnCancel.Click
        ClearText()
    End Sub
    Protected Sub BtnDelete_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnDelete.Click
        Dim strId As String = ""
        Dim StrErr As String = ""
        For i As Integer = 0 To rep.Items.Count - 1
            If CType(rep.Items(i).FindControl("ChkUserInfo"), CheckBox).Checked = True Then
                Dim cnt As Integer = 0
                'cnt = Obj.GetField("select COUNT(LeadId) as cnt from LeadsMaster where LeadStatus='" & CType(rep.Items(i).FindControl("LblChk"), Label).Text & "'", "cnt")
                If cnt = 0 Then
                    strId &= CType(rep.Items(i).FindControl("LblChk"), Label).Text & ","
                Else
                    StrErr &= CType(rep.Items(i).FindControl("lblName"), Label).Text & ","
                End If
            End If
        Next

        If strId <> "" Then

            strId = Left(strId, strId.Length - 1)

            Dim Sql As String = "Delete From InvoiceNoMaster where InvnoID in (" & strId & ")"
            Obj.ExecuteCommand(Sql)
            bind()
            If StrErr <> "" Then
                StrErr = Left(StrErr, StrErr.Length - 1)
                LblMessage.Text = "Unable to delete " & StrErr
            Else
                LblMessage.Text = "Sucessfully Deleted !"
            End If
        End If

    End Sub


    Public Sub PreviousUserRecord(ByVal sender As Object, ByVal e As System.EventArgs)
        LblHideUserCount.Text = CInt(LblHideUserCount.Text) - 1
        Obj.PreviousRecord()
        bind()
    End Sub

    Public Sub NextUserRecord(ByVal sender As Object, ByVal e As System.EventArgs)
        LblHideUserCount.Text = CInt(LblHideUserCount.Text) + 1
        Obj.NextRecord()
        bind()
    End Sub

    Protected Sub OnUserRecord(ByVal sender As Object, ByVal e As System.EventArgs)
        LblHideUserCount.Text = CType(rep.Controls(rep.Controls.Count - 1).FindControl("DrpOnRecord"), DropDownList).SelectedValue
        Obj.OnRecord(LblHideUserCount.Text)
        bind()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        bind()
    End Sub

    Private Sub rep_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rep.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            If e.Item.DataItem("CompanyId") = 0 Then
                CType(e.Item.FindControl("ChkUserInfo"), CheckBox).Visible = False
                CType(e.Item.FindControl("LnkEdit"), LinkButton).Visible = False
            End If
            If Not IsDBNull(e.Item.DataItem("StartDate")) Then CType(e.Item.FindControl("LblStartDate"), Label).Text = e.Item.DataItem("StartDate")
            If Not IsDBNull(e.Item.DataItem("EndDate")) Then CType(e.Item.FindControl("LblEndDate"), Label).Text = e.Item.DataItem("EndDate")
            If Not IsDBNull(e.Item.DataItem("InvoicePrefix")) Then CType(e.Item.FindControl("LblInvPrefix"), Label).Text = e.Item.DataItem("InvoicePrefix")
            If Not IsDBNull(e.Item.DataItem("InvoiceDigit")) Then CType(e.Item.FindControl("LblInvDigits"), Label).Text = e.Item.DataItem("InvoiceDigit")
        End If
    End Sub

    Private Sub rep_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rep.ItemCommand
        If e.CommandName = "Edit" Then
            Dim dt As New DataTable
            dt = Obj.GetDataTable("Select * from InvoiceNoMaster where InvnoID=" & e.CommandArgument)
            If dt.Rows.Count > 0 Then
                TxtStartDate.Text = Obj.GetFormattedDate(dt.Rows(0).Item("StartDate"))
                TxtEndDate.Text = Obj.GetFormattedDate(dt.Rows(0).Item("EndDate"))
                TxtInvPrefix.Text = dt.Rows(0).Item("InvoicePrefix")
                TxtInvDigit.Text = dt.Rows(0).Item("InvoiceDigit")

                LblMode.Text = "Edit"
                LblMessage.Text = ""
                LblId.Text = e.CommandArgument
            End If
        End If

    End Sub

End Class
