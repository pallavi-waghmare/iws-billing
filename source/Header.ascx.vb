﻿
Partial Class Header
    Inherits System.Web.UI.UserControl
    Dim obj As New Engine
    Private Sub Header_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Session("CustId") = Nothing Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsNothing(Session("Company")) Then
            If Not IsNothing(Session("smscount")) And Not IsNothing(Session("Emailcount")) Then
                Dim str As String = ""
                Dim sms As Integer = obj.GetField("Select smscount from CustomerMaster where CustomerId=" & Session("CompanyId"), "smscount")
                Dim Email As Integer = obj.GetField("Select Emailcount from CustomerMaster where CustomerId=" & Session("CompanyId"), "Emailcount")
                str = Session("Company").ToString 
                '& "<br/> Sms Count -" & sms.ToString() & " And Email Count -" & Email.ToString()
                LblUserName.Text = str
            Else
                LblUserName.Text = Session("Company").ToString
            End If

        End If

        If Session("LeadSystem") = False And IsDBNull(Session("Enddate")) Then

            'liAvaLeads.Visible = False
            'liAddSingle.Visible = False
            'liAddMulti.Visible = False
            liPro.Visible = False
            liAlert.Visible = False
            'liTeam.Visible = False

            'liConfig.Visible = False
        ElseIf Session("LeadSystem") = True And Session("Enddate") < DateTime.Now Then

            'liAvaLeads.Visible = False
            'liAddSingle.Visible = False
            'liAddMulti.Visible = False
            liPro.Visible = False
            liAlert.Visible = False
            'liTeam.Visible = False

            'liConfig.Visible = False
        End If
        
        If Session("Role") = "Team" Then
            'liAddSingle.Visible = False
           'liAddMulti.Visible = False
            liPro.Visible = False
            'liTeam.Visible = False
            'liConfig.Visible = False
            'lnkAvaliable.Visible = True
            'li6.Visible = False
            'LinkButton3.Visible = False
            'lnkDailyReport.Visible = False
        Else
            'lnkAvaliable.Visible = False
        End If
        

    End Sub

    Private Sub LnkLogOut_Click(sender As Object, e As EventArgs) Handles LnkLogOut.Click
        Session.Abandon()
        Response.Redirect("UserLogin.aspx")
    End Sub
End Class
