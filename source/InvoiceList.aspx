﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="InvoiceList.aspx.vb" Inherits="InvoiceList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Src="~/Header.ascx" TagName="UCHeader" TagPrefix="UC1" %>
<%@ Register Src="~/footer.ascx" TagName="UCFooter" TagPrefix="UC1" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Customer List</title>
    <link rel="icon" href="../Images/thumb.png" />
    <link href="Css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="Css/website/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="Css/css.css" rel="stylesheet" type="text/css" />
    <link type="text/css" rel="Stylesheet" href="Css/Style.css" />

</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div>
                <UC1:UCHeader ID="UCHeader" runat="server" />
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="boxshadow">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="title">
                                    Search Parameter
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 padding">
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="email">
                                            <label for="email">
                                                Customer
                                            </label>
                                            <asp:DropDownList ID="drpSerCust" runat="server" AutoPostBack="true" class="form-control">
                                            </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="email">
                                            Product
                                        </label>
                                        <asp:DropDownList ID="drpSerPro" runat="server" class="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="email">
                                            From
                                        </label>
                                        <asp:TextBox ID="TxtFrom" runat="server" class="form-control"></asp:TextBox>
                                        <ajax:CalendarExtender ID="AjxCel1" runat="server" TargetControlID="TxtFrom" Format="dd-MMM-yyyy"
                                            CssClass="cal">
                                        </ajax:CalendarExtender>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="email">
                                            To
                                        </label>
                                        <asp:TextBox ID="TxtTo" runat="server" class="form-control"></asp:TextBox>
                                        <ajax:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="TxtTo"
                                            Format="dd-MMM-yyyy" CssClass="cal">
                                        </ajax:CalendarExtender>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="form-group">
                                        <asp:Button runat="server" ID="btnSearch" Text="Search" CssClass="button1" CausesValidation="false" />
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 space1">
                                <ul class="nav nav-pills nav-pills-justified">
                                    <li class="active"><a data-toggle="tab" href="#proforma">Proforma Invoice</a></li>
                                    <li><a data-toggle="tab" href="#tax">Tax Invoice</a></li>
                                </ul>
                            </div>
                            <div class="tab-content">
                                <div id="proforma" class="tab-pane fade in active">
                                    <div class="col-md-12 col-sm-12 col-xs-12 space1">
                                        <asp:Label ID="LblProDispUserCount" runat="server" CssClass="showtxt"></asp:Label>
                                        <asp:Label ID="LblProHideUserCount" runat="server" Visible="false" CssClass="showtxt"></asp:Label>
                                        <asp:Label ID="LblProNoUserCount" runat="server" CssClass="showtxt"></asp:Label>
                                        <asp:Label ID="lblProinfo" runat="server" CssClass="showtxt"></asp:Label>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 space">
                                        <div>
                                            <asp:Label ID="lblProLeads" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 space1">
                                        <asp:Repeater runat="server" ID="repproforma">
                                            <HeaderTemplate>
                                                <table class="table">
                                                    <tr>
                                                        <th width="2%"></th>
                                                        <th width="8%">Invoice No
                                                        </th>
                                                        <th width="8%">Name
                                                        </th>
                                                        <th width="8%">Invoice Amount
                                                        </th>
                                                        <th width="7%">Invoice Date
                                                        </th>
                                                        <th width="15%"></th>
                                                    </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td align="center">
                                                        <asp:CheckBox runat="server" ID="ChkUserInfo" />
                                                        <asp:Label runat="server" ID="LblChk" Text='<%#Container.DataItem("InvID")%>' Visible="false"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="LblInvNo"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblName"></asp:Label>
                                                        <asp:HiddenField ID="HdnCustID" runat="server" Value='<%#Container.DataItem("CustomerId")%>' />
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="LblInvAmt"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="InvDate"></asp:Label>
                                                    </td>

                                                    <td>
                                                        <asp:Label runat="server" ID="LblRemark"></asp:Label>
                                                        <asp:LinkButton ID="LnkInvoice" runat="server" CausesValidation="false" Text="View Details"
                                                            CommandArgument='<%# DataBinder.Eval(Container.DataItem, "InvID") %>'></asp:LinkButton>&nbsp; |&nbsp;
                                                        <ajax:ModalPopupExtender ID="ModalPopStatus" runat="server" TargetControlID="LnkInvoice"
                                                            PopupControlID="PanelStatus" CancelControlID="btnPopStatCancel"
                                                            BackgroundCssClass="ModalBackground" />
                                                        <asp:Panel ID="PanelStatus" runat="server" Style="display: none; padding: 1px; width: 35%;"
                                                            CssClass="popup table">
                                                            <table width="100%" class="table">
                                                                <tr>
                                                                    <th colspan="2" style="text-align: center;">Product Details
                                                                    </th>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left"><b>Invoice Number</b>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="LblDInvNo" runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="padding: 0;" colspan="2">
                                                                        <asp:Repeater ID="RptInvDtl" runat="server" OnItemDataBound="rptInvDtl_ItemDataBound">
                                                                            <HeaderTemplate>
                                                                                <table width="100%" class="table">
                                                                                    <tr>
                                                                                        <th>Product </th>
                                                                                        <th>Qty</th>
                                                                                        <th>Unit Price</th>
                                                                                        <th>Amount</th>
                                                                                    </tr>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Label ID="LblProdct" runat="server"></asp:Label></td>
                                                                                    <td>
                                                                                        <asp:Label ID="LblQty" runat="server"></asp:Label></td>
                                                                                    <td>
                                                                                        <asp:Label ID="LblUnitPrice" runat="server"></asp:Label></td>
                                                                                    <td>
                                                                                        <asp:Label ID="LblAmount" runat="server"></asp:Label></td>
                                                                                </tr>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                </table>
                                                                            </FooterTemplate>
                                                                        </asp:Repeater>
                                                                    </td>

                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" style="text-align: center;">
                                                                        <asp:Button ID="btnPopStatCancel" runat="server" Text="Close" CssClass="button"
                                                                            CausesValidation="false" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                        <asp:LinkButton ID="LnkCnvrt" runat="server" CausesValidation="false" Text="Convert to Tax Invoice"
                                                            CommandArgument='<%# DataBinder.Eval(Container.DataItem, "InvID") %>'></asp:LinkButton>

                                                        <ajax:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="LnkCnvrt"
                                                            PopupControlID="PnlCnvert" CancelControlID="btnPopStatCancel"
                                                            BackgroundCssClass="ModalBackground" />
                                                        <asp:Panel ID="PnlCnvert" runat="server" Style="display: none; padding: 1px; width: 35%;"
                                                            CssClass="popup table">
                                                            <table width="100%" class="table">
                                                                <tr>
                                                                    <th colspan="2" style="text-align: center;">Alert Message
                                                                    </th>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left"><b>Do you want to Edit or Convert the same?</b>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td colspan="2" style="text-align: center;">
                                                                        <asp:Button ID="BtnEdit" runat="server" Text="Edit Invoice" CommandName="EditInvoice" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "InvID") %>' CssClass="button"
                                                                            CausesValidation="false" />
                                                                        <asp:Button ID="BtnConvert" runat="server" Text="Convert Invoice" CommandName="ConvertInvoice" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "InvID") %>' CssClass="button"
                                                                            CausesValidation="false" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                    </td>

                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <tr>
                                                    <td colspan="12" style="text-align: center;">
                                                        <asp:LinkButton ID="LnkPreviousRecord" Text="<< Prev" runat="server" OnClick="NextProUserRecord" Style="top: 0px;"
                                                            CausesValidation="false" class="btnnxt" />&nbsp;
                                            <asp:DropDownList ID="DrpOnRecord" AutoPostBack="true" runat="server" class="pagedrp" Style="margin-top: 0px; width: 50px; float: none; display: inherit;" OnSelectedIndexChanged="OnUserRecordPro" />&nbsp;
                                         
                                            <asp:LinkButton ID="LnkNextRecord" Text="Next >>" runat="server" OnClick="NextProUserRecord" Style="top: 0px;"
                                                CausesValidation="false" class="btnnxt" />&nbsp;
                                                    </td>
                                                </tr>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>


                                </div>
                                <div id="tax" class="tab-pane fade">
                                    <div class="col-md-12 col-sm-12 col-xs-12 space1">
                                        <asp:Label ID="LblTaxDispUserCount" runat="server" CssClass="showtxt"></asp:Label>
                                        <asp:Label ID="LblTaxHideUserCount" runat="server" Visible="false" CssClass="showtxt"></asp:Label>
                                        <asp:Label ID="LblTaxNoUserCount" runat="server" CssClass="showtxt"></asp:Label>
                                        <asp:Label ID="lblTaxinfo" runat="server" CssClass="showtxt"></asp:Label>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 space">
                                        <div>
                                            <asp:Label ID="lblTaxLeads" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 space1">
                                         <asp:UpdatePanel runat="server" ID="updpanel">
                                                            <ContentTemplate>
                                        <asp:Repeater runat="server" ID="reptax">
                                            <HeaderTemplate>
                                                <table class="table">
                                                    <tr>
                                                        <th width="2%"></th>
                                                        <th width="8%">Invoice No
                                                        </th>
                                                        <th width="8%">Name
                                                        </th>
                                                        <th width="8%">Invoice Amount
                                                        </th>
                                                        <th width="7%">Invoice Date
                                                        </th>
                                                        <th width="15%"></th>
                                                    </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td align="center">
                                                        <asp:CheckBox runat="server" ID="ChkUserInfo" />
                                                        <asp:Label runat="server" ID="LblChk" Text='<%#Container.DataItem("InvID")%>' Visible="false"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="LblInvNo"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblName"></asp:Label>
                                                        <asp:HiddenField ID="HdnCustID" runat="server" Value='<%#Container.DataItem("CustomerId")%>' />
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="LblInvAmt"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="InvDate"></asp:Label>
                                                    </td>

                                                    <td>
                                                        <asp:Label runat="server" ID="LblRemark"></asp:Label>
                                                        <asp:LinkButton ID="LnkInvoice" runat="server" CausesValidation="false" Text="View Details"
                                                            CommandArgument='<%# DataBinder.Eval(Container.DataItem, "InvID") %>'></asp:LinkButton>&nbsp;|&nbsp;
                                                        <ajax:ModalPopupExtender ID="ModalPopStatus" runat="server" TargetControlID="LnkInvoice"
                                                            PopupControlID="PanelStatus" CancelControlID="btnPopStatCancel"
                                                            BackgroundCssClass="ModalBackground" />
                                                        <asp:Panel ID="PanelStatus" runat="server" Style="display: none; padding: 1px; width: 35%;"
                                                            CssClass="popup table">
                                                            <table width="100%" class="table">
                                                                <tr>
                                                                    <th colspan="2" style="text-align: center;">Product Details
                                                                    </th>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left"><b>Invoice Number</b>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="LblDInvNo" runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" style="padding: 0;">
                                                                        <asp:Repeater ID="RptInvDtl" runat="server" OnItemDataBound="rptInvDtl_ItemDataBound">
                                                                            <HeaderTemplate>
                                                                                <table width="100%" class="table">
                                                                                    <tr>
                                                                                        <th>Product </th>
                                                                                        <th>Qty</th>
                                                                                        <th>Unit Price</th>
                                                                                        <th>Amount</th>
                                                                                    </tr>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Label ID="LblProdct" runat="server"></asp:Label></td>
                                                                                    <td>
                                                                                        <asp:Label ID="LblQty" runat="server"></asp:Label></td>
                                                                                    <td>
                                                                                        <asp:Label ID="LblUnitPrice" runat="server"></asp:Label></td>
                                                                                    <td>
                                                                                        <asp:Label ID="LblAmount" runat="server"></asp:Label></td>
                                                                                </tr>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                </table>
                                                                            </FooterTemplate>
                                                                        </asp:Repeater>
                                                                    </td>

                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" style="text-align: center;">
                                                                        <asp:Button ID="btnPopStatCancel" runat="server" Text="Close" CssClass="button"
                                                                            CausesValidation="false" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                        <asp:LinkButton ID="LnkPaymentRecv" runat="server" CausesValidation="false" Text="Payment Received"
                                                            CommandArgument='<%# DataBinder.Eval(Container.DataItem, "InvID") %>'></asp:LinkButton>
                                                        <%--<ajax:ConfirmButtonExtender ID="CnfrmPay" runat="server" TargetControlID="LnkPaymentRecv" DisplayModalPopupID="MdlPay"></ajax:ConfirmButtonExtender>--%>
                                                       
                                                        <ajax:ModalPopupExtender ID="MdlPay" runat="server" TargetControlID="LnkPaymentRecv"
                                                            PopupControlID="PnlCnvert" CancelControlID="btnPopStatCancel"
                                                            BackgroundCssClass="ModalBackground" />
                                                        <asp:Panel ID="PnlCnvert" runat="server" Style="display: none; padding: 1px; width: 35%; top: 0px !important;"
                                                            CssClass="popup table">
                                                            <table width="100%" class="table">
                                                                <tr>
                                                                    <th colspan="2" style="text-align: center;">Payment Received
                                                                    </th>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <%--onclick='<%#"return getrdbchange()" %>'--%>
                                                                        <asp:RadioButtonList ID="RdbType" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal" onclick="return getrdbchange(this);">
                                                                            <asp:ListItem class="radio-inline" Text="Without TDS" Value="WO" ></asp:ListItem>
                                                                            <asp:ListItem class="radio-inline" Text="With TDS" Value="WT" ></asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                        <asp:HiddenField ID="hdntype" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="padding: 0;">
                                                                           <div id="dvwotds" runat="server" >
                                                                            <table width="100%" class="table">
                                                                                <tr>
                                                                                    <td style="padding: 18px 0px 0px 5px;">
                                                                                        <label>Amount :</label></td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="TxtAmtRecved" runat="server" CssClass="form-control" onkeyup="return amtcheck(this);"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TxtAmtRecved"
                                                                                            ErrorMessage="<br/>Enter Amount" SetFocusOnError="true" Display="Dynamic"
                                                                                            ValidationGroup="SavePay"></asp:RequiredFieldValidator>
                                                                                        <asp:HiddenField ID="hdnOutAmount" runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td width="28%" style="padding: 18px 0px 0px 5px;">
                                                                                        <label>Payment Mode :</label></td>
                                                                                    <td>
                                                                                        <asp:DropDownList ID="DrpPaymenyMode" runat="server" CssClass="form-control" onchange="return getpaymenyMode(this);">
                                                                                            <asp:ListItem Text="Select Mode" Value=""></asp:ListItem>
                                                                                            <asp:ListItem Text="Cash" Value="CA"></asp:ListItem>
                                                                                            <asp:ListItem Text="Cheque" Value="CH"></asp:ListItem>
                                                                                            <asp:ListItem Text="NEFT" Value="NE"></asp:ListItem>
                                                                                        </asp:DropDownList>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DrpPaymenyMode"
                                                                                            ErrorMessage="<br/>Select Mode !" InitialValue="" SetFocusOnError="true" Display="Dynamic"
                                                                                            ValidationGroup="SavePay"></asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="trCh" runat="server" style="display: none;">
                                                                                    <td style="padding: 18px 0px 0px 5px;">
                                                                                        <label>Cheque No :</label></td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="TxtChqNo" runat="server" CssClass="form-control"></asp:TextBox></td>

                                                                                </tr>
                                                                                <tr id="trNet" runat="server" style="display: none;">
                                                                                    <td style="padding: 18px 0px 0px 2px;">
                                                                                        <label>Transaction ID :</label></td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="TxtTransaction" runat="server" CssClass="form-control"></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="padding: 18px 0px 0px 5px;">
                                                                                        <label>Remark :</label></td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="TxtRemark" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox></td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                        <div id="dvwtds" runat="server" style="display: none;">
                                                                            <table width="100%" class="table">
                                                                                <tr>
                                                                                    <td style="padding: 18px 0px 0px 5px;">
                                                                                        <label>Tax :</label></td>
                                                                                    <td>
                                                                                        <asp:DropDownList ID="DrpTax" runat="server" CssClass="form-control" onchange="return gettdscal(this);">
                                                                                        </asp:DropDownList>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                                                                                                    
                                                                                       <td style="padding: 18px 0px 0px 5px;">
                                                                                        <label>Tax  :</label></td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="TxtTaxValue" runat="server" CssClass="form-control" onkeyup="return textchange(this);"></asp:TextBox></td>
                                                                                  
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="padding: 18px 0px 0px 5px;">
                                                                                        <label>Payable Amount :</label></td>
                                                                                    <td>
                                                                                        <asp:Label ID="LblPayableAmount" runat="server" CssClass="form-control" style="padding: 0px 10px;"></asp:Label></td>

                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="padding: 18px 0px 0px 2px;">
                                                                                        <label>Amount :</label></td>
                                                                                    <td>
                                                                                        <asp:Label ID="LblAmountWOTDS" runat="server" CssClass="form-control" style="padding: 0px 10px;"></asp:Label>
                                                                                         <asp:HiddenField ID="HdnAmountWOTDS" runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="padding: 18px 0px 0px 5px;">
                                                                                        <label>Remark :</label></td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="TxtWoRemark" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                                                                       
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" style="text-align: center;">
                                                                        <asp:Label ID="LblMsg" runat="server" Visible="false" CssClass="red"></asp:Label>
                                                                        <asp:Button ID="BtnSave" runat="server" Text="Save" CausesValidation="false" CommandName="SavePayment" CommandArgument='<%# Container.DataItem("InvID")%>' CssClass="button" ValidationGroup="SavePay"  />                                                                        
                                                                        <asp:Button ID="btntest" runat="server" Style="display: none" CommandName="SavePayment" CommandArgument='<%# Container.DataItem("InvID")%>' />
                                                                        <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="button"
                                                                            CausesValidation="false" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                            
                                                    </td>

                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <tr>
                                                    <td colspan="12" style="text-align: center;">
                                                        <asp:LinkButton ID="LnkPreviousRecord" Text="<< Prev" runat="server" OnClick="PreviousTaxUserRecord" Style="top: 0px;"
                                                            CausesValidation="false" class="btnnxt" />&nbsp;
                                            <asp:DropDownList ID="DrpOnRecord" AutoPostBack="true" runat="server" class="pagedrp" Style="margin-top: 0px; width: 50px; float: none; display: inherit;" OnSelectedIndexChanged="OnUserRecordTax" />&nbsp;
                                         
                                            <asp:LinkButton ID="LnkNextRecord" Text="Next >>" runat="server" OnClick="NextTaxUserRecord" Style="top: 0px;"
                                                CausesValidation="false" class="btnnxt" />&nbsp;
                                                    </td>
                                                </tr>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                    </div>


                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div>
                <UC1:UCFooter ID="UCFooter" runat="server" />
            </div>
        </div>
    </form>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap/bootstrap.min.js"></script>
    <script>
        function getrdbchange(id) {
            debugger
            // var id= ;
            // alert(id);
            //  alert(id.value);
            var val = id.value;
            var a = id.id;
            var b = a.split("_");
            $("#" + b[0] + "_" + b[1] + "_hdntype").val(val);
            var rb = document.getElementById(a);
            var radio = rb.getElementsByTagName("input");
            var label = rb.getElementsByTagName("label");
            for (var i = 0; i < radio.length; i++) {
                if (radio[i].checked) {
                    if (radio[i].value == "WT") {
                        //$("#" + b[0] + "_" + b[1] + "_dvwtds").css("display", "none");
                        $("#" + b[0] + "_" + b[1] + "_dvwtds").show();
                        $("#" + b[0] + "_" + b[1] + "_dvwotds").hide();
                    }
                    else if (radio[i].value == "WO") {
                        $("#" + b[0] + "_" + b[1] + "_dvwotds").show();
                        $("#" + b[0] + "_" + b[1] + "_dvwtds").hide();
                        //$("#" + b[0] + "_" + b[1] + "_dvwotds").Show;
                    }

                }
            }

         
           

        }


        function getpaymenyMode(id) {
            //debugger
            //var id= ;
            //alert(id);
            //alert(id.value);
            var val = id.value;
            var a = id.id;
            var b = a.split("_");
            if (val == "CH") {
                //$("#" + b[0] + "_" + b[1] + "_dvwtds").css("display", "none");
                $("#" + b[0] + "_" + b[1] + "_trCh").show();
                $("#" + b[0] + "_" + b[1] + "_trNet").hide();
            }
            else if (val == "NE") {
                $("#" + b[0] + "_" + b[1] + "_trCh").hide();
                $("#" + b[0] + "_" + b[1] + "_trNet").show();
                //$("#" + b[0] + "_" + b[1] + "_dvwotds").Show;

            }
            else if (val == "CA") {
                $("#" + b[0] + "_" + b[1] + "_trCh").hide();
                $("#" + b[0] + "_" + b[1] + "_trNet").hide();
            }

        }
        function test(id) {
            debugger
            var a = id.id;
            var b = a.split("_");

            $("#" + b[0] + "_" + b[1] + "_btntest").click();
            ////var rb = document.getElementById(a);
            ////var radio = rb.getElementsByTagName("input");
            ////var label = rb.getElementsByTagName("label");
            ////for (var i = 0; i < radio.length; i++) {
            ////    if (radio[i].checked) {
            ////        if (radio[i].value == "WO") {
            ////            $("#" + b[0] + "_" + b[1] + "_LblMsg").removeClass("hide");
            ////            $("#" + b[0] + "_" + b[1] + "_LblMsg").text("Please Enter Amount !");
            ////        }
            ////    }
            //if ($("#" + b[0] + "_" + b[1] + "_hdntype").val() == "WO") {
            //    if ($("#" + b[0] + "_" + b[1] + "_TxtAmtRecved").val() == "") {
            //        $("#" + b[0] + "_" + b[1] + "_LblMsg").removeClass("hide");
            //        $("#" + b[0] + "_" + b[1] + "_LblMsg").text("Please Enter Amount !");
            //    } else if ($("#" + b[0] + "_" + b[1] + "_DrpPaymenyMode").val() == "") {
            //        $("#" + b[0] + "_" + b[1] + "_LblMsg").removeClass("hide");
            //        $("#" + b[0] + "_" + b[1] + "_LblMsg").text("Please Select Payment Mode!");
            //    } else if ($("#" + b[0] + "_" + b[1] + "_DrpPaymenyMode").val() == "CH") {
            //        if ($("#" + b[0] + "_" + b[1] + "_TxtChqNo").val() == "") {
            //            $("#" + b[0] + "_" + b[1] + "_LblMsg").removeClass("hide");
            //            $("#" + b[0] + "_" + b[1] + "_LblMsg").text("Please Enter Cheque No!");
            //        } else {
            //            $("#" + b[0] + "_" + b[1] + "_btntest").click();
            //        }
            //    } else if ($("#" + b[0] + "_" + b[1] + "_DrpPaymenyMode").val() == "NE") {
            //        if ($("#" + b[0] + "_" + b[1] + "_TxtTransaction").val() == "") {
            //            $("#" + b[0] + "_" + b[1] + "_LblMsg").removeClass("hide");
            //            $("#" + b[0] + "_" + b[1] + "_LblMsg").text("Please Enter Transaction No!");
            //        } else {
            //            $("#" + b[0] + "_" + b[1] + "_btntest").click();
            //        }
            //    } else {
            //        $("#" + b[0] + "_" + b[1] + "_btntest").click();
            //    }


            //} else {
            //    $("#" + b[0] + "_" + b[1] + "_btntest").click();
            //}

        }

        function amtcheck(id) {
            var val1 = id.value;
            var taxval;
            var a = id.id;
            var b = a.split("_");
            if ($("#" + b[0] + "_" + b[1] + "_TxtAmtRecved").val() > $("#" + b[0] + "_" + b[1] + "_hdnOutAmount").val()) {
                $("#" + b[0] + "_" + b[1] + "_LblMsg").removeClass("hide");
                $("#" + b[0] + "_" + b[1] + "_LblMsg").text("Amount should be less or equal to Invoice Amount !");
            }
        }

        function gettdscal(id) {
            //debugger
            var val1 = id.value;
            var taxval;
            var a = id.id;
            var b = a.split("_");

            $("#" + b[0] + "_" + b[1] + "_TxtTaxValue").val(val1);
            if ($("#" + b[0] + "_" + b[1] + "_TxtTaxValue").val() != "") {
                taxval = Number($("#" + b[0] + "_" + b[1] + "_LblPayableAmount").text()) * Number($("#" + b[0] + "_" + b[1] + "_TxtTaxValue").val()) / 100;
                $("#" + b[0] + "_" + b[1] + "_LblAmountWOTDS").text(taxval);
                $("#" + b[0] + "_" + b[1] + "_HdnAmountWOTDS").val(taxval);
            }
        }
        function textchange(id) {
            var val1 = id.value;
            var taxval;
            var a = id.id;
            var b = a.split("_");
            if ($("#" + b[0] + "_" + b[1] + "_TxtTaxValue").val() != "") {
                taxval = Number($("#" + b[0] + "_" + b[1] + "_LblPayableAmount").text()) * Number($("#" + b[0] + "_" + b[1] + "_TxtTaxValue").val()) / 100;
                $("#" + b[0] + "_" + b[1] + "_LblAmountWOTDS").text(taxval);
                $("#" + b[0] + "_" + b[1] + "_HdnAmountWOTDS").val(taxval);
            }
        }
    </script>

</body>
</html>
