﻿Imports System.Data
Imports System.IO
Partial Class EditInvoice
    Inherits System.Web.UI.Page
    Dim obj As New Engine
    Dim Total As Double = 0
    Private Sub page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Session("CompanyId") = Nothing Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            CType(UCHeader.FindControl("LblHeading"), Label).Text = "Create Invoice"
            TxtServiceValidity.Attributes.Add("readonly", "readonly")
            Dim Dttl As New DataTable
            Dttl.Columns.AddRange(New DataColumn(3) {New DataColumn("Product"), New DataColumn("Qty"), New DataColumn("UnitPrice"), New DataColumn("Amount")})
            ViewState("Desc") = Dttl
            BindDrp()
            If Not IsNothing(Request.QueryString("InvID")) Then
                LblMode.Text = "Edit"
                BindEdit()
            End If

        End If
    End Sub
    Private Sub BindEdit()
        Dim dt As New DataTable
        dt = obj.GetDataTable("Select * From InvoiceDetails Id left join InvoiceMaster Im on Id.InvID=Im.InvID where Id.InvID=" & Request.QueryString("InvID"))

        If dt.Rows.Count > 0 Then
            DrpType.SelectedValue = dt(0)("InvoiceType")
            TxtServiceValidity.Text = obj.GetFormattedDate(dt(0)("ValidDate"))
            Dim dtdtl As New DataTable
            dtdtl = ViewState("Desc")
            dtdtl.Rows.Clear()
            For i As Integer = 0 To dt.Rows.Count - 1
                dtdtl.Rows.Add(dt(i)("Product"), dt(i)("Qty"), dt(i)("UnitPrice"), dt(i)("TotalAmount"))
            Next

            ViewState("Desc") = dtdtl

            RptInvoice.DataSource = dtdtl
            RptInvoice.DataBind()

        End If
        LblTotal.Text = Total
        HdnAmt.Value = Total
        BindNetvalue()

    End Sub

    Public Sub BindDrp()
        Dim sql As String = ""
        Dim dt As New DataTable

        sql = "select CompanyName from CustomerMaster where CustomerId=" & Session("CustId").ToString
        dt = obj.GetDataTable(sql)
        If Not IsNothing(dt) Then
            If dt.Rows.Count > 0 Then
                LblCompanyName.Text = dt(0)("CompanyName")
            End If
        End If
        Dim dthead As New DataTable
        dthead = obj.GetDataTable("Select Address,ContactNumber From InvoiceHeader where CompanyID=" & Session("CompanyId"))
        If dthead.Rows.Count > 0 Then
            LblInvHead.Text = dthead(0)("Address") & "</br>" & dthead(0)("ContactNumber")
        End If

        Dim dtfoot As New DataTable
        dtfoot = obj.GetDataTable("Select Body From InvoiceFooter where CompanyID=" & Session("CompanyId"))
        If dtfoot.Rows.Count > 0 Then
            LblFooter.Text = dtfoot(0)("Body")
        End If

        Dim dtBillingdtl As New DataTable
        dtBillingdtl = obj.GetDataTable("Select * From BillingDetails where CustomerID=" & Request.QueryString("ID"))

        If Not IsNothing(dtBillingdtl) Then
            If dtBillingdtl.Rows.Count > 0 Then
                LblCCompanyName.Text = dtBillingdtl(0)("CompanyName")
                LblCAddress.Text = dtBillingdtl(0)("BillingAddress")
                LblCContactPerson.Text = dtBillingdtl(0)("ContactPerson")
                LblCGSTIN.Text = dtBillingdtl(0)("GSTINNumber")
            End If
        End If

        Dim Dttax As New DataTable
        Dttax = obj.GetDataTable("Select * from TaxMaster where Type='I' and CompanyId=" & Session("CompanyId") & " Order By TaxID desc")

        If Not IsNothing(Dttax) Then
            If Dttax.Rows.Count > 0 Then
                RptTax.DataSource = Dttax
                RptTax.DataBind()
            End If
        End If

        'obj.BindListControl(Drptax, "Select * from TaxMaster where CompanyId=" & Session("CompanyId") & " Order By TaxID desc", "Name", "TaxID")
        'Drptax.Items.Insert(0, New ListItem("Select Tax", "0"))
    End Sub

    Private Sub LnkAdd_Click(sender As Object, e As EventArgs) Handles LnkAdd.Click
        Dim Dt As New DataTable
        Dt = ViewState("Desc")

        Dt.Rows.Add(TxtProduct.Text, TxtQty.Text, TxtUnitPrice.Text, HdnAmt.Value)

        TxtProduct.Text = ""
        TxtQty.Text = ""
        TxtUnitPrice.Text = ""
        LblAmt.Text = ""

        ViewState("Desc") = Dt

        RptInvoice.DataSource = Dt
        RptInvoice.DataBind()

        LblTotal.Text = Total
        HdnAmt.Value = Total
        BindTaxValue()
        BindNetvalue()
    End Sub

    Private Sub RptInvoice_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles RptInvoice.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            If Not IsDBNull(e.Item.DataItem("Product")) Then CType(e.Item.FindControl("LblProduct"), Label).Text = e.Item.DataItem("Product")
            If Not IsDBNull(e.Item.DataItem("Qty")) Then CType(e.Item.FindControl("LblQty"), Label).Text = e.Item.DataItem("Qty")
            If Not IsDBNull(e.Item.DataItem("UnitPrice")) Then CType(e.Item.FindControl("LblUnitPrice"), Label).Text = e.Item.DataItem("UnitPrice")
            If Not IsDBNull(e.Item.DataItem("Amount")) Then CType(e.Item.FindControl("LblAmt"), Label).Text = e.Item.DataItem("Amount")
            'If Not IsDBNull(e.Item.DataItem("Tax")) Then CType(e.Item.FindControl("LblTax"), Label).Text = e.Item.DataItem("Tax")

            Total += e.Item.DataItem("Amount")
        End If
    End Sub
    Public Sub RptTax_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        If e.Item.ItemIndex >= 0 Then
            If Not IsDBNull(e.Item.DataItem("Name")) Then CType(e.Item.FindControl("LblTax"), Label).Text = "<b>" & e.Item.DataItem("Name") & " (" & e.Item.DataItem("Amount") & "%)</b>"
            If Not IsDBNull(e.Item.DataItem("Amount")) Then CType(e.Item.FindControl("LblTaxPer"), Label).Text = e.Item.DataItem("Amount")
            Dim dt As New DataTable
            dt = obj.GetDataTable("Select * from InvoiceTaxDetails where InvID=" & Request.QueryString("InvID") & " and TaxID=" & e.Item.DataItem("TaxID"))
            If dt.Rows.Count > 0 Then
                CType(e.Item.FindControl("ChkUserInfo"), CheckBox).Checked = True
                CType(e.Item.FindControl("TxtTotal"), TextBox).Text = dt(0)("TaxAmount")
                CType(e.Item.FindControl("HdnTotal"), HiddenField).Value = dt(0)("TaxAmount")
            End If
        End If
    End Sub

    Private Sub RptInvoice_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles RptInvoice.ItemCommand
        If e.CommandName = "Remove" Then
            Dim Dt As New DataTable
            Dt = ViewState("Desc")

            Dt.Rows(e.Item.ItemIndex).Delete()

            ViewState("Desc") = Dt

            RptInvoice.DataSource = Dt
            RptInvoice.DataBind()

            LblTotal.Text = Total
            HdnAmt.Value = Total
            BindTaxValue()
            BindNetvalue()
        End If
    End Sub

    Private Sub TxtUnitPrice_TextChanged(sender As Object, e As EventArgs) Handles TxtUnitPrice.TextChanged
        If TxtQty.Text <> "" And TxtUnitPrice.Text <> "" Then
            LblAmt.Text = Val(TxtQty.Text) * Val(TxtUnitPrice.Text)
        End If
    End Sub

    Private Sub TxtQty_TextChanged(sender As Object, e As EventArgs) Handles TxtQty.TextChanged
        If TxtQty.Text <> "" And TxtUnitPrice.Text <> "" Then
            LblAmt.Text = Val(TxtQty.Text) * Val(TxtUnitPrice.Text)
        End If
    End Sub
    Private Sub BindTaxValue()
        Dim Amount As Double = 0.0
        Dim RptItm As RepeaterItem
        For Each RptItm In RptTax.Items
            If CType(RptItm.FindControl("ChkUserInfo"), CheckBox).Checked = True Then
                Amount = Val(LblTotal.Text) * Val(CType(RptItm.FindControl("LblTaxPer"), Label).Text) / 100
                'If CType(RptItm.FindControl("LblTotal"), Label).Text <> "0" Then
                CType(RptItm.FindControl("TxtTotal"), TextBox).Text = Amount
                CType(RptItm.FindControl("HdnTotal"), HiddenField).Value = Amount
                'End If
            End If
        Next
    End Sub
    Private Sub BindNetvalue()
        Dim netTotal As Double = 0.0
        Dim RptItm As RepeaterItem
        For Each RptItm In RptTax.Items
            If CType(RptItm.FindControl("HdnTotal"), HiddenField).Value <> "" Then
                'If CType(RptItm.FindControl("LblTotal"), Label).Text <> "0" Then
                CType(RptItm.FindControl("TxtTotal"), TextBox).Text = CType(RptItm.FindControl("HdnTotal"), HiddenField).Value
                netTotal += CType(RptItm.FindControl("HdnTotal"), HiddenField).Value
                'End If
            End If
        Next
        LblNetTotal.Text = Val(LblTotal.Text) + netTotal
    End Sub
    Private Sub Btnclick_Click(sender As Object, e As EventArgs) Handles Btnclick.Click
        BindNetvalue()
    End Sub
    Private Function ValidateEnter() As String
        Dim StrError As String = ""
        Dim StrWhere As String = ""
        Dim cnt As Integer = 0
        Dim Dt As New DataTable
        Dt = ViewState("Desc")

        If IsNothing(Dt) Then
            StrError = "Enter Product Details !"
        Else
            If Dt.Rows.Count = 0 Then
                StrError = "Enter Product Details !"
            End If
        End If


        BtnInvCancel.Visible = True

        Return StrError
    End Function
    Private Sub BtnSaveInv_Click(sender As Object, e As EventArgs) Handles BtnSaveInv.Click
        Dim rs As New RecorsetUpdate

        Dim vals As String = ValidateEnter()
        If vals = "" Then
            Dim InvNo As String = ""
            Dim InvNoPrefix As String = ""
            Dim Cnt As Integer = 0
            Cnt = obj.GetField("Select Count(*) As Cnt From InvoiceMaster where CompanyID=" & Session("CompanyId"), "Cnt")
            InvNoPrefix = obj.GetField("Select isnull(Max(InvoicePrefix),0) As InvoicePrefix From InvoiceNoMaster where CompanyID=" & Session("CompanyId") & " And CONVERT(Varchar(20),StartDate,106)<=CONVERT(Varchar(20),'" & obj.GetFormattedDate(Date.Now) & "',106) or CONVERT(Varchar(20),EndDate,106)<=CONVERT(Varchar(20),'" & obj.GetFormattedDate(Date.Now) & "',106)", "InvoicePrefix")
            If DrpType.SelectedValue <> "P" Then
                If Cnt = 0 Then
                    InvNo = obj.GetField("Select InvoiceDigit From InvoiceNoMaster where CompanyID=" & Session("CompanyId") & " and CONVERT(Varchar(20),StartDate,106)<=CONVERT(Varchar(20),'" & obj.GetFormattedDate(Date.Now) & "',106) or CONVERT(Varchar(20),EndDate,106)<=CONVERT(Varchar(20),'" & obj.GetFormattedDate(Date.Now) & "',106)", "InvoiceDigit")
                Else
                    InvNo = obj.GetField("Select Max(IIf(InvoiceNodigit=0,0,InvoiceNodigit)) As InvoiceNo From InvoiceMaster where CompanyID=" & Session("CompanyId") & " And  CustomerID=" & Request.QueryString("ID"), "InvoiceNo")
                    If InvNo = "0" Then
                        InvNo = obj.GetField("Select InvoiceDigit From InvoiceNoMaster where CompanyID=" & Session("CompanyId") & " and CONVERT(Varchar(20),StartDate,106)<=CONVERT(Varchar(20),'" & obj.GetFormattedDate(Date.Now) & "',106) or CONVERT(Varchar(20),EndDate,106)<=CONVERT(Varchar(20),'" & obj.GetFormattedDate(Date.Now) & "',106)", "InvoiceDigit")
                    Else
                        InvNo = Val(InvNo) + 1
                    End If
                End If
            End If

            rs.TableParameter("InvoiceMaster", "Edit", "InvID=" & Request.QueryString("InvID"))
            rs.SetField("CustomerID", Request.QueryString("Id"), "Number")
            rs.SetField("CompanyID", Session("CompanyId"), "Number")
            rs.SetField("InvoiceType", DrpType.SelectedValue, "String")
            If DrpType.SelectedValue <> "P" Then
                rs.SetField("InvoiceNo", InvNoPrefix & InvNo, "String")
                rs.SetField("InvoiceNodigit", InvNo, "Number")
            End If
            rs.SetField("InvAmount", HdnAmt.Value, "Number")
            rs.SetField("InvNetAmount", LblNetTotal.Text, "Number")
            rs.SetField("ValidDate", obj.GetFormattedDate(TxtServiceValidity.Text), "String")
            rs.Execute()

            If LblMode.Text <> "Edit" Then
                Dim InvID As String = ""
                InvID = obj.GetField("Select ISNULL(MAX(InvID),0) As InvID from InvoiceMaster where  CompanyId=" & Session("CompanyId") & " And  CustomerID=" & Request.QueryString("ID"), "InvID")

            End If

            Dim Dt As New DataTable
            Dt = ViewState("Desc")
            If LblMode.Text = "Edit" Then
                obj.ExecuteCommand("Delete From InvoiceDetails where InvID=" & Request.QueryString("InvID"))
            End If
            If Not IsNothing(Dt) Then
                If Dt.Rows.Count > 0 Then
                    For i As Integer = 0 To Dt.Rows.Count - 1
                        rs.TableParameter("InvoiceDetails", "Add")
                        rs.SetField("InvID", Request.QueryString("InvID"), "Number")
                        rs.SetField("Product", Dt(i)("Product"), "String")
                        rs.SetField("Qty", Dt(i)("Qty"), "Number")
                        rs.SetField("UnitPrice", Dt(i)("UnitPrice"), "Number")
                        rs.SetField("TotalAmount", Dt(i)("Amount"), "Number")
                        rs.Execute()
                    Next
                End If
            End If
            If LblMode.Text = "Edit" Then
                obj.ExecuteCommand("Delete From InvoiceTaxDetails where InvID=" & Request.QueryString("InvID"))
            End If
            For i As Integer = 0 To RptTax.Items.Count - 1
                rs.TableParameter("InvoiceTaxDetails", "Add")
                If CType(RptTax.Items(i).FindControl("ChkUserInfo"), CheckBox).Checked = True Then
                    rs.SetField("InvID", Request.QueryString("InvID"), "Number")
                    rs.SetField("TaxID", CType(RptTax.Items(i).FindControl("LblChk"), Label).Text, "Number")
                    rs.SetField("TaxValue", CType(RptTax.Items(i).FindControl("LblTaxPer"), Label).Text, "Number")
                    rs.SetField("TaxAmount", CType(RptTax.Items(i).FindControl("HdnTotal"), HiddenField).Value, "Number")
                    rs.Execute()
                End If
            Next
            'Drptax.ClearSelection()
            TxtServiceValidity.Text = ""
            DrpType.ClearSelection()
            'LblTax.Text = ""
            LblTotal.Text = ""

            Dt = ViewState("Desc")

            Dt.Rows.Clear()
            ViewState("Desc") = Dt

            RptInvoice.DataSource = Dt
            RptInvoice.DataBind()

            Dim Dttax As New DataTable
            Dttax = obj.GetDataTable("Select * from TaxMaster where Type='I' and CompanyId=" & Session("CompanyId") & " Order By TaxID desc")

            If Not IsNothing(Dttax) Then
                If Dttax.Rows.Count > 0 Then
                    RptTax.DataSource = Dttax
                    RptTax.DataBind()
                End If
            End If

            Response.Redirect("InvoiceList.aspx")
        End If
    End Sub

    Private Sub BtnInvCancel_Click(sender As Object, e As EventArgs) Handles BtnInvCancel.Click
        'Drptax.ClearSelection()
        TxtServiceValidity.Text = ""

        Dim dt As New DataTable
        dt = ViewState("Desc")

        dt.Rows.Clear()
        ViewState("Desc") = dt

        RptInvoice.DataSource = dt
        RptInvoice.DataBind()
    End Sub

    Private Sub DrpType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DrpType.SelectedIndexChanged
        If DrpType.SelectedValue = "T" Then
            Dim InvNo As String = ""
            Dim InvNoPrefix As String = ""
            Dim Cnt As Integer = 0
            Cnt = obj.GetField("Select Count(*) As Cnt From InvoiceMaster where CompanyID=" & Session("CompanyId"), "Cnt")
            InvNoPrefix = obj.GetField("Select isnull(Max(InvoicePrefix),0) As InvoicePrefix From InvoiceNoMaster where CompanyID=" & Session("CompanyId") & " And CONVERT(Varchar(20),StartDate,106)<=CONVERT(Varchar(20),'" & obj.GetFormattedDate(Date.Now) & "',106) or CONVERT(Varchar(20),EndDate,106)<=CONVERT(Varchar(20),'" & obj.GetFormattedDate(Date.Now) & "',106)", "InvoicePrefix")
            'If DrpType.SelectedValue <> "P" Then
            If Cnt = 0 Then
                InvNo = obj.GetField("Select InvoiceDigit From InvoiceNoMaster where CompanyID=" & Session("CompanyId") & " and CONVERT(Varchar(20),StartDate,106)<=CONVERT(Varchar(20),'" & obj.GetFormattedDate(Date.Now) & "',106) or CONVERT(Varchar(20),EndDate,106)<=CONVERT(Varchar(20),'" & obj.GetFormattedDate(Date.Now) & "',106)", "InvoiceDigit")
            Else
                InvNo = obj.GetField("Select Max(IIf(InvoiceNodigit=0,0,InvoiceNodigit)) As InvoiceNo From InvoiceMaster where CompanyID=" & Session("CompanyId") & " And  CustomerID=" & Request.QueryString("ID"), "InvoiceNo")
                If InvNo = "0" Then
                    InvNo = obj.GetField("Select InvoiceDigit From InvoiceNoMaster where CompanyID=" & Session("CompanyId") & " and CONVERT(Varchar(20),StartDate,106)<=CONVERT(Varchar(20),'" & obj.GetFormattedDate(Date.Now) & "',106) or CONVERT(Varchar(20),EndDate,106)<=CONVERT(Varchar(20),'" & obj.GetFormattedDate(Date.Now) & "',106)", "InvoiceDigit")
                Else
                    InvNo = Val(InvNo) + 1
                End If
            End If
            'End If
            LblInvNo.Text = InvNoPrefix & InvNo
        End If
    End Sub
End Class
