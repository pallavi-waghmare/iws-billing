﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Dashboard.aspx.vb" Inherits="Dashboard" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Src="~/Header.ascx" TagName="UCHeader" TagPrefix="UC1" %>
<%@ Register Src="~/footer.ascx" TagName="UCFooter" TagPrefix="UC1" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Dashboard-Cusrela</title>
    <link rel="icon" href="Images/thumb.png" />
    <link href="Css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="Css/css.css" rel="stylesheet" type="text/css" />
    <link href="Css/Style.css" rel="Stylesheet" type="text/css" />
    <link href="ListBox/css/bootstrap-multiselect.css" rel="stylesheet" />
    <link href="ListBox/css/bootstrap.min.css" rel="stylesheet" />


</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div>
                <UC1:UCHeader ID="UCHeader" runat="server" />
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="boxshadow">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="title">
                                    Search Parameter
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 padding">
                                <div class="col-md-2 col-sm-2 col-xs-12" id="DivLstitem" runat="server">
                                    <div class="form-group">
                                        <label for="email">
                                            Team Member 
                                        </label>
                                        <%--<asp:DropDownList ID="LstTeam" runat="server" class="form-control">
                                    </asp:DropDownList>--%>
                                        <asp:ListBox ID="LstTeam" runat="server" Width="180px" CssClass="dummy" SelectionMode="Multiple" AutoPostBack="true"></asp:ListBox>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="email">
                                            Product
                                        </label>
                                        <asp:DropDownList ID="drpPro" runat="server" class="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="email">
                                            From
                                        </label>
                                        <asp:TextBox ID="TxtFrom" runat="server" class="form-control"></asp:TextBox>
                                        <ajax:CalendarExtender ID="calFrm" runat="server" TargetControlID="TxtFrom" Format="dd-MMM-yyyy"
                                            CssClass="cal">
                                        </ajax:CalendarExtender>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="email">
                                            To
                                        </label>
                                        <asp:TextBox ID="TxtTo" runat="server" class="form-control"></asp:TextBox>
                                        <ajax:CalendarExtender ID="calTo" runat="server" TargetControlID="TxtTo" Format="dd-MMM-yyyy"
                                            CssClass="cal">
                                        </ajax:CalendarExtender>
                                    </div>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-12">
                                    <div class="form-group">
                                        <asp:Button runat="server" ID="btnSearch" Text="Search" CssClass="button1" CausesValidation="false" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 space1">
                                <div class="title">
                                    Report by Product
                                </div>
                                <div>
                                    <asp:Label ID="lblLeads" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 space1" id="TblDailyReport" runat="server" visible="false">
                                <div class="title">
                                    Team Followup Analysis
                                </div>
                                <div>
                                    <asp:Label ID="lblTeamDailyReport" runat="server" />
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 space1" id="TblTeam" runat="server" visible="false">
                                <div class="title">
                                    Team Wise Lead Analysis
                                </div>
                                <div>
                                    <asp:Label ID="lblTeamsDetails" runat="server" />
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 space1 padding">
                                <div class="col-md-2 col-sm-10 col-xs-12">
                                    <div class="title">
                                        Todays Alerts
                                    </div>
                                </div>
                                <div class="col-md-10 col-sm-2 col-xs-12">
                                    <div class="viewbtn">
                                        <a id="lnkView" runat="server" href="Alert.aspx">View All Alerts</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div>
                                    <asp:Label ID="lblTodayAlert" runat="server"></asp:Label>
                                    <asp:Label ID="lblUrl" runat="server" Visible="false"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <UC1:UCFooter ID="UCFooter" runat="server" />
            </div>
        </div>
    </form>
    <script src="ListBox/Js/jquery-1.9.1.min.js"></script>
    <script src="ListBox/Js/bootstrap-multiselect.js"></script>
    <script src="ListBox/Js/bootstrap.min.js"></script>

    <script type="text/javascript">
        $(function getlistbox() {
            $('.dummy').multiselect({
                includeSelectAllOption: true
            });

        });

    </script>


</body>
</html>
