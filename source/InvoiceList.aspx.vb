﻿Imports System.Data
Imports System.IO
Imports AjaxControlToolkit
Partial Class InvoiceList
    Inherits System.Web.UI.Page
    Dim obj As New Engine
    Private Sub page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Session("CompanyId") = Nothing Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            CType(UCHeader.FindControl("LblHeading"), Label).Text = "Invoice List"
            'TxtFrom.Text = obj.GetFormattedDate(Date.Today.AddMonths(-1))
            'TxtTo.Text = obj.GetFormattedDate(Date.Today)

            TxtFrom.Attributes.Add("Readonly", "Readonly")
            TxtTo.Attributes.Add("Readonly", "Readonly")

            BindDrp()
            Bind()
        End If
    End Sub

    Public Sub BindDrp()
        Dim sql As String = ""
        sql &= "select CustomerId,CompanyName from CustomerMaster Where CustomerId=" & Session("CustId") & ";"
        'sql &= "select ProductId,ProductName from ProductMaster;"
        'sql &= "select * from MediumMaster;"
        Dim ds As New DataSet
        ds = obj.GetDataset(sql)

        obj.BindListControl1(drpSerCust, ds.Tables(0), "CompanyName", "CustomerId")
        'obj.BindListControl1(drpSerPro, ds.Tables(1), "ProductName", "ProductId")

        'drpSerCust.Items.Insert(0, New ListItem("All Customer", ""))
        drpSerPro.Items.Insert(0, New ListItem("All Product", ""))
        If drpSerCust.SelectedValue <> "" Then
            bindPro(drpSerCust.SelectedValue)
        End If

    End Sub
    Public Sub bindPro(ByVal id As Integer)
        obj.BindListControl(drpSerPro, "select ProductId,ProductName from ProductMaster where CustomerId=" & id, "ProductName", "ProductId")
    End Sub
    Public Sub Bind()
        Dim sql = "", sqltax = "", strWhere As String = ""

        If Not IsNothing(Request.QueryString("CustID")) Then
            strWhere &= " and LM.CustomerId=" & Request.QueryString("CustID")
        End If

        If drpSerCust.SelectedValue <> "" Then
            strWhere &= " and CM.CustomerId=" & drpSerCust.SelectedValue
        End If

        If TxtFrom.Text <> "" Then
            strWhere &= " and LM.InvDate>='" & TxtFrom.Text.Replace("'", "''") & "'"
        End If

        If TxtTo.Text <> "" Then
            strWhere &= " and LM.InvDate<=DATEADD(day,1,'" & TxtTo.Text.Replace("'", "''") & "')"
        End If

        sql &= "select Cu.Name as CustomerName,Lm.* from InvoiceMaster LM "
        sql &= "left join UserCustomerMaster Cu on LM.CustomerId=Cu.CustomerId "
        sql &= "left join CustomerMaster CM on CM.CustomerId=LM.CompanyID "
        sql &= " where 1=1 and InvoiceType='P' and CM.CustomerId= " & Session("CompanyId") & strWhere & " order by LM.InvID desc"

        sqltax &= "select Cu.Name as CustomerName,Lm.* from InvoiceMaster LM "
        sqltax &= "left join UserCustomerMaster Cu on LM.CustomerId=Cu.CustomerId "
        sqltax &= "left join CustomerMaster CM on CM.CustomerId=LM.CompanyID "
        sqltax &= " where 1=1 and InvoiceType='T' and CM.CustomerId= " & Session("CompanyId") & strWhere & " order by LM.InvID desc"

        Dim dt As DataTable = obj.GetDataTable(sql)
        Dim StrString As String = ""
        lblProLeads.Text = StrString

        Dim IntRecourtCount As Integer
        IntRecourtCount = obj.BindGridControl(repproforma, sql, LblProHideUserCount, LblProDispUserCount, LblProNoUserCount, "Proforma Invoice", "LnkPreviousRecord", "LnkNextRecord", "DrpOnRecord", 50)
        IntRecourtCount = obj.BindGridControl(reptax, sqltax, LblTaxHideUserCount, LblTaxDispUserCount, LblTaxNoUserCount, "Tax Invoice", "LnkPreviousRecord", "LnkNextRecord", "DrpOnRecord", 50)
        'If IntRecourtCount > 0 Then
        '    BtnDelete.Visible = True
        'Else
        '    BtnDelete.Visible = False
        'End If

    End Sub

    Public Sub PreviousProUserRecord(ByVal sender As Object, ByVal e As System.EventArgs)
        LblProHideUserCount.Text = CInt(LblProHideUserCount.Text) - 1
        obj.PreviousRecord()
        Bind()
    End Sub

    Public Sub NextProUserRecord(ByVal sender As Object, ByVal e As System.EventArgs)
        LblProHideUserCount.Text = CInt(LblProHideUserCount.Text) + 1
        obj.NextRecord()
        Bind()
    End Sub

    Protected Sub OnUserRecordPro(ByVal sender As Object, ByVal e As System.EventArgs)
        LblProHideUserCount.Text = CType(repproforma.Controls(repproforma.Controls.Count - 1).FindControl("DrpOnRecord"), DropDownList).SelectedValue
        obj.OnRecord(LblProHideUserCount.Text)
        Bind()
    End Sub

    Public Sub PreviousTaxUserRecord(ByVal sender As Object, ByVal e As System.EventArgs)
        LblTaxHideUserCount.Text = CInt(LblTaxHideUserCount.Text) - 1
        obj.PreviousRecord()
        Bind()
    End Sub

    Public Sub NextTaxUserRecord(ByVal sender As Object, ByVal e As System.EventArgs)
        LblTaxHideUserCount.Text = CInt(LblTaxHideUserCount.Text) + 1
        obj.NextRecord()
        Bind()
    End Sub

    Protected Sub OnUserRecordTax(ByVal sender As Object, ByVal e As System.EventArgs)
        LblTaxHideUserCount.Text = CType(reptax.Controls(reptax.Controls.Count - 1).FindControl("DrpOnRecord"), DropDownList).SelectedValue
        obj.OnRecord(LblTaxHideUserCount.Text)
        Bind()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Bind()
    End Sub

    Private Sub repproforma_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) Handles repproforma.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            If Not IsDBNull(e.Item.DataItem("InvoiceNo")) Then
                If e.Item.DataItem("InvoiceNo") <> "" Then
                    CType(e.Item.FindControl("LblInvNo"), Label).Text = e.Item.DataItem("InvoiceNo")
                Else
                    CType(e.Item.FindControl("LblInvNo"), Label).Text = "XXXXXXXXXXXX"
                End If
            End If
            If Not IsDBNull(e.Item.DataItem("InvoiceNo")) Then
                If e.Item.DataItem("InvoiceNo") <> "" Then
                    CType(e.Item.FindControl("LblDInvNo"), Label).Text = e.Item.DataItem("InvoiceNo")
                Else
                    CType(e.Item.FindControl("LblDInvNo"), Label).Text = "XXXXXXXXXXXX"
                End If
            End If
            If Not IsDBNull(e.Item.DataItem("CustomerName")) Then CType(e.Item.FindControl("LblName"), Label).Text = e.Item.DataItem("CustomerName")
            If Not IsDBNull(e.Item.DataItem("InvAmount")) Then CType(e.Item.FindControl("LblInvAmt"), Label).Text = e.Item.DataItem("InvAmount")
            If Not IsDBNull(e.Item.DataItem("InvDate")) Then CType(e.Item.FindControl("InvDate"), Label).Text = obj.GetFormattedDate(e.Item.DataItem("InvDate"))

            Dim Dt As New DataTable
            Dt = obj.GetDataTable("Select * From InvoiceDetails where InvID=" & e.Item.DataItem("InvID"))

            If Not IsNothing("Dt") Then
                If Dt.Rows.Count > 0 Then
                    CType(e.Item.FindControl("RptInvDtl"), Repeater).DataSource = Dt
                    CType(e.Item.FindControl("RptInvDtl"), Repeater).DataBind()
                End If
            End If
        End If
    End Sub

    Private Sub repproforma_ItemCommand(ByVal source As Object, ByVal e As RepeaterCommandEventArgs) Handles repproforma.ItemCommand
        If e.CommandName = "ConvertInvoice" Then
            Dim InvNo As String = ""
            Dim InvNoPrefix As String = ""
            Dim Cnt As Integer = 0
            Cnt = obj.GetField("Select Count(*) As Cnt From InvoiceMaster where CompanyID=" & Session("CompanyId"), "Cnt")
            InvNoPrefix = obj.GetField("Select isnull(Max(InvoicePrefix),0) As InvoicePrefix From InvoiceNoMaster where CompanyID=" & Session("CompanyId") & " And CONVERT(Varchar(20),StartDate,106)<=CONVERT(Varchar(20),'" & obj.GetFormattedDate(Date.Now) & "',106) or CONVERT(Varchar(20),EndDate,106)<=CONVERT(Varchar(20),'" & obj.GetFormattedDate(Date.Now) & "',106)", "InvoicePrefix")
            'If DrpType.SelectedValue <> "P" Then
            If Cnt = 0 Then
                InvNo = obj.GetField("Select InvoiceDigit From InvoiceNoMaster where CompanyID=" & Session("CompanyId") & " and CONVERT(Varchar(20),StartDate,106)<=CONVERT(Varchar(20),'" & obj.GetFormattedDate(Date.Now) & "',106) or CONVERT(Varchar(20),EndDate,106)<=CONVERT(Varchar(20),'" & obj.GetFormattedDate(Date.Now) & "',106)", "InvoiceDigit")
            Else
                InvNo = obj.GetField("Select Max(IIf(InvoiceNodigit=0,0,InvoiceNodigit)) As InvoiceNo From InvoiceMaster where CompanyID=" & Session("CompanyId") & " And  CustomerID=" & CType(e.Item.FindControl("HdnCustID"), HiddenField).Value, "InvoiceNo")
                If InvNo = "0" Then
                    InvNo = obj.GetField("Select InvoiceDigit From InvoiceNoMaster where CompanyID=" & Session("CompanyId") & " and CONVERT(Varchar(20),StartDate,106)<=CONVERT(Varchar(20),'" & obj.GetFormattedDate(Date.Now) & "',106) or CONVERT(Varchar(20),EndDate,106)<=CONVERT(Varchar(20),'" & obj.GetFormattedDate(Date.Now) & "',106)", "InvoiceDigit")
                Else
                    InvNo = Val(InvNo) + 1
                End If
            End If
            'End If
            obj.ExecuteCommand("Update InvoiceMaster Set InvoiceNo='" & InvNoPrefix & InvNo & "',InvoiceNodigit=" & InvNo & ",InvoiceType='T',ModifyDate='" & obj.GetFormattedDate(Date.Now) & "' where InvID=" & e.CommandArgument)
            Bind()
        End If
        If e.CommandName = "EditInvoice" Then
            Response.Redirect("EditInvoice.aspx?ID=" & CType(e.Item.FindControl("HdnCustID"), HiddenField).Value & "&InvId=" & e.CommandArgument)
        End If

    End Sub

    Private Sub reptax_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles reptax.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            If Not IsDBNull(e.Item.DataItem("InvoiceNo")) Then
                If e.Item.DataItem("InvoiceNo") <> "" Then
                    CType(e.Item.FindControl("LblInvNo"), Label).Text = e.Item.DataItem("InvoiceNo")
                Else
                    CType(e.Item.FindControl("LblInvNo"), Label).Text = "XXXXXXXXXXXX"
                End If
            End If
            If Not IsDBNull(e.Item.DataItem("InvoiceNo")) Then
                If e.Item.DataItem("InvoiceNo") <> "" Then
                    CType(e.Item.FindControl("LblDInvNo"), Label).Text = e.Item.DataItem("InvoiceNo")
                Else
                    CType(e.Item.FindControl("LblDInvNo"), Label).Text = "XXXXXXXXXXXX"
                End If
            End If
            If Not IsDBNull(e.Item.DataItem("CustomerName")) Then CType(e.Item.FindControl("LblName"), Label).Text = e.Item.DataItem("CustomerName")
            If Not IsDBNull(e.Item.DataItem("InvAmount")) Then CType(e.Item.FindControl("LblInvAmt"), Label).Text = e.Item.DataItem("InvAmount")
            If Not IsDBNull(e.Item.DataItem("InvDate")) Then CType(e.Item.FindControl("InvDate"), Label).Text = obj.GetFormattedDate(e.Item.DataItem("InvDate"))
            If Not IsDBNull(e.Item.DataItem("InvAmount")) Then CType(e.Item.FindControl("LblPayableAmount"), Label).Text = e.Item.DataItem("InvAmount")
            Dim Dt As New DataTable
            Dt = obj.GetDataTable("Select * From InvoiceDetails where InvID=" & e.Item.DataItem("InvID"))

            obj.BindListControl(CType(e.Item.FindControl("DrpTax"), DropDownList), "Select * From TaxMaster where Type='C'", "Name", "Amount")
            CType(e.Item.FindControl("DrpTax"), DropDownList).Items.Insert(0, New ListItem("Select Tax", "0"))
            If Not IsNothing("Dt") Then
                If Dt.Rows.Count > 0 Then
                    CType(e.Item.FindControl("RptInvDtl"), Repeater).DataSource = Dt
                    CType(e.Item.FindControl("RptInvDtl"), Repeater).DataBind()
                End If
            End If

            Dim dtpay As New DataTable
            dtpay = obj.GetDataTable("Select PaidAmount From PaymentReceived where InvID=" & e.Item.DataItem("InvID"))
            Dim PaidAmt As Integer = 0
            Dim OutAmount As Double = 0.0
            If Not IsNothing(dtpay) Then
                If dtpay.Rows.Count > 0 Then
                    For i As Integer = 0 To dtpay.Rows.Count - 1
                        PaidAmt += dtpay(i)("PaidAmount")
                    Next
                End If
            End If
            OutAmount = e.Item.DataItem("InvNetAmount") - PaidAmt
            CType(e.Item.FindControl("hdnOutAmount"), HiddenField).Value = OutAmount
            If Val(e.Item.DataItem("InvNetAmount")) = PaidAmt Then
                CType(e.Item.FindControl("LnkPaymentRecv"), LinkButton).Visible = False
            Else
                CType(e.Item.FindControl("LnkPaymentRecv"), LinkButton).Visible = True
            End If
            CType(e.Item.FindControl("dvwotds"), HtmlControl).Style.Add("display", "none")
        End If
    End Sub
    Public Sub rptInvDtl_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        If e.Item.ItemIndex >= 0 Then
            If Not IsDBNull(e.Item.DataItem("Product")) Then CType(e.Item.FindControl("LblProdct"), Label).Text = e.Item.DataItem("Product")
            If Not IsDBNull(e.Item.DataItem("Qty")) Then CType(e.Item.FindControl("LblQty"), Label).Text = e.Item.DataItem("Qty")
            If Not IsDBNull(e.Item.DataItem("UnitPrice")) Then CType(e.Item.FindControl("LblUnitPrice"), Label).Text = e.Item.DataItem("UnitPrice")
            If Not IsDBNull(e.Item.DataItem("TotalAmount")) Then CType(e.Item.FindControl("LblAmount"), Label).Text = e.Item.DataItem("TotalAmount")
        End If
    End Sub

    Private Sub reptax_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles reptax.ItemCommand
        If e.CommandName = "SavePayment" Then
            If CType(e.Item.FindControl("RdbType"), RadioButtonList).SelectedValue = "WO" Then
                Dim rst As New RecorsetUpdate
                rst.TableParameter("PaymentReceived", "Add")
                rst.SetField("InvID", e.CommandArgument, "Number")
                If CType(e.Item.FindControl("TxtAmtRecved"), TextBox).Text = "" Then
                    CType(e.Item.FindControl("LblMsg"), Label).Visible = True
                    CType(e.Item.FindControl("LblMsg"), Label).Text = "Please Enter Cheque No !"
                    CType(e.Item.FindControl("dvwotds"), HtmlControl).Style.Remove("display")
                    CType(e.Item.FindControl("MdlPay"), AjaxControlToolkit.ModalPopupExtender).Show()
                    GoTo fin
                Else
                    rst.SetField("PaidAmount", CType(e.Item.FindControl("TxtAmtRecved"), TextBox).Text, "Number")
                End If
                rst.SetField("Type", CType(e.Item.FindControl("RdbType"), RadioButtonList).SelectedValue, "String")
                rst.SetField("PaymentMode", CType(e.Item.FindControl("DrpPaymenyMode"), DropDownList).SelectedValue, "String")
                If CType(e.Item.FindControl("DrpPaymenyMode"), DropDownList).SelectedValue = "CH" Then
                    If CType(e.Item.FindControl("TxtChqNo"), TextBox).Text <> "" Then
                        rst.SetField("ChequeNo", CType(e.Item.FindControl("TxtChqNo"), TextBox).Text, "Number")
                    Else
                        CType(e.Item.FindControl("LblMsg"), Label).Visible = True
                        CType(e.Item.FindControl("LblMsg"), Label).Text = "Please Enter Cheque No !"
                        CType(e.Item.FindControl("dvwotds"), HtmlControl).Style.Remove("display")
                        CType(e.Item.FindControl("trCh"), HtmlTableRow).Style.Remove("display")
                        CType(e.Item.FindControl("MdlPay"), AjaxControlToolkit.ModalPopupExtender).Show()
                        GoTo fin
                    End If
                End If
                If CType(e.Item.FindControl("DrpPaymenyMode"), DropDownList).SelectedValue = "NE" Then
                    If CType(e.Item.FindControl("TxtTransaction"), TextBox).Text <> "" Then
                        rst.SetField("TransactionID", CType(e.Item.FindControl("TxtTransaction"), TextBox).Text, "String")
                    Else
                        CType(e.Item.FindControl("LblMsg"), Label).Visible = True
                        CType(e.Item.FindControl("LblMsg"), Label).Text = "Please Enter TransactionID !"
                        CType(e.Item.FindControl("dvwotds"), HtmlControl).Style.Remove("display")
                        CType(e.Item.FindControl("trNet"), HtmlTableRow).Style.Remove("display")
                        CType(e.Item.FindControl("MdlPay"), ModalPopupExtender).Show()
                        GoTo fin
                    End If
                End If
                rst.SetField("Remark", CType(e.Item.FindControl("TxtRemark"), TextBox).Text, "String")
                rst.Execute()

            Else
                Dim rst As New RecorsetUpdate
                rst.TableParameter("PaymentReceived", "Add")
                rst.SetField("InvID", e.CommandArgument, "Number")
                rst.SetField("PaidAmount", CType(e.Item.FindControl("LblPayableAmount"), Label).Text, "Number")
                rst.SetField("Type", CType(e.Item.FindControl("RdbType"), RadioButtonList).SelectedValue, "String")
                rst.SetField("TaxValue", CType(e.Item.FindControl("TxtTaxValue"), TextBox).Text, "Number")
                rst.SetField("TaxAmount", CType(e.Item.FindControl("HdnAmountWOTDS"), HiddenField).Value, "Number")
                rst.SetField("Remark", CType(e.Item.FindControl("TxtWoRemark"), TextBox).Text, "String")
                rst.Execute()
fin:
            End If
        End If

    End Sub


    'Private Sub btnrdb_Click(sender As Object, e As EventArgs) Handles btnrdb.Click
    '    For i As Integer = 0 To reptax.Items.Count - 1
    '        If CType(reptax.Items(i).FindControl("Rdbtype"), RadioButtonList).SelectedValue = "WT" Then
    '            CType(reptax.Items(i).FindControl("dvwtds"), HtmlGenericControl).Visible = True
    '            CType(reptax.Items(i).FindControl("dvwotds"), HtmlGenericControl).Visible = False
    '        ElseIf CType(reptax.Items(i).FindControl("Rdbtype"), RadioButtonList).SelectedValue = "WO" Then
    '            CType(reptax.Items(i).FindControl("dvwotds"), HtmlGenericControl).Visible = True
    '            CType(reptax.Items(i).FindControl("dvwtds"), HtmlGenericControl).Visible = False
    '        End If

    '    Next
    'End Sub
    'Protected Sub BtnDelete_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnDelete.Click
    '    Dim strId As String = ""
    '    For i As Integer = 0 To rep.Items.Count - 1
    '        If CType(rep.Items(i).FindControl("ChkUserInfo"), CheckBox).Checked = True Then
    '            strId &= CType(rep.Items(i).FindControl("LblChk"), Label).Text & ","
    '        End If
    '    Next

    '    If strId <> "" Then

    '        strId = Left(strId, strId.Length - 1)

    '        Dim Sql As String = "Delete From InvoiceMaster where LeadId in (" & strId & ")"
    '        obj.ExecuteCommand(Sql)
    '        Bind()
    '       \ LblMessage.Text = "Sucessfully Deleted !"
    '    End If

    'End Sub



End Class
