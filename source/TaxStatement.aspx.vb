﻿Imports System.Data
Imports System.IO
Imports AjaxControlToolkit
Partial Class TaxStatement
    Inherits System.Web.UI.Page
    Dim obj As New Engine
    Dim ToAmount As Double = 0.0
    Dim ToSGST As Double = 0.0
    Dim ToCGST As Double = 0.0
    Dim ToIGST As Double = 0.0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Session("CompanyId") = Nothing Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            CType(UCHeader.FindControl("LblHeading"), Label).Text = "Tax Statement"
            'TxtFrom.Text = obj.GetFormattedDate(Date.Today.AddMonths(-1))
            'TxtTo.Text = obj.GetFormattedDate(Date.Today)

            TxtFrom.Attributes.Add("Readonly", "Readonly")
            TxtTo.Attributes.Add("Readonly", "Readonly")

            BindDrp()
            Bind()
        End If
    End Sub

    Public Sub BindDrp()
        Dim sql As String = ""
        sql &= "select CustomerId,CompanyName from CustomerMaster Where CustomerId=" & Session("CustId") & ";"
        sql &= "select * from MediumMaster;"
        Dim ds As New DataSet
        ds = obj.GetDataset(sql)

        obj.BindListControl1(drpSerCust, ds.Tables(0), "CompanyName", "CustomerId")

        drpSerPro.Items.Insert(0, New ListItem("All Product", ""))
        If drpSerCust.SelectedValue <> "" Then
            bindPro(drpSerCust.SelectedValue)
        End If

    End Sub
    Public Sub bindPro(ByVal id As Integer)
        obj.BindListControl(drpSerPro, "select ProductId,ProductName from ProductMaster where CustomerId=" & id, "ProductName", "ProductId")
    End Sub
    Public Sub Bind()
        Dim sql = "", sqltax = "", strWhere As String = ""

        If Not IsNothing(Request.QueryString("CustID")) Then
            strWhere &= " and LM.CustomerId=" & Request.QueryString("CustID")
        End If

        If drpSerCust.SelectedValue <> "" Then
            strWhere &= " and CM.CustomerId=" & drpSerCust.SelectedValue
        End If

        If TxtFrom.Text <> "" Then
            strWhere &= " and LM.InvDate>='" & TxtFrom.Text.Replace("'", "''") & "'"
        End If

        If TxtTo.Text <> "" Then
            strWhere &= " and LM.InvDate<=DATEADD(day,1,'" & TxtTo.Text.Replace("'", "''") & "')"
        End If

        sqltax &= "select Cu.Name as CustomerName,Cu.Mobile,Lm.* from InvoiceMaster LM "
        sqltax &= "left join UserCustomerMaster Cu on LM.CustomerId=Cu.CustomerId "
        sqltax &= "left join CustomerMaster CM on CM.CustomerId=LM.CompanyID "
        sqltax &= " where 1=1 and InvoiceType='T' and CM.CustomerId= " & Session("CompanyId") & strWhere & " order by LM.InvID desc"

        Dim IntRecourtCount As Integer
        IntRecourtCount = obj.BindGridControl(reptax, sqltax, LblTaxHideUserCount, LblTaxDispUserCount, LblTaxNoUserCount, "Tax Statement", "LnkPreviousRecord", "LnkNextRecord", "DrpOnRecord", 50)

    End Sub

    Public Sub PreviousTaxUserRecord(ByVal sender As Object, ByVal e As System.EventArgs)
        LblTaxHideUserCount.Text = CInt(LblTaxHideUserCount.Text) - 1
        obj.PreviousRecord()
        Bind()
    End Sub

    Public Sub NextTaxUserRecord(ByVal sender As Object, ByVal e As System.EventArgs)
        LblTaxHideUserCount.Text = CInt(LblTaxHideUserCount.Text) + 1
        obj.NextRecord()
        Bind()
    End Sub

    Protected Sub OnUserRecordTax(ByVal sender As Object, ByVal e As System.EventArgs)
        LblTaxHideUserCount.Text = CType(reptax.Controls(reptax.Controls.Count - 1).FindControl("DrpOnRecord"), DropDownList).SelectedValue
        obj.OnRecord(LblTaxHideUserCount.Text)
        Bind()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Bind()
    End Sub


    Private Sub reptax_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles reptax.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            If Not IsDBNull(e.Item.DataItem("InvoiceNo")) Then
                If e.Item.DataItem("InvoiceNo") <> "" Then
                    CType(e.Item.FindControl("LblInvNo"), Label).Text = e.Item.DataItem("InvoiceNo")
                Else
                    CType(e.Item.FindControl("LblInvNo"), Label).Text = "XXXXXXXXXXXX"
                End If
            End If

            Dim Product As String = ""
            Dim dt As New DataTable
            dt = obj.GetDataTable("Select Product From InvoiceDetails where Invid=" & e.Item.DataItem("invID"))
            If dt.Rows.Count > 0 Then
                For i As Integer = 0 To dt.Rows.Count - 1
                    Product &= dt.Rows(i).Item("Product") & "</br>"
                Next
            End If

            If Product <> "" Then
                Product = Left(Product, Product.Length - 1)
            End If

            Dim SGST As Double = 0.0
            SGST = obj.GetField("Select isnull(sum(TaxAmount),0) as Amt From InvoiceTaxDetails IT left join TaxMaster Tm on IT.taxID=Tm.TaxID where InvID=" & e.Item.DataItem("InvID") & " and Tm.name like '%SGST%'", "Amt")

            Dim CGST As Double = 0.0
            CGST = obj.GetField("Select isnull(sum(TaxAmount),0) as Amt From InvoiceTaxDetails IT left join TaxMaster Tm on IT.taxID=Tm.TaxID where InvID=" & e.Item.DataItem("InvID") & " and Tm.name like '%CGST%'", "Amt")

            Dim IGST As Double = 0.0
            IGST = obj.GetField("Select isnull(sum(TaxAmount),0) as Amt From InvoiceTaxDetails IT left join TaxMaster Tm on IT.taxID=Tm.TaxID where InvID=" & e.Item.DataItem("InvID") & " and Tm.name like '%IGST%'", "Amt")

            CType(e.Item.FindControl("LblPDesc"), Label).Text = Product
            If Not IsDBNull(e.Item.DataItem("CustomerName")) Then CType(e.Item.FindControl("LblName"), Label).Text = e.Item.DataItem("CustomerName")
            If Not IsDBNull(e.Item.DataItem("InvDate")) Then CType(e.Item.FindControl("LblInvDate"), Label).Text = obj.GetFormattedDate(e.Item.DataItem("InvDate"))
            If Not IsDBNull(e.Item.DataItem("InvAmount")) Then CType(e.Item.FindControl("LblInvAmt"), Label).Text = e.Item.DataItem("InvAmount")
            CType(e.Item.FindControl("LblSGST"), Label).Text = SGST
            CType(e.Item.FindControl("LblCGST"), Label).Text = CGST
            CType(e.Item.FindControl("LblIGST"), Label).Text = IGST

            ToAmount += e.Item.DataItem("InvAmount")
            ToSGST += SGST
            ToCGST += CGST
            ToIGST += IGST

        End If
        If e.Item.ItemType = ListItemType.Footer Then
            CType(e.Item.FindControl("LblToAmt"), Label).Text = ToAmount
            CType(e.Item.FindControl("LblToSGST"), Label).Text = ToSGST
            CType(e.Item.FindControl("LblToCGST"), Label).Text = ToCGST
            CType(e.Item.FindControl("LblToIGST"), Label).Text = ToIGST

        End If
    End Sub
End Class
