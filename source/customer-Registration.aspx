﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="customer-Registration.aspx.vb" Inherits="customer_Registration" %>


<%@ Register Src="~/webheader.ascx" TagName="hdr" TagPrefix="uc" %>
<%@ Register Src="~/webfooter.ascx" TagName="ftr" TagPrefix="uc" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>   

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Contact Us</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="Images/web/thumb.png" />
    <link href="Css/website/css.css" rel="Stylesheet" type="text/css" />
    <link href="Css/website/css.css" rel="Stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <ajax:ToolkitScriptManager runat="Server" ID="ScriptManager1">
</ajax:ToolkitScriptManager>
            <div>
                <uc:hdr ID="Healer" runat="server" />
            </div>
            <div>
                <a href="contact-us.aspx">
                    <img src="Images/web/banner4.png" class="max width" /></a>
            </div>
            <div class="container">
                <div class="row">
                     <div class="col-md-12 col-sm-12 col-xs-12 padding">
                         <div class="txt13">
                              Hey Lets Register !!!
                            </div>
                         </div>
                      <div class="col-md-12 col-sm-12 col-xs-12 padding">
                                        <div class="col-md-3 col-sm-3 col-xs-12">
                                            <div class="form-group">
                                                <label for="email">
                                                    <span class="red">*</span>Company Name
                                                </label>
                                                <asp:TextBox ID="txtCompName" runat="server" class="form-control custFreeBorder"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="req" runat="server" ControlToValidate="txtCompName"
                                                    ErrorMessage="<br/>Enter Company Name" InitialValue="" SetFocusOnError="true"
                                                    Display="Dynamic" ValidationGroup="Save"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-3 col-xs-12">
                                            <div class="form-group">
                                                <label for="email">
                                                    <span class="red">*</span>Contact Person
                                                </label>
                                                <asp:TextBox ID="txtCustName" runat="server" class="form-control custFreeBorder"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtCustName"
                                                    ErrorMessage="<br/>Enter Customer Name" InitialValue="" SetFocusOnError="true"
                                                    Display="Dynamic" ValidationGroup="Save"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-3 col-xs-12">
                                            <div class="form-group">
                                                <label for="email">
                                                    <span class="red">*</span>User Email
                                                </label>
                                                <asp:TextBox ID="txtUserEmail" runat="server" class="form-control custFreeBorder"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtUserEmail"
                                                    ErrorMessage="<br/>Enter User Email" InitialValue="" SetFocusOnError="true" Display="Dynamic"
                                                    ValidationGroup="Save"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtUserEmail"
                                                    SetFocusOnError="true" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                    ErrorMessage="<br/>Enter Valid EmailID" Text="" Display="Dynamic" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-3 col-xs-12">
                                            <div class="form-group">
                                                <label for="email">
                                                    <span class="red">*</span>Password
                                                </label>
                                                <asp:TextBox ID="txtPass" TextMode="Password" runat="server" MaxLength="10" class="form-control custFreeBorder"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtPass"
                                                    ErrorMessage="<br/>Enter Password" InitialValue="" SetFocusOnError="true" Display="Dynamic"
                                                    ValidationGroup="Save"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-3 col-xs-12">
                                            <div class="form-group">
                                                <label for="email">
                                                    <span class="red">*</span>Alert Email
                                                </label>
                                                <asp:TextBox ID="txtAlertEmail" runat="server" TextMode="MultiLine" Rows="2" class="form-control custFreeBorder"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtAlertEmail"
                                                    ErrorMessage="<br/>Enter Alert Email" InitialValue="" SetFocusOnError="true"
                                                    Display="Dynamic" ValidationGroup="Save"></asp:RequiredFieldValidator>
                                                <br />
                                                <b style="color: #ff0000;">Seprate each Email by comma</b>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-3 col-xs-12">
                                            <div class="form-group">
                                                <label for="email">
                                                    <span class="red">*</span>Alert Mobile
                                                </label>
                                                <asp:TextBox ID="txtMob" runat="server" MaxLength="31" TextMode="MultiLine" Rows="2"
                                                    class="form-control custFreeBorder"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtMob"
                                                    ErrorMessage="<br/>Enter Alert Email" InitialValue="" SetFocusOnError="true"
                                                    Display="Dynamic" ValidationGroup="Save"></asp:RequiredFieldValidator>
                                                <ajax:FilteredTextBoxExtender ID="fil" runat="server" TargetControlID="txtMob" FilterType="Numbers,Custom"
                                                    ValidChars="+,">
                                                </ajax:FilteredTextBoxExtender>
                                                <br />
                                                <b style="color: #ff0000;">Seprate each mobile by comma</b>
                                            </div>
                                        </div>
                                       
                                    </div>
                                  
                                    
                                    <div class="col-md-12 col-sm-12 col-xs-12 space1">
                                        <table width="100%" class="table">
                                            <tr>
                                                <th colspan="2">Custom Fields
                                                </th>
                                                <th>View In List
                                                </th>
                                                <th>Sequence
                                                </th>
                                                <th>IsMandatory
                                                </th>
                                            </tr>
                                            <asp:Repeater ID="rep" runat="server">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td align='right'>
                                                            <asp:Label ID="lblName" runat="server"></asp:Label>
                                                            :
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtName" runat="server" CssClass="custFreeBorder form-control"></asp:TextBox>
                                                        </td>
                                                        <td align="center">
                                                            <asp:RadioButtonList ID="rdView" runat="server" RepeatDirection="Horizontal">
                                                                <asp:ListItem Text="Yes" Value="True"></asp:ListItem>
                                                                <asp:ListItem Text="No" Value="False" Selected="True"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtSeq" runat="server" CssClass="custFreeBorder form-control"></asp:TextBox>
                                                            <ajax:FilteredTextBoxExtender ID="fil" runat="server" TargetControlID="txtSeq" FilterType="Numbers">
                                                            </ajax:FilteredTextBoxExtender>
                                                        </td>
                                                        <td>
                                                            <asp:RadioButtonList ID="rdMand" runat="server" RepeatDirection="Horizontal">
                                                                <asp:ListItem Text="Yes" Value="True"></asp:ListItem>
                                                                <asp:ListItem Text="No" Value="False" Selected="True"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </table>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 space1">
                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                        <asp:Button runat="server" ID="BtnSubmit" Text="Save" CssClass="btn btn-default" ValidationGroup="Save" />
                                             </div>
                                         <div class="col-md-3 col-sm-3 col-xs-12" style="display:none">
                                        <asp:Button runat="server" ID="btnNew" Text="Save and New" CssClass="btn btn-default" ValidationGroup="Save" />
                                             </div>
                                           <div class="col-md-6 col-sm-6 col-xs-12">
                                        <asp:Button runat="server" ID="BtnCancel" Text="Cancel" CausesValidation="false"
                                            CssClass="btn btn-default" />
                                             </div>
                                        <asp:Label runat="server" ID="LblMode" Visible="false" Text="Add"></asp:Label>
                                    </div>
                     <div class="col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label runat="server" ID="LblId" Visible="false"></asp:Label>
                                        <asp:Label runat="server" ID="LblMessage" CssClass="bold red"></asp:Label>
                                    </div>
                </div>
            </div>
            <div class="container-fluid yellowbg">
                <div class="container">
                    <div align="center" class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="txt9">
                                Try Cusrela Now.... Love It Forever
                            </div>
                           
                        </div>
                    </div>
                    <div align="center" class="row">
                        
                    </div>
                </div>
            </div>
            <div>
                <uc:ftr ID="Footer" runat="server" />
            </div>
        </div>
    </form>
</body>
</html>

