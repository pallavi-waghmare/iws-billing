﻿Imports System.Data
Partial Class customer_Registration
    Inherits System.Web.UI.Page
    Dim obj As New Engine

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Not IsPostBack Then

            LblId.Text = "0"
            If Not IsNothing(Request.QueryString("Id")) Then
                LblId.Text = Request.QueryString("Id")
                LblMode.Text = "Edit"
                'bind()
            End If
            bindFields()

        End If
    End Sub
    'Public Sub bind()
    '    Dim ds As New DataSet
    '    Dim sql As String = ""

    '    sql &= "select * from CustomerMaster where CustomerId=" & Request.QueryString("Id") & ";"
    '    sql &= "select * from SMSAlertSetting where CompanyId=" & Request.QueryString("Id") & ";"
    '    sql &= "select * from EmailAlertSetting where CompanyId=" & Request.QueryString("Id") & ";"

    '    ds = obj.GetDataset(sql)

    '    Dim dt As New DataTable
    '    dt = ds.Tables(0)
    '    If dt.Rows.Count > 0 Then
    '        txtAlertEmail.Text = dt.Rows(0).Item("AlertEmail")
    '        txtCompName.Text = dt.Rows(0).Item("CompanyName")
    '        txtCustName.Text = dt.Rows(0).Item("CustomerName")
    '        txtMob.Text = dt.Rows(0).Item("AlertMobile")
    '        txtUserEmail.Text = dt.Rows(0).Item("UserEmail")
    '        drpActive.SelectedValue = dt.Rows(0).Item("ActiveTax")
    '        txtPass.Attributes.Add("Value", dt.Rows(0).Item("LoginPassword"))
    '    End If
    '    If dt.Rows(0).Item("LeadSystem") = True Then
    '        ChkProvideLs.Checked = True
    '        TrEnddate.Attributes.Remove("style")
    '        TrFollowupSms.Attributes.Remove("style")
    '        TrFollowupEmail.Attributes.Remove("style")
    '        TrMassEmail.Attributes.Remove("style")
    '        TrMassSms.Attributes.Remove("style")
    '        TrAdminUsrCnt.Attributes.Remove("style")
    '        TrStaffUserCnt.Attributes.Remove("style")
    '        TxtFrom.Text = dt.Rows(0).Item("ENdate")
    '        If dt.Rows(0).Item("MassEmail") = True Then
    '            ChkMassEmail.Checked = True
    '        Else
    '            ChkMassEmail.Checked = False

    '        End If
    '        If dt.Rows(0).Item("MassSms") = True Then
    '            ChkMassSms.Checked = True
    '        Else
    '            ChkMassEmail.Checked = False

    '        End If
    '        If dt.Rows(0).Item("FollupEmail") = True Then
    '            ChkFollowupEmail.Checked = True
    '        Else
    '            ChkFollowupEmail.Checked = False

    '        End If
    '        If dt.Rows(0).Item("FollupSms") = True Then
    '            ChkFollowUpSms.Checked = True
    '        Else
    '            ChkFollowUpSms.Checked = False

    '        End If
    '        TxtAdminUserCnt.Text = dt.Rows(0).Item("AdminUserCount")
    '        TxtStaffUserCnt.Text = dt.Rows(0).Item("StaffUserCount")
    '        TxtEmailCnt.Text = dt.Rows(0).Item("EmailCount")
    '        TxtSmsCnt.Text = dt.Rows(0).Item("SmsCount")

    '    Else
    '        ChkProvideLs.Checked = False
    '    End If


    '    If ds.Tables(1).Rows.Count > 0 Then
    '        txtSmsUrl.Text = ds.Tables(1).Rows(0).Item("SMSUrl")
    '    End If

    '    If ds.Tables(2).Rows.Count > 0 Then
    '        txtHost.Text = ds.Tables(2).Rows(0).Item("MailHost")
    '        txtMailUser.Text = ds.Tables(2).Rows(0).Item("MailUserName")

    '        txtMailPass.Attributes.Add("Value", ds.Tables(2).Rows(0).Item("MailPassword"))

    '        txtFrmMail.Text = ds.Tables(2).Rows(0).Item("FromEmailID")
    '    End If

    'End Sub
    Public Sub bindFields()
        Dim dt As New DataTable
        Dim sql As String = ""

        sql &= " Select V.fields,CF.CustomName,CF.IsMandatory,CF.ViewInList,CF.Sequence from fields V "
        sql &= " Left Join CUSTOMERFIEL(" & LblId.Text & ") CF on CF.FiledName=v.FIELDS "
        sql &= " order by v.fieldsid "
        dt = obj.GetDataTable(sql)
        rep.DataSource = dt
        rep.DataBind()

    End Sub
    Private Sub rep_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rep.ItemDataBound
        If e.Item.ItemIndex >= 0 Then

            If Not IsDBNull(e.Item.DataItem("FIELDS")) Then CType(e.Item.FindControl("lblName"), Label).Text = e.Item.DataItem("FIELDS")
            If Not e.Item.DataItem("FIELDS").ToString.Contains("Custom") Then
                CType(e.Item.FindControl("txtName"), TextBox).Visible = False
                CType(e.Item.FindControl("rdMand"), RadioButtonList).Visible = False
            End If
            If Not IsDBNull(e.Item.DataItem("Sequence")) Then CType(e.Item.FindControl("txtSeq"), TextBox).Text = e.Item.DataItem("Sequence")
            If Not IsDBNull(e.Item.DataItem("CustomName")) Then CType(e.Item.FindControl("txtName"), TextBox).Text = e.Item.DataItem("CustomName")
            If Not IsDBNull(e.Item.DataItem("IsMandatory")) Then CType(e.Item.FindControl("rdMand"), RadioButtonList).SelectedValue = e.Item.DataItem("IsMandatory")
            If Not IsDBNull(e.Item.DataItem("ViewInList")) Then CType(e.Item.FindControl("rdView"), RadioButtonList).SelectedValue = e.Item.DataItem("ViewInList")
        End If

    End Sub
    Public Function Save() As String
        'TxtFrom.Attributes.Remove("Readonly")
        Dim StrError As String = ""
        Dim StrWhere As String = ""
        Dim cnt As Integer = 0
        If LblMode.Text = "Edit" Then
            StrWhere = " and CustomerId<>" & LblId.Text
        End If
        Dim STR As String = "select COUNT(CustomerId) as cnt from CustomerMaster where (UserEmail='" & txtUserEmail.Text.Replace("'", "''") & "' or CompanyName='" & txtCompName.Text.Replace("'", "''") & "') " & StrWhere
        cnt = obj.GetField("select COUNT(CustomerId) as cnt from CustomerMaster where (UserEmail='" & txtUserEmail.Text.Replace("'", "''") & "' or CompanyName='" & txtCompName.Text.Replace("'", "''") & "') " & StrWhere, "cnt")
        Dim StrId As String = ""
        Dim rs As New RecorsetUpdate
        Dim rsSms As New RecorsetUpdate
        Dim rsMail As New RecorsetUpdate
        If cnt = 0 Then
            If LblMode.Text = "Edit" Then
                rs.TableParameter("CustomerMaster", "Edit", "CustomerId=" & LblId.Text)
                StrId = LblId.Text
            Else
                rs.TableParameter("CustomerMaster", "Add")
                StrId = "(select MAX(CustomerId) from CustomerMaster)"
            End If
            rs.SetField("CompanyName", txtCompName.Text.Replace("'", "''"), "String")
            rs.SetField("CustomerName", txtCustName.Text.Replace("'", "''"), "String")
            rs.SetField("UserEmail", txtUserEmail.Text.Replace("'", "''"), "String")
            rs.SetField("LoginPassword", txtPass.Text.Replace("'", "''"), "String")
            rs.SetField("AlertEmail", txtAlertEmail.Text.Replace("'", "''"), "String")
            rs.SetField("AlertMobile", txtMob.Text.Replace("'", "''"), "String")
            rs.SetField("ActiveTax", 1, "Number")
            rs.SetField("LeadSystem", 1, "Number")
            rs.SetField("FollupSms", 0, "Number")
            rs.SetField("FollupEmail", 0, "Number")
            rs.SetField("MassEmail", 1, "Number")
            rs.SetField("MassSms", 1, "Number")
            rs.SetField("SmsCount", 0, "Number")
            rs.SetField("EmailCount", 0, "Number")
            rs.SetField("StaffUserCount", 1, "Number")
            rs.SetField("AdminUserCount", 1, "Number")
            rs.SetField("Endate", "DATEADD(DAY,7,GETDATE())", "Number")
            rs.Execute()


            Dim sql As String = ""
            For i = 0 To rep.Items.Count - 1
                sql = ""

                Dim seq As Integer = 0

                If CType(rep.Items(i).FindControl("txtSeq"), TextBox).Text <> "" Then
                    seq = CType(rep.Items(i).FindControl("txtSeq"), TextBox).Text.Replace("'", "''")
                End If

                sql &= "If (EXISTS(SELECT * FROM CustomFields WHERE FiledName='" & CType(rep.Items(i).FindControl("lblName"), Label).Text & "' and CompanyId=" & StrId & ")) "
                sql &= "    BEGIN"
                sql &= "        Update CustomFields set Sequence=" & seq & ",CustomName='" & CType(rep.Items(i).FindControl("txtName"), TextBox).Text.Replace("'", "''") & "',IsMandatory='" & CType(rep.Items(i).FindControl("rdMand"), RadioButtonList).SelectedValue & "',ViewInList='" & CType(rep.Items(i).FindControl("rdView"), RadioButtonList).SelectedValue & "' where FiledName='" & CType(rep.Items(i).FindControl("lblName"), Label).Text.Replace("'", "''") & "' and CompanyId=" & StrId
                sql &= "    End "
                sql &= "Else "
                sql &= "    BEGIN "
                sql &= "        insert into CustomFields(FiledName, CustomName, CompanyId,IsMandatory,ViewInList,Sequence) values ('" & CType(rep.Items(i).FindControl("lblName"), Label).Text.Replace("'", "''") & "','" & CType(rep.Items(i).FindControl("txtName"), TextBox).Text.Replace("'", "''") & "'," & StrId & ",'" & CType(rep.Items(i).FindControl("rdMand"), RadioButtonList).SelectedValue & "','" & CType(rep.Items(i).FindControl("rdView"), RadioButtonList).SelectedValue & "'," & seq & ")"
                sql &= "    End "
                obj.ExecuteCommand(sql)
            Next
        Else
            StrError = "User Email Already Exists"
        End If


        Return StrError


    End Function

    Private Sub BtnSubmit_Click(sender As Object, e As EventArgs) Handles BtnSubmit.Click
        Dim str As String = ""
        str = Save()
        If str = "" Then
            Dim myScript As String = "window.alert('Thank You, please login and Enjoy your free trial');location.href='/Userlogin.aspx'"
            ClientScript.RegisterStartupScript(Me.GetType(), "myScript", myScript, True)
        Else
            LblMessage.Text = str
        End If
    End Sub

    

    Private Sub BtnCancel_Click(sender As Object, e As EventArgs) Handles BtnCancel.Click
        Dim myScript As String = "window.alert('Oops ! Something went wrong??');location.href='/Default.aspx'"
        ClientScript.RegisterStartupScript(Me.GetType(), "myScript", myScript, True)
    End Sub

    
End Class
