﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="UserCustomer.aspx.vb" Inherits="UserCustomer" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Src="~/Header.ascx" TagName="UCHeader" TagPrefix="UC1" %>
<%@ Register Src="~/footer.ascx" TagName="UCFooter" TagPrefix="UC1" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Customer List</title>
    <link rel="icon" href="../Images/thumb.png" />
    <link href="../Css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="../Css/css.css" rel="stylesheet" type="text/css" />
    <link href="Css/website/bootstrap.min.css" type="text/css" />
    <link type="text/css" rel="Stylesheet" href="../Css/Style.css" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div>
                <UC1:UCHeader ID="UCHeader" runat="server" />
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="boxshadow">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="title">
                                    Search Parameter
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 padding">
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="email">
                                            <label for="email">
                                                Customer
                                            </label>
                                            <asp:DropDownList ID="drpSerCust" runat="server" AutoPostBack="true" class="form-control">
                                            </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="email">
                                            Product
                                        </label>
                                        <asp:DropDownList ID="drpSerPro" runat="server" class="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="email">
                                            From
                                        </label>
                                        <asp:TextBox ID="TxtFrom" runat="server" class="form-control"></asp:TextBox>
                                        <ajax:CalendarExtender ID="AjxCel1" runat="server" TargetControlID="TxtFrom" Format="dd-MMM-yyyy"
                                            CssClass="cal">
                                        </ajax:CalendarExtender>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="email">
                                            To
                                        </label>
                                        <asp:TextBox ID="TxtTo" runat="server" class="form-control"></asp:TextBox>
                                        <ajax:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="TxtTo"
                                            Format="dd-MMM-yyyy" CssClass="cal">
                                        </ajax:CalendarExtender>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="form-group">
                                        <asp:Button runat="server" ID="btnSearch" Text="Search" CssClass="button1" CausesValidation="false" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 space1">
                                <asp:Label ID="LblDispUserCount" runat="server" CssClass="showtxt"></asp:Label>
                                <asp:Label ID="LblHideUserCount" runat="server" Visible="false" CssClass="showtxt"></asp:Label>
                                <asp:Label ID="LblNoUserCount" runat="server" CssClass="showtxt"></asp:Label>
                                <asp:Label ID="lblinfo" runat="server" CssClass="showtxt"></asp:Label>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 space">
                                <div>
                                    <asp:Label ID="lblLeads" runat="server"></asp:Label>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 space1">
                                <asp:Repeater runat="server" ID="rep">
                                    <HeaderTemplate>
                                        <table class="table">
                                            <tr>
                                                <th width="2%"></th>
                                                <th width="5%">Name
                                                </th>
                                                <th width="5%">EmailAddress
                                                </th>
                                                <th width="5%">Mobile
                                                </th>
                                                <th width="5%">CreateDate
                                                </th>
                                                <th width="5%">Product
                                                </th>
                                                <th width="4%">Total Leads
                                                </th>

                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td align="center">
                                                <asp:CheckBox runat="server" ID="ChkUserInfo" />
                                                <asp:Label runat="server" ID="LblChk" Text='<%#Container.DataItem("CustomerID")%>' Visible="false"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblName"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblEmail"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblMob"></asp:Label>
                                            </td>

                                            <td>
                                                <asp:Label runat="server" ID="lblCDate"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblPro"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblleadcount"></asp:Label>
                                            </td>



                                        </tr>
                                        <tr>
                                            <td colspan="7">
                                                <asp:Label runat="server" ID="LblRemark"></asp:Label>
                                                <asp:LinkButton ID="LnkInvoice" runat="server" CausesValidation="false" data-toggle="tooltip" data-placement="top" Text="Generate Invoice"
                                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "CustomerID") %>' CommandName="GenerateInvoice"></asp:LinkButton>
                                                &nbsp;|&nbsp;
                                                 <asp:LinkButton ID="LnkViewInvoice" runat="server" CausesValidation="false" Text="View Invoice"
                                                     CommandArgument='<%# DataBinder.Eval(Container.DataItem, "CustomerID") %>' CommandName="ViewInvoice"></asp:LinkButton>
                                                &nbsp;|&nbsp;
                                               <asp:LinkButton ID="LnkEdit" runat="server" CausesValidation="false" Text="Edit"
                                                   CommandArgument='<%# DataBinder.Eval(Container.DataItem, "CustomerID") %>' CommandName="Edit"></asp:LinkButton>
                                                &nbsp;|&nbsp;
                                               <asp:LinkButton ID="LnkBillingDtl" runat="server" CausesValidation="false" Text="Add Billing Details"
                                                   CommandArgument='<%# DataBinder.Eval(Container.DataItem, "CustomerID") %>' CommandName="AddBillingDtl"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <tr>
                                            <td colspan="12" style="text-align: center;">
                                                <asp:LinkButton ID="LnkPreviousRecord" Text="<< Prev" runat="server" OnClick="PreviousUserRecord" Style="top: 0px;"
                                                    CausesValidation="false" class="btnnxt" />&nbsp;
                                            <asp:DropDownList ID="DrpOnRecord" AutoPostBack="true" runat="server" class="pagedrp" Style="margin-top: 0px; width: 50px; float: none; display: inherit;" OnSelectedIndexChanged="OnUserRecord" />&nbsp;
                                         
                                            <asp:LinkButton ID="LnkNextRecord" Text="Next >>" runat="server" OnClick="NextUserRecord" Style="top: 0px;"
                                                CausesValidation="false" class="btnnxt" />&nbsp;
                                            </td>
                                        </tr>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 space" style="display: none">
                                <asp:Label ID="LblMessage" runat="server" ForeColor="Green"></asp:Label><br />
                                <asp:Button runat="server" ID="BtnDelete" Text="Delete Lead(s)" CssClass="dwnbtn"
                                    CausesValidation="false" />
                                <ajax:ConfirmButtonExtender ID="confDelete1" runat="server" TargetControlID="BtnDelete"
                                    DisplayModalPopupID="ModalPopupExtender1" />
                                <ajax:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="BtnDelete"
                                    PopupControlID="PNL1" OkControlID="ButtonOk1" CancelControlID="ButtonCancel1"
                                    BackgroundCssClass="ModalBackground" />
                                <asp:Panel ID="PNL1" runat="server" Style="display: none; padding: 20px;" CssClass="popup">
                                    <b>Are you sure you want to Delete this Lead ?</b>
                                    <br />
                                    <br />
                                    <div style="text-align: center;">
                                        <asp:Button ID="ButtonOk1" runat="server" Text="YES" CssClass="button" CausesValidation="false" />
                                        <asp:Button ID="ButtonCancel1" runat="server" Text="NO" CssClass="button" CausesValidation="false" />
                                    </div>
                                </asp:Panel>
                                <asp:Button runat="server" ID="BtnCsv" Text="Download Excel" CssClass="dwnbtn" CausesValidation="false" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <UC1:UCFooter ID="UCFooter" runat="server" />
            </div>
        </div>
    </form>
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <script src="js/bootstrap/bootstrap.min.js" type="text/javascript"></script>
    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
</body>
</html>
