﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Partial Class BillingDetails
    Inherits System.Web.UI.Page
    Dim obj As New Engine

    Private Sub AddSingleLead_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("CompanyId") = Nothing Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            CType(UCHeader.FindControl("LblHeading"), Label).Text = "Add/Edit Billing Details"
            If Not IsNothing(Request.QueryString("Id")) Then
                LblId.Text = Request.QueryString("Id")
                LblMode.Text = "Edit"
                bind()
            End If

        End If
    End Sub
    Public Sub bind()
        Dim dt As New DataTable
        dt = obj.GetDataTable("select LM.*,PM.CustomerId from BillingDetails LM left join BillingDetails PM on PM.Customerid=LM.Customerid where LM.CustomerId=" & Request.QueryString("Id"))
        If dt.Rows.Count > 0 Then
            'If dt.Rows(0).Item("LeadBy") <> Session("CompanyId") Then
            '    Response.Redirect("Leads.aspx")
            'End If
            txtCompanyName.Text = dt.Rows(0).Item("CompanyName")
            TxtAddress.Text = dt.Rows(0).Item("BillingAddress")
            TxtContactPerson.Text = dt.Rows(0).Item("ContactPerson")
            TxtGstNo.Text = dt.Rows(0).Item("GSTINNumber")
        Else
            LblMode.Text = "Add"
        End If
    End Sub

    Public Function Save() As String
        Dim StrError As String = ""
        Dim StrWhere As String = ""
        Dim cnt As Integer = 0
        Dim rs As New RecorsetUpdate
        Dim CurrentDateTime As DateTime
        CurrentDateTime = Date.Now
        CurrentDateTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(CurrentDateTime, TimeZoneInfo.Local.Id, "India Standard Time")

        If cnt = 0 Then
            If LblMode.Text = "Edit" Then
                rs.TableParameter("BillingDetails", "Edit", "CustomerID=" & LblId.Text)
                rs.SetField("LastUpdated", "GETDATE()", "Number")
            Else
                rs.SetField("LastUpdated", "GETDATE()", "Number")
                rs.TableParameter("BillingDetails", "Add")
            End If
            rs.SetField("Customerid", Request.QueryString("Id"), "String")
            rs.SetField("CompanyName", txtCompanyName.Text.Replace("'", "''"), "String")
            rs.SetField("BillingAddress", TxtAddress.Text.Replace("'", "''"), "String")
            rs.SetField("ContactPerson", TxtContactPerson.Text.Replace("'", "''"), "String")
            rs.SetField("GSTINNumber", TxtGstNo.Text.Replace("'", "''"), "String")
            rs.Execute()

        Else
            StrError = "Billing Details Already Exists"
        End If

        Return StrError

    End Function

    Private Sub BtnSubmit_Click(sender As Object, e As EventArgs) Handles BtnSubmit.Click
        Dim str As String = ""
        str = Save()
        If str = "" Then
            Response.Redirect("UserCustomer.aspx")
        Else
            LblMessage.Text = str
        End If
    End Sub

    Private Sub BtnCancel_Click(sender As Object, e As EventArgs) Handles BtnCancel.Click
        Response.Redirect("Leads.aspx")
    End Sub

End Class
