﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Partial Class AddCustomer
    Inherits System.Web.UI.Page
    Dim obj As New Engine

    Private Sub AddSingleLead_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("CompanyId") = Nothing Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            CType(UCHeader.FindControl("LblHeading"), Label).Text = "Add/Edit Customer"
            BindDrp()
            If Not IsNothing(Request.QueryString("Id")) Then
                LblId.Text = Request.QueryString("Id")
                LblMode.Text = "Edit"
                bind()
            End If

        End If
    End Sub
    Public Sub BindDrp()
        Dim sql As String = ""
        Dim ds As New DataSet
        sql &= "select ProductId,ProductName from ProductMaster where IsArchiEved=0 and CustomerId=" & Session("CompanyId") & ";"
        sql &= "select * from MediumMaster where (CompanyId=0 or CompanyId=" & Session("CompanyId") & ") and MediumId not in (select MediumId from CustomerMedium where CustomerId=" & Session("CompanyId") & " and IsActive=0);"
        'sql &= "select * from CustomFields where CompanyId=" & Session("CompanyId") & " and CustomName<>'' and ViewInCustomerList=1"
        'sql &= "Select * from LocationMAster;"
        'sql &= "select * from StatusMaster where CompanyId=0 or CompanyId=" & Session("CompanyId") & "and Statusid in (Select Statusid from CustomerStatus where CustomerId=" & Session("CompanyId") & " and IsActive= 1) order by StatusId;"
        'sql &= "select BifurcationId,Bifurcation from BifurcationMaster where CompanyId=" & Session("CompanyId") & " order by Bifurcation;"
        'sql &= "select * from ActionMaster;"
        'sql &= "Select * from BranchMaster where IsArchiEved = 0 "
        ds = obj.GetDataset(sql)

        'obj.BindListControl1(drpMed, ds.Tables(1), "MediumName", "MediumId")

        'obj.BindListControl1(drpPro, ds.Tables(0), "ProductName", "ProductId")
        'obj.BindListControl1(drplocation, ds.Tables(3), "Location", "LocationID")

        'drpPro.Items.Insert(0, New ListItem("Select Product", ""))
        'drpMed.Items.Insert(0, New ListItem("Select Medium", ""))
        ' drplocation.Items.Insert(0, New ListItem("Select Location", ""))
        '-------------Binding the Dropdown for the Add Followups 

        'If ds.Tables(7).Rows.Count > 0 Then
        '    obj.BindListControl1(DrpBranch, ds.Tables(7), "BranchName", "BranchId")
        'End If
        'drpStatus.Items.Insert(0, New ListItem("Select Status", "0"))
        'drpBirf.Items.Insert(0, New ListItem("Select Bifurcation", ""))
        'drpAction.Items.Insert(0, New ListItem("Select Action", ""))
        'DrpBranch.Items.Insert(0, New ListItem("Select Branch", "0"))
        'txtAlertDate.Attributes.Add("readonly", "readonly")



        '------------------- End Section 
        'rep.DataSource = ds.Tables(2)
        'rep.DataBind()

    End Sub
    Public Sub bindPro()
        'obj.BindListControl(drpPro, "select ProductId,ProductName from ProductMaster", "ProductName", "ProductId")
    End Sub
    Public Sub bind()
        Dim dt As New DataTable
        dt = obj.GetDataTable("select LM.*,PM.CustomerId from UserCustomerMaster LM left join ProductMaster PM on PM.ProductId=LM.ProductId where LM.CustomerId=" & Request.QueryString("Id"))
        If dt.Rows.Count > 0 Then
            'If dt.Rows(0).Item("LeadBy") <> Session("CompanyId") Then
            '    Response.Redirect("Leads.aspx")
            'End If
            txtEmail.Text = dt.Rows(0).Item("EmailAddress")
            txtMob.Text = dt.Rows(0).Item("Mobile")
            txtCName.Text = dt.Rows(0).Item("Name")
            'txtQuery.Text = dt.Rows(0).Item("Query")
            bindPro()
            'drpPro.SelectedValue = dt.Rows(0).Item("ProductId")
            'drpMed.SelectedValue = dt.Rows(0).Item("MediumId")

            For i = 0 To rep.Items.Count - 1
                CType(rep.Items(i).FindControl("txtField"), TextBox).Text = dt.Rows(0).Item(CType(rep.Items(i).FindControl("lblMainField"), Label).Text)
            Next
        Else
            Response.Redirect("UserCustomer.aspx")
        End If
    End Sub

    Public Function Save() As String
        Dim StrError As String = ""
        Dim StrWhere As String = ""
        Dim cnt As Integer = 0
        Dim rs As New RecorsetUpdate
        Dim CurrentDateTime As DateTime
        CurrentDateTime = Date.Now
        CurrentDateTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(CurrentDateTime, TimeZoneInfo.Local.Id, "India Standard Time")

        If cnt = 0 Then
            If LblMode.Text = "Edit" Then
                rs.TableParameter("UserCustomerMaster", "Edit", "CustomerID=" & LblId.Text)
                rs.SetField("LastUpdated", "GETDATE()", "Number")
            Else
                rs.SetField("LastUpdated", "GETDATE()", "Number")
                rs.TableParameter("UserCustomerMaster", "Add")
            End If
            rs.SetField("Name", txtCName.Text.Replace("'", "''"), "String")
            'rs.SetField("BranchId", DrpBranch.SelectedValue, "Number")
            rs.SetField("EmailAddress", txtEmail.Text.Replace("'", "''"), "String")
            rs.SetField("Mobile", txtMob.Text.Replace("'", "''"), "String")
            'If txtQuery.Text <> "" Then
            '    rs.SetField("Query", txtQuery.Text.Replace("'", "''"), "String")
            'End If
            ''rs.SetField("LastUpdated", "getdate()", "Number")
            'rs.SetField("ProductId", drpPro.SelectedValue, "Number")
            'rs.SetField("MediumId", drpMed.SelectedValue, "Number")

            rs.SetField("LeadBy", Session("CompanyId"), "Number")
            'Dim location As String = 0
            'location = obj.GetField("Select top 1 LocationId from LocationMaster where Location ='" & txtContactsSearch.Text & "'", "LocationId")
            'If location <> "" Then
            '    rs.SetField("location", location, "Number")
            'Else
            '    AddLocation()
            '    location = obj.GetField("Select top 1 LocationId from LocationMaster where Location ='" & txtContactsSearch.Text & "'", "LocationId")
            '    rs.SetField("location", location, "Number")
            'End If

            'For i = 0 To rep.Items.Count - 1
            '    If CType(rep.Items(i).FindControl("txtField"), TextBox).Text <> "" Then
            '        rs.SetField(CType(rep.Items(i).FindControl("lblMainField"), Label).Text, CType(rep.Items(i).FindControl("txtField"), TextBox).Text.Replace("'", "''"), "String")
            '    End If
            'Next

            rs.Execute()

        Else
            StrError = "User Email Already Exists"
        End If

        Return StrError

    End Function

    Private Sub BtnSubmit_Click(sender As Object, e As EventArgs) Handles BtnSubmit.Click
        Dim str As String = ""
        str = Save()
        If str = "" Then
            Response.Redirect("UserCustomer.aspx")
        Else
            LblMessage.Text = str
        End If
    End Sub

    Private Sub BtnCancel_Click(sender As Object, e As EventArgs) Handles BtnCancel.Click
        Response.Redirect("Leads.aspx")
    End Sub

    Private Sub rep_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rep.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            If e.Item.DataItem("IsMandatory") = True Then
                CType(e.Item.FindControl("lblField"), Label).Text = "<b class='red'>*</b>" & e.Item.DataItem("CustomName")
                CType(e.Item.FindControl("reqField"), RequiredFieldValidator).ValidationGroup = "Save"
                CType(e.Item.FindControl("reqField"), RequiredFieldValidator).ErrorMessage = "<br/>Enter " & e.Item.DataItem("CustomName")
            Else
                CType(e.Item.FindControl("lblField"), Label).Text = e.Item.DataItem("CustomName")
            End If
            CType(e.Item.FindControl("lblMainField"), Label).Text = e.Item.DataItem("FiledName")
        End If
    End Sub
    Public Sub AddLocation()
        'Dim rs As New RecorsetUpdate
        'rs.TableParameter("LocationMaster", "Add")
        'rs.SetField("Location", txtContactsSearch.Text, "String")
        'rs.Execute()
    End Sub

End Class
