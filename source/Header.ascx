﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Header.ascx.vb" Inherits="Header" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<link href="Css/Style.css" rel="stylesheet" type="text/css" />
<link href="Css/bootstrap.css" rel="stylesheet" type="text/css" />
<link href="Css/css.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css">
<ajax:ToolkitScriptManager runat="Server" EnablePartialRendering="true" ID="ScriptManager1" />

<div class="container-fluid headerbg">
    <div align="center" class="row">
        <div class="col-md-3 col-sm-3 col-xs-12">
            <div>
                <img src="Images/logo.png" class="max" />
            </div>
        </div>
        <div class="col-md-9 col-sm-9 col-xs-12 padding">
            <div class="col-md-12 col-sm-12 col-xs-12 padding">
                <div class="col-md-10 col-sm-10 col-xs-12">
                    <div class="welcometxt">
                        Welcome :
                        <asp:Label ID="LblUserName" runat="server" class="wellabel"></asp:Label>
                    </div>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-12">
                    <div>
                        <asp:LinkButton ID="LnkLogOut" runat="server" CausesValidation="False" Text="Log&nbsp;out"
                            CssClass="logout"></asp:LinkButton><i class="fas fa-sign-out-alt logicon"></i>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="top_menu">
                    <ul>
                        <li class="item" id="LiHome" runat="server">
                            <asp:LinkButton ID="LnkHome" runat="server" PostBackUrl="~/Dashboard.aspx" CausesValidation="false">Dashboard</asp:LinkButton></li>
                       
                        <li class="item" id="liAlert" runat="server">
                            <asp:LinkButton ID="LinkButton2" runat="server" PostBackUrl="~/Alert.aspx" CausesValidation="false">My Alerts</asp:LinkButton></li>
                        
                          <li class="item parent" id="li16" runat="server"><a href="#">Customer</a>
                            <div class="submenu-dv">
                                <div class="submenu_dv_outer">
                                    <ul>
                                        <li id="li17" runat="server">
                                            <asp:LinkButton ID="LinkButton29" runat="server" PostBackUrl="~/UserCustomer.aspx"
                                                CausesValidation="false"><span>Customer List</span></asp:LinkButton></li>
                                        <li id="li18" runat="server">
                                            <asp:LinkButton ID="LinkButton30" runat="server" PostBackUrl="~/AddCustomer.aspx"
                                                CausesValidation="false"><span>Add New Customer</span></asp:LinkButton></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li class="item" id="liInvoiceList" runat="server">
                            <asp:LinkButton ID="LnkInvoiceList" runat="server" PostBackUrl="~/InvoiceList.aspx" CausesValidation="false">Invoice List</asp:LinkButton></li>
                        <li class="item parent" id="li19" runat="server"><a href="#">Reports</a>
                            <div class="submenu-dv">
                                <div class="submenu_dv_outer">
                                    <ul>
                                        <li id="li20" runat="server">
                                            <asp:LinkButton ID="LinkButton31" runat="server" PostBackUrl="~/OutstandingReport.aspx"
                                                CausesValidation="false"><span>Outstanding Report</span></asp:LinkButton></li>
                                        <li id="li21" runat="server">
                                            <asp:LinkButton ID="LinkButton32" runat="server" PostBackUrl="~/SalesReport.aspx"
                                                CausesValidation="false"><span>Sales Report</span></asp:LinkButton></li>
                                        <li id="li22" runat="server">
                                            <asp:LinkButton ID="LinkButton33" runat="server" PostBackUrl="~/TaxStatement.aspx"
                                                CausesValidation="false"><span>Tax Statement</span></asp:LinkButton></li>
                                        <li id="li23" runat="server">
                                            <asp:LinkButton ID="LinkButton34" runat="server" PostBackUrl="~/TDSStatement.aspx"
                                                CausesValidation="false"><span>TDS Statement</span></asp:LinkButton></li>
                                        <li id="li24" runat="server">
                                            <asp:LinkButton ID="LinkButton35" runat="server" PostBackUrl="~/LedgerReport.aspx"
                                                CausesValidation="false"><span>Ledger Report</span></asp:LinkButton></li>
                                    </ul>
                                </div>
                            </div>
                        </li>

                        <li class="item parent" id="liPro" runat="server"><a href="#">Product</a>
                            <div class="submenu-dv">
                                <div class="submenu_dv_outer">
                                    <ul>
                                        <li id="li2" runat="server">
                                            <asp:LinkButton ID="LinkButton6" runat="server" PostBackUrl="~/MyProducts.aspx" CausesValidation="false"><span>My Product</span></asp:LinkButton></li>
                                        <li id="li3" runat="server">
                                            <asp:LinkButton ID="LinkButton12" runat="server" PostBackUrl="~/ArchiveProducts.aspx"
                                                CausesValidation="false"><span>Archive products</span></asp:LinkButton></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li class="item" id="liProfile" runat="server">
                            <asp:LinkButton ID="LinkButton13" runat="server" PostBackUrl="~/MyProfile.aspx" CausesValidation="false">My Profile</asp:LinkButton></li>
                       
                        <li class="item parent" id="liConfig" runat="server"><a href="#">Configuration</a>
                            <div class="submenu-dv">
                                <div class="submenu_dv_outer">
                                    <ul>
                                       
                                        <li id="LiInvHeader" runat="server">
                                            <asp:LinkButton ID="LinkButton26" runat="server" PostBackUrl="~/InvoiceTemplate.aspx"
                                                CausesValidation="false"><span>Invoice Header Template</span></asp:LinkButton></li>
                                        <li id="liInvFooter" runat="server">
                                            <asp:LinkButton ID="LinkButton27" runat="server" PostBackUrl="~/InvoiceFooterTemplate.aspx"
                                                CausesValidation="false"><span>Invoice Footer Template</span></asp:LinkButton></li>
                                         <li id="liInvNo" runat="server">
                                            <asp:LinkButton ID="LinkButton28" runat="server" PostBackUrl="~/InvoicenoMaster.aspx"
                                                CausesValidation="false"><span>Invoice No</span></asp:LinkButton></li>
                                      
                                        <li id="liCustFiel" runat="server">
                                            <asp:LinkButton ID="LinkButton10" runat="server" PostBackUrl="~/CustomFields.aspx"
                                                CausesValidation="false"><span>Custom Fields</span></asp:LinkButton></li>
                                       
                                         <li id="litax" runat="server">
                                            <asp:LinkButton ID="LinkButton25" runat="server" PostBackUrl="~/TaxMaster.aspx"
                                                CausesValidation="false"><span>Tax Master</span></asp:LinkButton></li>
                                       
                                        <li id="li14" runat="server">
                                            <asp:LinkButton ID="LinkButton24" runat="server" PostBackUrl="~/UserCustomer.aspx"
                                                CausesValidation="false"><span>Customer Master</span></asp:LinkButton></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid yellowbg">
    <div align="center" class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="maintitle">
                <asp:Label ID="LblHeading" runat="server"></asp:Label>
            </div>
        </div>
    </div>
</div>
