﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CreateInvoice.aspx.vb" Inherits="CreateInvoice" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Src="~/Header.ascx" TagName="UCHeader" TagPrefix="UC1" %>
<%@ Register Src="~/footer.ascx" TagName="UCFooter" TagPrefix="UC1" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Tax Master</title>
    <link rel="icon" href="../Images/thumb.png" />
    <link href="../Css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="../Css/css.css" rel="stylesheet" type="text/css" />
    <link type="text/css" rel="Stylesheet" href="../Css/Style.css" />
</head>
<body>
    <form id="form1" runat="server" align="center">
        <div>
            <div>
                <UC1:UCHeader ID="UCHeader" runat="server" />
            </div>
            <div class="container-fluid" style="text-align: center;">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="boxshadow">
                            <asp:UpdatePanel ID="Updt" runat="server">
                                <ContentTemplate>
                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                        <asp:DropDownList ID="DrpType" runat="server" CssClass="form-control" AutoPostBack="true">
                                            <asp:ListItem Text="Select Invoice Type" Value=""></asp:ListItem>
                                            <asp:ListItem Text="Proforma Invoice" Value="P"></asp:ListItem>
                                            <asp:ListItem Text="Tax Invoice" Value="T"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="req" runat="server" ControlToValidate="DrpType" ErrorMessage="Select Invoice Type !"
                                            InitialValue="" SetFocusOnError="true" Display="Dynamic" ValidationGroup="Save"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                        <asp:TextBox ID="TxtServiceValidity" runat="server" class="form-control" placeholder="Enter Validity Date"></asp:TextBox>
                                        <ajax:CalendarExtender ID="CalStartDate" runat="server" TargetControlID="TxtServiceValidity" CssClass="cal" Format="dd-MMM-yyyy"></ajax:CalendarExtender>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TxtServiceValidity" ErrorMessage="Enter Start Date !"
                                            InitialValue="" SetFocusOnError="true" Display="Dynamic" ValidationGroup="Save"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <table width="100%" border="0" class="table">
                                            <tr>
                                                <th colspan="2" style="text-align: center;">Invoice
                                                </th>
                                            </tr>
                                            <tr>
                                                <td style="border: none;">
                                                    <b>
                                                        <asp:Label ID="LblCompanyName" runat="server" Style="font-size: 24px;"></asp:Label></b><br />
                                                    <asp:Label ID="LblInvHead" runat="server"></asp:Label>
                                                </td>
                                                <td style="border: none;">
                                                    <img src="Images/web/thumb.png" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2"></td>
                                            </tr>

                                        </table>
                                        <table width="100%" class="table">
                                            <tr>
                                                <td width="10%"><b>Company Name</b></td>
                                                <td width="25%">
                                                    <asp:Label ID="LblCCompanyName" runat="server"></asp:Label></td>

                                                <td width="10%"><b>Inv. No</b></td>
                                                <td width="25%">
                                                    <asp:Label ID="LblInvNo" runat="server" Text="XXXXXXXXXXX"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td><b>Address</b></td>
                                                <td>
                                                    <asp:Label ID="LblCAddress" runat="server"></asp:Label>
                                                </td>
                                                <td><b>GSTIN No</b></td>
                                                <td>
                                                    <asp:Label ID="LblGSTIN" runat="server"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td><b>Contact Person</b></td>
                                                <td>
                                                    <asp:Label ID="LblCContactPerson" runat="server"></asp:Label></td>
                                                <td><b>Contact Person</b></td>
                                                <td>
                                                    <asp:Label ID="LblContactPerson" runat="server"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td><b>GSTIN</b></td>

                                                <td>
                                                    <asp:Label ID="LblCGSTIN" runat="server"></asp:Label></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </table>
                                        <table class="table">
                                            <tr>
                                                <th width="2%"></th>
                                                <th width="50%">Product</th>
                                                <%-- <th width="15%">Tax</th>--%>
                                                <th width="5%">Quantity</th>
                                                <th width="8%">Unit Price</th>
                                                <th width="5%">Amount</th>
                                            </tr>
                                            <asp:Repeater ID="RptInvoice" runat="server">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <asp:LinkButton ID="LnkRemove" runat="server" Text='<i class="fa fa-times" style="color:#ef0505;" aria-hidden="true"></i>' CommandName="Remove"></asp:LinkButton></td>
                                                        <td>

                                                            <asp:Label ID="LblProduct" runat="server"></asp:Label></td>
                                                        <%-- <td>
                                                            <asp:Label ID="LblTax" runat="server"></asp:Label></td>--%>
                                                        <td style="text-align: right;">
                                                            <asp:Label ID="LblQty" runat="server" Style="text-align: right;"></asp:Label></td>
                                                        <td style="text-align: right;">
                                                            <asp:Label ID="LblUnitPrice" runat="server" Style="text-align: right;"></asp:Label></td>
                                                        <td style="text-align: right;">
                                                            <asp:Label ID="LblAmt" runat="server" Style="text-align: right;"></asp:Label></td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            <tr>
                                                <td style="padding: 19px 5px;">
                                                    <asp:LinkButton ID="LnkAdd" runat="server" Text='<i class="fa fa-plus" style="color:#ef0505;" aria-hidden="true"></i>' ValidationGroup="SavePro"></asp:LinkButton></td>
                                                <td>

                                                    <asp:TextBox ID="TxtProduct" runat="server" class="form-control"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TxtProduct"
                                                        ErrorMessage="<br/>Enter Product" InitialValue="" SetFocusOnError="true" Display="Dynamic"
                                                        ValidationGroup="SavePro"></asp:RequiredFieldValidator>
                                                </td>
                                                <%--  <td>
                                                    <asp:DropDownList ID="Drptax" runat="server" CssClass="form-control" onchange="return getdropval()"></asp:DropDownList>
                                                    <asp:HiddenField ID="HdnMode" runat="server" />
                                                    <asp:Label ID="LblTax" runat="server"></asp:Label>
                                                </td>--%>
                                                <td style="text-align: right;">
                                                    <asp:TextBox ID="TxtQty" runat="server" class="form-control"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TxtQty"
                                                        ErrorMessage="<br/>Enter Quantity" InitialValue="" SetFocusOnError="true" Display="Dynamic"
                                                        ValidationGroup="SavePro"></asp:RequiredFieldValidator>
                                                </td>
                                                <td style="text-align: right;">
                                                    <asp:TextBox ID="TxtUnitPrice" runat="server" class="form-control" onkeyup="return GetValue()"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TxtUnitPrice"
                                                        ErrorMessage="<br/>Enter Unit Price" InitialValue="" SetFocusOnError="true" Display="Dynamic"
                                                        ValidationGroup="SavePro"></asp:RequiredFieldValidator>
                                                </td>
                                                <td style="text-align: right; padding: 19px 5px;">
                                                    <asp:Label ID="LblAmt" runat="server"></asp:Label>
                                                    <asp:HiddenField ID="HdnAmt" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" style="text-align: right;">
                                                    <b>Total</b>
                                                </td>
                                                <td style="text-align: right;">
                                                    <asp:Label ID="LblTotal" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <asp:Repeater ID="RptTax" runat="server" OnItemDataBound="RptTax_ItemDataBound">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td colspan="4" style="text-align: right;">
                                                            <asp:CheckBox runat="server" ID="ChkUserInfo"  onchange='<%#"return chkval(""" + Container.FindControl("ChkUserInfo").ClientID + """,""" + Container.FindControl("LblTaxPer").ClientID + """,""" + Container.FindControl("TxtTotal").ClientID + """,""" + Container.FindControl("HdnTotal").ClientID + """)" %>' />
                                                            <asp:Label runat="server" ID="LblChk" Text='<%#Container.DataItem("TaxID")%>'
                                                                Visible="false"></asp:Label>
                                                            <asp:Label ID="LblTaxPer"  runat="server" style="display:none;"></asp:Label>
                                                            <asp:Label ID="LblTax" runat="server"></asp:Label>
                                                        </td>
                                                        <td style="text-align: right;">
                                                            <asp:TextBox ID="TxtTotal" runat="server"></asp:TextBox>
                                                            <asp:HiddenField ID="HdnTotal" runat="server" />
                                                           
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            <tr>
                                                <th colspan="4" style="text-align: right;">
                                                    <b>Net Total</b>
                                                </th>
                                                <th style="text-align: right;">
                                                    <asp:Label ID="LblNetTotal" runat="server"></asp:Label>
                                                    <asp:HiddenField ID="HdnNetTot" runat="server" />
                                                </th>
                                            </tr>
                                        </table>

                                        <table width="100%" class="table">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="LblFooter" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                        <table width="100%" class="table">
                                            <tr>
                                                <td colspan="2" style="text-align: center;">
                                                    <asp:Button ID="Btnclick" runat="server" Style="display: none;" />
                                                    <asp:Button ID="BtnSaveInv" runat="server" Text="Save" CssClass="button" ValidationGroup="Save" />
                                                    <asp:Button ID="BtnInvCancel" runat="server" Text="Cancel" CssClass="button"
                                                        CausesValidation="false" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>


                        </div>
                    </div>

                </div>
            </div>
            <div>
                <UC1:UCFooter ID="UCFooter" runat="server" />
            </div>
        </div>
    </form>
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <script>
        function GetValue() {
            //debugger
            var qty = $("#TxtQty").val();
            var unitprice = $("#TxtUnitPrice").val();
            var total;
            var tax;

            total = Number(qty) * Number(unitprice);
            if ($("#HdnMode").val() == "A") {
                total = Number(total) + Number($("#LblTax").text());
            } else {
                total = total + (Number(total) * $("#LblTax").text() / 100);
            }
            document.getElementById("LblAmt").innerText = total;
            $("#HdnAmt").val(total);
        }

        function getdropval() {
            //debugger
            $('#Btnclick').click();

        }

        function chkval(Chk, TaxPer, Total, HdnTotal) {
            debugger
            var total = $("#LblTotal").text();
            var tax = document.getElementById(TaxPer).innerText;
            if (document.getElementById(Chk).checked) {
                total = Number(total) * Number(tax) / 100;
                document.getElementById(Total).value = total;
                document.getElementById(HdnTotal).value = total;
                $('#Btnclick').click();
                //alert(total);
            }
            else {
                document.getElementById(Total).value = 0;
                document.getElementById(HdnTotal).value = 0;
                $('#Btnclick').click();
            }
        }
    </script>
</body>
</html>

